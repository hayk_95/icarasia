package com.icarasia.leadmarketplace.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.support.annotation.NonNull;

import com.icarasia.leadmarketplace.AppDelegate;
import com.icarasia.leadmarketplace.data.StatusResource;
import com.icarasia.leadmarketplace.network.body.RegisterBody;

public class SignupViewModel extends AndroidViewModel {

//    private UserCredentials userCredentials;
    private MutableLiveData<RegisterBody> credentials = new MutableLiveData<>();
    private LiveData<StatusResource> user = Transformations.switchMap(credentials, registerBody -> {
        if(registerBody.getEmail() != null && registerBody.getPassword() != null) {
            return ((AppDelegate) getApplication()).getRepository().registerUser(registerBody);
        }else {
            return null;
        }
    });

    public SignupViewModel(@NonNull Application application) {
        super(application);

    }

    public LiveData<StatusResource> sendEmailForVerification(String email){
        return ((AppDelegate) getApplication()).getRepository().sendEmailForVerification(email);

    }

    public void setEmail(String email){
        credentials.setValue(new RegisterBody(null,email,null));
    }

    public void setPassword(String password){
        credentials.setValue(new RegisterBody("John",credentials.getValue().getEmail(),password));
    }

    public LiveData<StatusResource> sendVerification(String code){
        return ((AppDelegate) getApplication()).getRepository().sendVerification(code);
    }

    public LiveData<StatusResource> getRegisterResponse(){
        return user;
    }
}
