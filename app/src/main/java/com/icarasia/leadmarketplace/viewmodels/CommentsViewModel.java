package com.icarasia.leadmarketplace.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.icarasia.leadmarketplace.AppDelegate;
import com.icarasia.leadmarketplace.data.Resource;
import com.icarasia.leadmarketplace.data.models.Comment;
import com.icarasia.leadmarketplace.data.models.User;
import com.icarasia.leadmarketplace.network.response.SendCommentResponse;

import java.util.List;

public class CommentsViewModel extends AndroidViewModel {

    public CommentsViewModel(@NonNull Application application) {
        super(application);
    }

    public LiveData<Resource<List<Comment>>> getComments(int leadId){
        return ((AppDelegate) getApplication()).getRepository().getComments(leadId);
    }

    public LiveData<Resource<SendCommentResponse>> sendComment(int leadId,String content){
        return ((AppDelegate) getApplication()).getRepository().sendComment(leadId,content);
    }

    public LiveData<Resource<User>> getUserInformation(){
        return ((AppDelegate) getApplication()).getRepository().getUser();
    }
}
