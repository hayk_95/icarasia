package com.icarasia.leadmarketplace.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.icarasia.leadmarketplace.AppDelegate;
import com.icarasia.leadmarketplace.data.Resource;
import com.icarasia.leadmarketplace.data.models.Notification;

import java.util.List;

public class NotificationsViewModel extends AndroidViewModel {

    public NotificationsViewModel(@NonNull Application application) {
        super(application);
    }

    public LiveData<Resource<List<Notification>>> getNotifications(){
        return ((AppDelegate) getApplication()).getRepository().getNotifications();
    }
}
