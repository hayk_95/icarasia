package com.icarasia.leadmarketplace.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.icarasia.leadmarketplace.AppDelegate;
import com.icarasia.leadmarketplace.data.Resource;
import com.icarasia.leadmarketplace.data.models.User;

public class AccountBalanceViewModel extends AndroidViewModel {

    public AccountBalanceViewModel(@NonNull Application application) {
        super(application);
    }

    public LiveData<Resource<User>> getUserInformation(){
        return ((AppDelegate) getApplication()).getRepository().getUser();
    }
}
