package com.icarasia.leadmarketplace.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.icarasia.leadmarketplace.AppDelegate;
import com.icarasia.leadmarketplace.data.Resource;
import com.icarasia.leadmarketplace.network.response.StatusResponse;

public class PaymentViewModel extends AndroidViewModel {

    public PaymentViewModel(@NonNull Application application) {
        super(application);
    }

    public MutableLiveData<Resource<StatusResponse>> processPayment(int id,String number,String name,String date,String cvv,String promoCode,int save){
        return ((AppDelegate) getApplication()).getRepository().processPayment(id,number,name,date,cvv,promoCode,save);
    }
}
