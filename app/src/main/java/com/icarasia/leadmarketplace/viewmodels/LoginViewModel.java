package com.icarasia.leadmarketplace.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.support.annotation.NonNull;

import com.icarasia.leadmarketplace.AppDelegate;
import com.icarasia.leadmarketplace.data.StatusResource;
import com.icarasia.leadmarketplace.network.body.LoginBody;

public class LoginViewModel extends AndroidViewModel {
    private LoginBody userCredentials;
    private MutableLiveData<LoginBody> credentials = new MutableLiveData<>();
    private LiveData<StatusResource> user = Transformations.switchMap(credentials, credentialsData ->
            ((AppDelegate) getApplication()).getRepository().login(credentialsData));

    public LoginViewModel(@NonNull Application application) {
        super(application);
        userCredentials = new LoginBody();
    }

    public void setEmail(String email){
        userCredentials.setEmail(email);
    }

    public void setPassword(String password){
        userCredentials.setPassword(password);
        credentials.setValue(userCredentials);
    }

    public LiveData<StatusResource> getLoginResponse(){
        return user;
    }


}
