package com.icarasia.leadmarketplace.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.icarasia.leadmarketplace.AppDelegate;
import com.icarasia.leadmarketplace.data.Resource;
import com.icarasia.leadmarketplace.data.StatusResource;
import com.icarasia.leadmarketplace.data.models.UserInformation;
import com.icarasia.leadmarketplace.network.body.AccountUpdateBody;
import com.icarasia.leadmarketplace.network.response.AccountUpdateResponse;

import java.io.File;

public class EditProfileViewModel extends AndroidViewModel {

    private String oldPassword;
    private String password;
    private String passwordConfirmation;

    public EditProfileViewModel(@NonNull Application application) {
        super(application);
    }

    public LiveData<Resource<UserInformation>> getUserInformation() {
        return ((AppDelegate) getApplication()).getRepository().getUserInformation();
    }

    public MutableLiveData<Resource<AccountUpdateResponse>> updateUserAccount(String name, String position, String phone, String phoneCode, String email) {
        AccountUpdateBody body;
        if(password != null){
            body = new AccountUpdateBody(name, position, phone, phoneCode, email, oldPassword, password, passwordConfirmation);
        }else {
            body = new AccountUpdateBody(name, position, phone, phoneCode, email);
        }
        return ((AppDelegate) getApplication()).getRepository().updateUserAccount(body);
    }

    public void setPassword(String oldPassword, String password, String passwordConfirmation) {
        this.oldPassword = oldPassword;
        this.password = password;
        this.passwordConfirmation = passwordConfirmation;
    }

    public MutableLiveData<StatusResource> uploadAvatarImage(File file){
        return ((AppDelegate) getApplication()).getRepository().uploadAvatarImage(file);
    }
}
