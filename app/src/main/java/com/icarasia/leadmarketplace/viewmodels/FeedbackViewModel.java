package com.icarasia.leadmarketplace.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.icarasia.leadmarketplace.AppDelegate;
import com.icarasia.leadmarketplace.data.Resource;
import com.icarasia.leadmarketplace.data.models.LeadDetail;

public class FeedbackViewModel extends AndroidViewModel {

    public FeedbackViewModel(@NonNull Application application) {
        super(application);
    }

    public LiveData<Resource<LeadDetail>> getLead(int id){
        return ((AppDelegate) getApplication()).getRepository().getLeadDetail(id);
    }

//    public void changeFeedback(int id,Feedback feedback){
//        ((AppDelegate) getApplication()).getRepository().updateFeedback(id,feedback);
//    }

}
