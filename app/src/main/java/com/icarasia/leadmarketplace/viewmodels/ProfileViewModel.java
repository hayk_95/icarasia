package com.icarasia.leadmarketplace.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.icarasia.leadmarketplace.AppDelegate;
import com.icarasia.leadmarketplace.data.Resource;
import com.icarasia.leadmarketplace.data.models.User;

public class ProfileViewModel extends AndroidViewModel {

    public ProfileViewModel(@NonNull Application application) {
        super(application);
    }

    public LiveData<Resource<User>> getUserInformation(){
        return ((AppDelegate) getApplication()).getRepository().getUser();
    }
}
