package com.icarasia.leadmarketplace.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.icarasia.leadmarketplace.AppDelegate;
import com.icarasia.leadmarketplace.data.Resource;
import com.icarasia.leadmarketplace.data.enums.LeadStatus;
import com.icarasia.leadmarketplace.network.response.StatusResponse;

public class ChangeStatusViewModel extends AndroidViewModel {

    public ChangeStatusViewModel(@NonNull Application application) {
        super(application);
    }

    public MutableLiveData<Resource<StatusResponse>> updateStatus(int id, LeadStatus status){
        return  ((AppDelegate) getApplication()).getRepository().updateStatus(id,status.getSerializeName());
    }
}
