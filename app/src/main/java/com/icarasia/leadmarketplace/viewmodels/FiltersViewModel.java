package com.icarasia.leadmarketplace.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.support.annotation.NonNull;

import com.icarasia.leadmarketplace.AppDelegate;
import com.icarasia.leadmarketplace.data.enums.LeadStatus;

public class FiltersViewModel extends AndroidViewModel {
    private int ratingFilter;
    private LeadStatus statusFilter;
    private String leadsDateFrom;
    private String leadsDateTo;

    public FiltersViewModel(@NonNull Application application) {
        super(application);
    }

    public String getLeadsDateFrom() {
        leadsDateFrom = ((AppDelegate) getApplication()).getRepository().getLeadsDateFromFilter();
        return leadsDateFrom;
    }

    public void setLeadsDateFrom(String leadsDateFrom) {
        this.leadsDateFrom = leadsDateFrom;
    }

    public String getLeadsDateTo() {
        leadsDateTo = ((AppDelegate) getApplication()).getRepository().getLeadsDateToFilter();
        return leadsDateTo;
    }

    public void setLeadsDateTo(String leadsDateTo) {
        this.leadsDateTo = leadsDateTo;
    }

    public int getRatingFilter() {
        ratingFilter = ((AppDelegate) getApplication()).getRepository().getRatingFilter();
        return ratingFilter;
    }

    public void setRatingFilter(int ratingFilter) {
        this.ratingFilter = ratingFilter;
    }

    public LeadStatus getStatusFilter() {
        statusFilter = LeadStatus.getStatusBySerializeName(((AppDelegate) getApplication()).getRepository().getStatusFilter());
        return statusFilter;
    }

    public void setStatusFilter(LeadStatus statusFilter) {
        this.statusFilter = statusFilter;
    }

    public void saveFilters() {
        ((AppDelegate) getApplication()).getRepository().saveFilters(leadsDateFrom, leadsDateTo, ratingFilter, statusFilter != null ? statusFilter.getSerializeName() : "");
    }
}
