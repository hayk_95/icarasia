package com.icarasia.leadmarketplace.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.icarasia.leadmarketplace.AppDelegate;
import com.icarasia.leadmarketplace.data.Resource;
import com.icarasia.leadmarketplace.data.models.Lead;

import java.util.List;

public class LeadsViewModel extends AndroidViewModel {

    public LeadsViewModel(@NonNull Application application) {
        super(application);
    }

    public LiveData<Resource<List<Lead>>> getLeads(){
        return ((AppDelegate) getApplication()).getRepository().getLeads();
    }
}
