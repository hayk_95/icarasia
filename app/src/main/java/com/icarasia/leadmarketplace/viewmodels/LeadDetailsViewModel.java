package com.icarasia.leadmarketplace.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.icarasia.leadmarketplace.AppDelegate;
import com.icarasia.leadmarketplace.data.Resource;
import com.icarasia.leadmarketplace.data.enums.LeadStatus;
import com.icarasia.leadmarketplace.data.models.LeadDetail;

public class LeadDetailsViewModel extends AndroidViewModel {

    public LeadDetailsViewModel(@NonNull Application application) {
        super(application);
    }

    public LiveData<Resource<LeadDetail>> getLeadDetail(int id){
        return ((AppDelegate) getApplication()).getRepository().getLeadDetail(id);
    }

    public void changeLeadStatus(int id,LeadStatus status){
//        ((AppDelegate) getApplication()).getRepository().changeStatus(id,status);
    }

}
