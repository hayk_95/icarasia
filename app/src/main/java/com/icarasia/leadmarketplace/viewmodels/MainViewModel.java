package com.icarasia.leadmarketplace.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.icarasia.leadmarketplace.AppDelegate;
import com.icarasia.leadmarketplace.data.StatusResource;

public class MainViewModel extends AndroidViewModel {

    public MainViewModel(@NonNull Application application) {
        super(application);
    }

    public boolean isUserExist(){
        return ((AppDelegate) getApplication()).getRepository().isUserExist();
    }

    public MutableLiveData<StatusResource> logOut(){
        return ((AppDelegate) getApplication()).getRepository().logOut();
    }
}
