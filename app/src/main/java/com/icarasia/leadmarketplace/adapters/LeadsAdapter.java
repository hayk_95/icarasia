package com.icarasia.leadmarketplace.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.icarasia.leadmarketplace.R;
import com.icarasia.leadmarketplace.data.models.Lead;

import java.util.ArrayList;
import java.util.List;

import static com.icarasia.leadmarketplace.data.enums.LeadStatus.READ;
import static com.icarasia.leadmarketplace.data.enums.LeadStatus.SALE_CLOSED;
import static com.icarasia.leadmarketplace.data.enums.LeadStatus.SALE_FAILED;
import static com.icarasia.leadmarketplace.data.enums.LeadStatus.SALE_IN_PROGRESS;
import static com.icarasia.leadmarketplace.data.enums.LeadStatus.UNREAD;

public class LeadsAdapter extends RecyclerView.Adapter<LeadsAdapter.LeadsViewHolder> {

    private Context context;
    private List<Lead> leads;

    private OnLeadAdapterItemClickListener adapterItemClickListener;

    public LeadsAdapter(Context context) {
        this.context = context;
        leads = new ArrayList<>();
    }

    public void updateLeads(List<Lead> newLeads) {
        leads.clear();
        leads.addAll(newLeads);
        notifyDataSetChanged();
    }

    private void setLeadStatus(Lead lead,LeadsViewHolder leadsViewHolder){
        switch (lead.getStatus()){
            case READ:
                leadsViewHolder.status.setBackground(context.getResources().getDrawable(R.drawable.read_status_background));
                leadsViewHolder.status.setText(READ.getStatusName());
                break;
            case UNREAD:
                leadsViewHolder.status.setBackground(context.getResources().getDrawable(R.drawable.unread_status_background));
                leadsViewHolder.status.setText(UNREAD.getStatusName());
                break;
            case SALE_CLOSED:
                leadsViewHolder.status.setBackground(context.getResources().getDrawable(R.drawable.sale_closed_status_background));
                leadsViewHolder.status.setText(SALE_CLOSED.getStatusName());
                break;
            case SALE_IN_PROGRESS:
                leadsViewHolder.status.setBackground(context.getResources().getDrawable(R.drawable.sale_in_progress_status_background));
                leadsViewHolder.status.setText(SALE_IN_PROGRESS.getStatusName());
                break;
            case SALE_FAILED:
                leadsViewHolder.status.setBackground(context.getResources().getDrawable(R.drawable.sale_failed_status_background));
                leadsViewHolder.status.setText(SALE_FAILED.getStatusName());
                break;
        }
    }

    public void setOnLeadAdapterItemClickListener(OnLeadAdapterItemClickListener adapterItemClickListener){
        this.adapterItemClickListener = adapterItemClickListener;
    }

    @NonNull
    @Override
    public LeadsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new LeadsViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.leads_view_holder, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final LeadsViewHolder leadsViewHolder, int i) {
//        Lead lead = getItem(i);
//        if(lead != null){
            leadsViewHolder.bindTo(leads.get(i));
//        }else {
//            leadsViewHolder.clear();
//        }
    }

    @Override
    public int getItemCount() {
        return leads.size();
    }

    class LeadsViewHolder extends RecyclerView.ViewHolder {
        ConstraintLayout root;
        TextView date;
        TextView name;
        TextView rating;
        TextView status;

        LeadsViewHolder(@NonNull View itemView) {
            super(itemView);
            root = itemView.findViewById(R.id.root);
            date = itemView.findViewById(R.id.mobileDate);
            name = itemView.findViewById(R.id.name);
            rating = itemView.findViewById(R.id.rating);
            status = itemView.findViewById(R.id.status);
        }

        private void bindTo(final Lead lead){
            name.setText(lead.getName());
            date.setText(lead.getMobileDate());
            rating.setText(String.valueOf(lead.getRating()));
            setLeadStatus(lead,this);
            root.setOnClickListener(v -> {
                if (adapterItemClickListener != null) {
                    adapterItemClickListener.onLeadItemClicked(lead.getId());
                }
            });
        }

        private void clear(){
            root.removeAllViews();
            root.setOnClickListener(null);
        }
    }

    public interface OnLeadAdapterItemClickListener{
        void onLeadItemClicked(int id);
    }

    private static DiffUtil.ItemCallback<Lead> DIFF_CALLBACK = new DiffUtil.ItemCallback<Lead>() {
                // Concert details may have changed if reloaded from the database,
                // but ID is fixed.

        @Override
        public boolean areItemsTheSame(@NonNull Lead lead, @NonNull Lead t1) {
            return lead.getId() == t1.getId();
        }

        @Override
        public boolean areContentsTheSame(@NonNull Lead lead, @NonNull Lead t1) {
            return lead.equals(t1);
        }
    };
}
