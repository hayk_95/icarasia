package com.icarasia.leadmarketplace.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.icarasia.leadmarketplace.R;
import com.icarasia.leadmarketplace.data.models.Comment;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.CommentViewHolder> {

    private Context context;
    private List<Comment> comments;

    public CommentsAdapter(Context context){
        this.context = context;
        comments = new ArrayList<>();
    }

    @NonNull
    @Override
    public CommentViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new CommentViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.comment_view_holder_layout,viewGroup,false));
    }

    @Override
    public void onBindViewHolder(@NonNull CommentViewHolder commentViewHolder, int i) {
        bindViewHolderData(commentViewHolder,i);
    }

    @Override
    public int getItemCount() {
        return comments.size();
    }

    private void bindViewHolderData(CommentViewHolder commentViewHolder,int i){
        commentViewHolder.author.setText(comments.get(i).getUserName());
        commentViewHolder.message.setText(comments.get(i).getContent());
        commentViewHolder.time.setText(comments.get(i).getShowDiffTime());
        Picasso.get().load(comments.get(i).getUser().getAvatar()).into(commentViewHolder.avatar);
        if(comments.get(i).isLiked()){
            commentViewHolder.like.setImageDrawable(context.getResources().getDrawable(R.drawable.like));
        }else {
            commentViewHolder.like.setImageDrawable(context.getResources().getDrawable(R.drawable.dislike));
        }
//        if(commentViewHolder.subComments.getLayoutManager() == null){
//            commentViewHolder.subComments.setLayoutManager(new LinearLayoutManager(context));
//        }
//        if(commentViewHolder.subComments.getAdapter() == null){
//            commentViewHolder.subComments.setAdapter(new CommentsAdapter(context));
//        }

    }

    public void updateComments(List<Comment> list){
        comments.clear();
        comments.addAll(list);
        notifyDataSetChanged();
    }

    public void addComment(Comment comment){
        comments.add(0,comment);
        notifyItemInserted(0);
    }

    class CommentViewHolder extends RecyclerView.ViewHolder{
        ImageView avatar;
        TextView author;
        TextView message;
        TextView time;
        ImageView like;

        CommentViewHolder(@NonNull View itemView) {
            super(itemView);
            avatar = itemView.findViewById(R.id.author_avatar);
            author = itemView.findViewById(R.id.author);
            message = itemView.findViewById(R.id.content);
            time = itemView.findViewById(R.id.time);
            like = itemView.findViewById(R.id.like);
        }
    }
}
