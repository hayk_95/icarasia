package com.icarasia.leadmarketplace.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.icarasia.leadmarketplace.R;
import com.icarasia.leadmarketplace.data.models.Notification;

import java.util.ArrayList;
import java.util.List;

public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.NotificationViewHolder> {
    private List<Notification> notifications;

    public NotificationsAdapter(){
        notifications = new ArrayList<>();
    }

    @NonNull
    @Override
    public NotificationViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new NotificationViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.notification_view_holder_layout,viewGroup,false));
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationViewHolder notificationViewHolder, int i) {
        bindViewHolderData(notificationViewHolder,i);
    }

    @Override
    public int getItemCount() {
        return notifications.size();
    }

    private void bindViewHolderData(NotificationViewHolder notificationViewHolder,int i){
        notificationViewHolder.number.setText("#" + String.valueOf(notifications.get(i).getNumber()) + ":");
        if(notifications.get(i).getCount() != null){
            notificationViewHolder.count.setVisibility(View.VISIBLE);
            notificationViewHolder.count.setText(String.valueOf(notifications.get(i).getCount()));
        }else {
            notificationViewHolder.count.setVisibility(View.GONE);
            notificationViewHolder.count.setText("");
        }
        notificationViewHolder.message.setText(notifications.get(i).getMessage());
        notificationViewHolder.time.setText(notifications.get(i).getTime());
        if(notifications.get(i).isNew()){
            notificationViewHolder.newIcon.setVisibility(View.VISIBLE);
        }else {
            notificationViewHolder.newIcon.setVisibility(View.INVISIBLE);
        }
    }

    public void updateNotifications(List<Notification> list){
        notifications.clear();
        notifications.addAll(list);
        notifyDataSetChanged();
    }

    class NotificationViewHolder extends RecyclerView.ViewHolder{
        View newIcon;
        TextView number;
        TextView count;
        TextView message;
        TextView time;

        NotificationViewHolder(@NonNull View itemView) {
            super(itemView);
            newIcon = itemView.findViewById(R.id.new_icon);
            number = itemView.findViewById(R.id.notification_number);
            count = itemView.findViewById(R.id.comments_count);
            message = itemView.findViewById(R.id.content);
            time = itemView.findViewById(R.id.notification_time);
        }
    }
}
