package com.icarasia.leadmarketplace.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.icarasia.leadmarketplace.R;

import java.util.ArrayList;
import java.util.List;

public class TopTradesAdapter extends RecyclerView.Adapter<TopTradesAdapter.TopTradeViewHolder> {

    private List<String> cars;
    private List<Integer> colors;

    public TopTradesAdapter(){
        cars = new ArrayList<>();
        colors = new ArrayList<>();
        colors.add(R.color.color_1);
        colors.add(R.color.color_2);
        colors.add(R.color.color_3);
        colors.add(R.color.color_4);
        colors.add(R.color.color_5);
        colors.add(R.color.color_6);
    }

    @NonNull
    @Override
    public TopTradeViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new TopTradeViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.top_trade_view_holder_layout,viewGroup,false));
    }

    @Override
    public void onBindViewHolder(@NonNull TopTradeViewHolder topTradeViewHolder, int i) {
        topTradeViewHolder.icon.setImageResource(colors.get(i));
        topTradeViewHolder.name.setText(cars.get(i));
    }

    @Override
    public int getItemCount() {
        return cars.size();
    }

    public void updateCars(List<String> list){
        cars.clear();
        cars.addAll(list);
        notifyDataSetChanged();
    }

    public List<Integer> getColors() {
        return colors;
    }

    class TopTradeViewHolder extends RecyclerView.ViewHolder{
        ImageView icon;
        TextView name;

        TopTradeViewHolder(@NonNull View itemView) {
            super(itemView);
            icon = itemView.findViewById(R.id.icon);
            name = itemView.findViewById(R.id.name);
        }
    }
}
