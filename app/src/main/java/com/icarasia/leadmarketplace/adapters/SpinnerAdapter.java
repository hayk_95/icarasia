package com.icarasia.leadmarketplace.adapters;

import android.content.Context;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.icarasia.leadmarketplace.R;

public class SpinnerAdapter extends ArrayAdapter<String> {
    private String[] list;
    private float textSize = -1;

    public SpinnerAdapter(Context context, int textViewResourceId,
                           String[] objects) {
        super(context, textViewResourceId, objects);
        list = objects;
    }

    public SpinnerAdapter(Context context, int textViewResourceId,
                          String[] objects,float textSize) {
        super(context, textViewResourceId, objects);
        list = objects;
        this.textSize = textSize;
    }

    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {

        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        return getFirstView(position, convertView, parent);
    }

    private View getCustomView(int position, View convertView,
                       ViewGroup parent) {

        View row = LayoutInflater.from(getContext()).inflate(R.layout.spinner_item_back,parent,false);
        TextView label = row.findViewById(R.id.text);
        if(textSize > 0) {
            label.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
        }
        label.setText(list[position]);
        return row;
    }

    private View getFirstView(int position, View convertView,
                      ViewGroup parent){
        View row = LayoutInflater.from(getContext()).inflate(R.layout.spinner_first_line_back, parent, false);
        TextView label = row.findViewById(R.id.text);
        if(textSize > 0) {
            label.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
        }
        label.setText(list[position]);
        return row;
    }

    public void setTextSize(float size){
        textSize = size;
    }

}
