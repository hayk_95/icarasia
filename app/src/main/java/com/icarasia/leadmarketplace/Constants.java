package com.icarasia.leadmarketplace;

public class Constants {
    public static final String AUTH_TOKEN = "AuthToken";
    public static final String LEADS_DATE_FROM_FILTER_PREF = "LeadsDateFromFilterPref";
    public static final String LEADS_DATE_TO_FILTER_PREF = "LeadsDateToFilterPref";
    public static final String RATING_FILTER_PREF = "RatingFilterPref";
    public static final String STATUS_FILTER_PREF = "StatusFilterPref";
}
