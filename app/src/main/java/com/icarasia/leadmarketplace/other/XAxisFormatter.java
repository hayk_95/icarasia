package com.icarasia.leadmarketplace.other;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.util.ArrayList;
import java.util.List;

public class XAxisFormatter implements IAxisValueFormatter {
    private List<String> list = new ArrayList<>();

    @Override
    public String getFormattedValue(float value, AxisBase axis) {
        return list.get((int) value);
    }

    public void setList(List<String> list) {
        this.list.clear();
        this.list.addAll(list);
    }
}
