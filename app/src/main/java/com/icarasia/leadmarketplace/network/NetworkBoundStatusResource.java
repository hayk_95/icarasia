package com.icarasia.leadmarketplace.network;

import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.MainThread;

import com.icarasia.leadmarketplace.data.StatusResource;

public abstract class NetworkBoundStatusResource {
    private final MutableLiveData<StatusResource> result = new MutableLiveData<>();

    @MainThread
    public NetworkBoundStatusResource() {
        result.setValue(StatusResource.loading());
        createCall();
    }

    @MainThread
    protected abstract void createCall();

    public void setResultValue(StatusResource value) {
        result.setValue(value);
    }

    public final MutableLiveData<StatusResource> getAsMutableLiveData() {
        return result;
    }
}
