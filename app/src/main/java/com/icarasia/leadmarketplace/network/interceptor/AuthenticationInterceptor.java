package com.icarasia.leadmarketplace.network.interceptor;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class AuthenticationInterceptor implements Interceptor {

    public static final String BASIC_AUTH_KEY = "Authorization";
    private String authToken;

    public AuthenticationInterceptor(String token) {
        this.authToken = token;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request original = chain.request();
        Request.Builder builder = original.newBuilder().header(BASIC_AUTH_KEY, authToken);
        Request request = builder.build();
        return chain.proceed(request);
    }
}
