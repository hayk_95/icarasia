package com.icarasia.leadmarketplace.network.response;

import com.google.gson.annotations.SerializedName;
import com.icarasia.leadmarketplace.data.models.Lead;

import java.util.List;

public class LeadsResponse {
    @SerializedName("current_page")
    private String currentPage;

    @SerializedName("data")
    private List<Lead> leads;

    public String getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(String currentPage) {
        this.currentPage = currentPage;
    }

    public List<Lead> getLeads() {
        return leads;
    }

    public void setLeads(List<Lead> leads) {
        this.leads = leads;
    }
}
