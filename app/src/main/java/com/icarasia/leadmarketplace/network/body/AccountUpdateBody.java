package com.icarasia.leadmarketplace.network.body;

import com.google.gson.annotations.SerializedName;

public class AccountUpdateBody {

    @SerializedName("name")
    private String name;

    @SerializedName("position")
    private String position;

    @SerializedName("phone")
    private String phone;

    @SerializedName("calling_code")
    private String callingCode;

    @SerializedName("email")
    private String email;

    @SerializedName("old_password")
    private String oldPassword;

    @SerializedName("password")
    private String password;

    @SerializedName("password_confirmation")
    private String passwordConfirmation;

    public AccountUpdateBody() {
    }

    public AccountUpdateBody(String name, String position, String phone, String callingCode, String email) {
        this.name = name;
        this.position = position;
        this.phone = phone;
        this.callingCode = callingCode;
        this.email = email;
    }

    public AccountUpdateBody(String name, String position, String phone, String callingCode, String email, String oldPassword, String password, String passwordConfirmation) {
        this.name = name;
        this.position = position;
        this.phone = phone;
        this.callingCode = callingCode;
        this.email = email;
        this.oldPassword = oldPassword;
        this.password = password;
        this.passwordConfirmation = passwordConfirmation;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCallingCode() {
        return callingCode;
    }

    public void setCallingCode(String callingCode) {
        this.callingCode = callingCode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }
}
