package com.icarasia.leadmarketplace.network.response;

import com.google.gson.annotations.SerializedName;
import com.icarasia.leadmarketplace.data.models.Comment;

import java.util.ArrayList;

public class CommentResponse {
    @SerializedName("current_page")
    private int currentPage;
    @SerializedName("data")
    private ArrayList<Comment> comments = new ArrayList<>();

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public ArrayList<Comment> getComments() {
        return comments;
    }

    public void setComments(ArrayList<Comment> comments) {
        this.comments = comments;
    }
}
