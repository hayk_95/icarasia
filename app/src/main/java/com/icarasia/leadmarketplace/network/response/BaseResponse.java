package com.icarasia.leadmarketplace.network.response;

import android.support.annotation.Nullable;

import com.icarasia.leadmarketplace.data.enums.ResponseStatus;

public class BaseResponse<T> {

    @Nullable
    private ResponseStatus status;
    @Nullable
    private String message;
    @Nullable
    private T data;

    public ResponseStatus getStatus() {
        return status;
    }

    public void setStatus(ResponseStatus status) {
        this.status = status;
    }

    @Nullable
    public String getMessage() {
        return message;
    }

    public void setMessage(@Nullable String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
