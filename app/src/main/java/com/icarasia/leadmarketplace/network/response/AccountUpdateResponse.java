package com.icarasia.leadmarketplace.network.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class AccountUpdateResponse {

    @SerializedName("status")
    private int status;

    @SerializedName("message")
    private String message;

    @SerializedName("errors")
    private Error errors;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Error getErrors() {
        return errors;
    }

    public void setErrors(Error errors) {
        this.errors = errors;
    }

    class Error{
        @SerializedName("name")
        private ArrayList<String> nameError;

        @SerializedName("position")
        private ArrayList<String> positionError;

        @SerializedName("phone")
        private ArrayList<String> phoneError;

        @SerializedName("calling_code")
        private ArrayList<String> callingCodeError;

        @SerializedName("email")
        private ArrayList<String> emailError;

        public ArrayList<String> getNameError() {
            return nameError;
        }

        public void setNameError(ArrayList<String> nameError) {
            this.nameError = nameError;
        }

        public ArrayList<String> getPositionError() {
            return positionError;
        }

        public void setPositionError(ArrayList<String> positionError) {
            this.positionError = positionError;
        }

        public ArrayList<String> getPhoneError() {
            return phoneError;
        }

        public void setPhoneError(ArrayList<String> phoneError) {
            this.phoneError = phoneError;
        }

        public ArrayList<String> getCallingCodeError() {
            return callingCodeError;
        }

        public void setCallingCodeError(ArrayList<String> callingCodeError) {
            this.callingCodeError = callingCodeError;
        }

        public ArrayList<String> getEmailError() {
            return emailError;
        }

        public void setEmailError(ArrayList<String> emailError) {
            this.emailError = emailError;
        }
    }
}
