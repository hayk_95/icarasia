package com.icarasia.leadmarketplace.network;

import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.MainThread;

import com.icarasia.leadmarketplace.data.Resource;

public abstract class NetworkBoundResource <T> {
    private final MutableLiveData<Resource<T>> result = new MutableLiveData<>();

    @MainThread
    public NetworkBoundResource() {
        result.setValue((Resource<T>) Resource.loading(null));
        createCall();
    }

    @MainThread
    protected abstract void createCall();

    public void setResultValue(Resource<T> value) {
        result.setValue(value);
    }

    public final MutableLiveData<Resource<T>> getAsMutableLiveData() {
        return result;
    }
}
