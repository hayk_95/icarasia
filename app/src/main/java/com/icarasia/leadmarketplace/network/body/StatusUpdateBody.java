package com.icarasia.leadmarketplace.network.body;

import com.google.gson.annotations.SerializedName;

public class StatusUpdateBody {

    @SerializedName("lead_id")
    private int leadId;

    @SerializedName("status")
    private String status;

    public StatusUpdateBody(int leadId, String status) {
        this.leadId = leadId;
        this.status = status;
    }

    public int getLeadId() {
        return leadId;
    }

    public void setLeadId(int leadId) {
        this.leadId = leadId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
