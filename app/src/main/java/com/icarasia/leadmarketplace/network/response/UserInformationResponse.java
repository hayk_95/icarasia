package com.icarasia.leadmarketplace.network.response;

import com.google.gson.annotations.SerializedName;
import com.icarasia.leadmarketplace.data.models.UserInformation;

public class UserInformationResponse {

    @SerializedName("user")
    private UserInformation userInformation;

    public UserInformation getUserInformation() {
        return userInformation;
    }

    public void setUserInformation(UserInformation userInformation) {
        this.userInformation = userInformation;
    }
}
