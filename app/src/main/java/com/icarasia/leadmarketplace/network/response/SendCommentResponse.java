package com.icarasia.leadmarketplace.network.response;

import com.google.gson.annotations.SerializedName;

public class SendCommentResponse {

    @SerializedName("key")
    private String key;

    @SerializedName("comment_id")
    private int commentId;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public int getCommentId() {
        return commentId;
    }

    public void setCommentId(int commentId) {
        this.commentId = commentId;
    }
}
