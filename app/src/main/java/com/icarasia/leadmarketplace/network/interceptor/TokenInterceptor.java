package com.icarasia.leadmarketplace.network.interceptor;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class TokenInterceptor implements Interceptor {
    public static final String AUTH_TOKEN_HEADER_KEY = "JWTMobileAuthorization";

    private String token;

    public TokenInterceptor(String token) {
        this.token = token;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request original = chain.request();
        Request.Builder builder = original.newBuilder().header(AUTH_TOKEN_HEADER_KEY,token);
        Request request = builder.build();
        return chain.proceed(request);
    }
}
