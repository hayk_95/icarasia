package com.icarasia.leadmarketplace.network.response;

import com.google.gson.annotations.SerializedName;

public class StatusResponse {

    @SerializedName("success")
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
