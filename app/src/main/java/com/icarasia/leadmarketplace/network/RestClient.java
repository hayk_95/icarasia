package com.icarasia.leadmarketplace.network;

import android.text.TextUtils;

import com.icarasia.leadmarketplace.network.interceptor.AcceptInterceptor;
import com.icarasia.leadmarketplace.network.interceptor.AuthenticationInterceptor;
import com.icarasia.leadmarketplace.network.interceptor.TokenInterceptor;
import com.icarasia.leadmarketplace.network.interceptor.UserAgentInterceptor;

import okhttp3.Credentials;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestClient {
    private static final String BASE_URL = "https://dev-leadmarketplace.icarasia.com/api/";
    private final String BASIC_AUTH_USERNAME = "dev-lead";
    private final String BASIC_AUTH_PASSWORD = "dev1leadmarketplace1";


    private volatile static RestClient instance = null;

    private RestClient() {
    }

    public static RestClient getInstance() {

        if (instance == null) {
            synchronized (RestClient.class) {
                if (instance == null) {
                    instance = new RestClient();
                }
            }
        }
        return instance;
    }

    public ApiService createService(){
        return createService(null);
    }

    public ApiService createService(String authToken) {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new AuthenticationInterceptor(Credentials.basic(BASIC_AUTH_USERNAME, BASIC_AUTH_PASSWORD)));
        httpClient.addInterceptor(new AcceptInterceptor());
        httpClient.addInterceptor(new UserAgentInterceptor(System.getProperty("http.agent")));
        if (!TextUtils.isEmpty(authToken)) {
            httpClient.addInterceptor(new TokenInterceptor(authToken));
        }
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();
        return retrofit.create(ApiService.class);
    }

}
