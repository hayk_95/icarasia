package com.icarasia.leadmarketplace.network;

import com.icarasia.leadmarketplace.data.models.LeadDetail;
import com.icarasia.leadmarketplace.data.models.User;
import com.icarasia.leadmarketplace.network.body.AccountUpdateBody;
import com.icarasia.leadmarketplace.network.body.LoginBody;
import com.icarasia.leadmarketplace.network.body.RegisterBody;
import com.icarasia.leadmarketplace.network.body.SendCommentBody;
import com.icarasia.leadmarketplace.network.response.AccountUpdateResponse;
import com.icarasia.leadmarketplace.network.response.BaseResponse;
import com.icarasia.leadmarketplace.network.response.CommentResponse;
import com.icarasia.leadmarketplace.network.response.LeadsResponse;
import com.icarasia.leadmarketplace.network.response.SendCommentResponse;
import com.icarasia.leadmarketplace.network.response.StatusResponse;
import com.icarasia.leadmarketplace.network.response.UserInformationResponse;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {
    @POST("auth/login")
    Call<BaseResponse> loginUser(@Body LoginBody body);

    @POST("auth/register")
    Call<BaseResponse> registerUser(@Body RegisterBody body);

    @POST("auth/logout")
    Call<BaseResponse> logOut();

    @GET("auth/user")
    Call<BaseResponse<User>> getUser();

    @GET("leads")
    Call<LeadsResponse> getLeads(@Query("date") String date,
                                 @Query("showRatingGreater") Integer rating,
                                 @Query("showOnly") String status);

    @GET("leads/{lead_id}/show")
    Call<LeadDetail> getLeadDetail(@Path("lead_id") int leadId);

    @GET("leads/comments/{lead_id}")
    Call<CommentResponse> getComments(@Path("lead_id") int leadId);

    @POST("leads/comments/comment")
    Call<SendCommentResponse> sendComment(@Body SendCommentBody body);

    @FormUrlEncoded
    @Headers("Content-Type: application/x-www-form-urlencoded")
    @PUT("leads/status/{lead_id}/update")
    Call<StatusResponse> updateStatus(@Path("lead_id") int leadId, @Field("status") String status);

    @GET("user/fetchData")
    Call<UserInformationResponse> getUserInformation();

    @POST("user/edit-data")
    Call<AccountUpdateResponse> updateAccount(@Body AccountUpdateBody body);

    @Multipart
    @POST("user/upload-avatar")
    Call<BaseResponse> uploadAvatarImage(@Part MultipartBody.Part image, @Part("name") RequestBody name);

    @Multipart
    @POST("payments/pay")
    Call<StatusResponse> processPayment(@Part("package_id") int id,
                                        @Part("card_number") String number,
                                        @Part("card_name") String name,
                                        @Part("expire_date") String date,
                                        @Part("card_cvv") String cvv,
                                        @Part("promo_code") String promoCode,
                                        @Part("save_card") int save);

}
