package com.icarasia.leadmarketplace.network.body;

import com.google.gson.annotations.SerializedName;

public class SendCommentBody {

    @SerializedName("lead_id")
    private int leadId;

    @SerializedName("content")
    private String content;

    public SendCommentBody(int leadId, String content) {
        this.leadId = leadId;
        this.content = content;
    }

    public int getLeadId() {
        return leadId;
    }

    public void setLeadId(int leadId) {
        this.leadId = leadId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
