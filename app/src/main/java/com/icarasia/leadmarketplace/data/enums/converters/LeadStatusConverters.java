package com.icarasia.leadmarketplace.data.enums.converters;

import android.arch.persistence.room.TypeConverter;

import com.icarasia.leadmarketplace.data.enums.LeadStatus;

public class LeadStatusConverters {

    @TypeConverter
    public static LeadStatus getLeadStatus(String status){
        if(status.equals(LeadStatus.READ.getStatusName())){
            return LeadStatus.READ;
        }else if(status.equals(LeadStatus.UNREAD.getStatusName())){
            return LeadStatus.UNREAD;
        }else if(status.equals(LeadStatus.SALE_CLOSED.getStatusName())){
            return LeadStatus.SALE_CLOSED;
        }else if(status.equals(LeadStatus.SALE_IN_PROGRESS.getStatusName())){
            return LeadStatus.SALE_IN_PROGRESS;
        }else if(status.equals(LeadStatus.SALE_FAILED.getStatusName())){
            return LeadStatus.SALE_FAILED;
        }else {
            throw new IllegalArgumentException("Could not recognize status");
        }
    }

    @TypeConverter
    public static String getLeadStatusName(LeadStatus status){
        return status.getStatusName();
    }
}
