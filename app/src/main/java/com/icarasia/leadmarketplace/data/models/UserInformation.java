package com.icarasia.leadmarketplace.data.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class UserInformation {

    @SerializedName("id")
    private int id;

    @SerializedName("name")
    private String name;

    @SerializedName("email")
    private String email;

    @SerializedName("created_at")
    private String createDate;

    @SerializedName("updated_at")
    private String updateDate;

    @SerializedName("phone")
    private String phone;

    @SerializedName("calling_code")
    private String callingCode;

    @SerializedName("location")
    private String location;

    @SerializedName("credits")
    private int credits;

    @SerializedName("position")
    private String position;

    @SerializedName("avatar")
    private String avatar;

    @SerializedName("role")
    private ArrayList<String> role;

    @SerializedName("full_phone")
    private String fullPhone;

    @SerializedName("roles")
    private ArrayList<UserRole> roles;

    @SerializedName("user_settings")
    private UserSettings userSettings;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCallingCode() {
        return callingCode;
    }

    public void setCallingCode(String callingCode) {
        this.callingCode = callingCode;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getCredits() {
        return credits;
    }

    public void setCredits(int credits) {
        this.credits = credits;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public ArrayList<String> getRole() {
        return role;
    }

    public void setRole(ArrayList<String> role) {
        this.role = role;
    }

    public String getFullPhone() {
        return fullPhone;
    }

    public void setFullPhone(String fullPhone) {
        this.fullPhone = fullPhone;
    }

    public ArrayList<UserRole> getRoles() {
        return roles;
    }

    public void setRoles(ArrayList<UserRole> roles) {
        this.roles = roles;
    }

    public UserSettings getUserSettings() {
        return userSettings;
    }

    public void setUserSettings(UserSettings userSettings) {
        this.userSettings = userSettings;
    }

    class UserRole{
        @SerializedName("id")
        private int id;

        @SerializedName("name")
        private String name;

        @SerializedName("guard_name")
        private String guardName;

        @SerializedName("created_at")
        private String createDate;

        @SerializedName("updated_at")
        private String updateDate;

        @SerializedName("pivot")
        private Pivot pivot;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getGuardName() {
            return guardName;
        }

        public void setGuardName(String guardName) {
            this.guardName = guardName;
        }

        public String getCreateDate() {
            return createDate;
        }

        public void setCreateDate(String createDate) {
            this.createDate = createDate;
        }

        public String getUpdateDate() {
            return updateDate;
        }

        public void setUpdateDate(String updateDate) {
            this.updateDate = updateDate;
        }

        public Pivot getPivot() {
            return pivot;
        }

        public void setPivot(Pivot pivot) {
            this.pivot = pivot;
        }

        class Pivot{
            @SerializedName("model_id")
            private int modelId;

            @SerializedName("role_id")
            private int roleId;

            @SerializedName("model_type")
            private String modelType;

            public int getModelId() {
                return modelId;
            }

            public void setModelId(int modelId) {
                this.modelId = modelId;
            }

            public int getRoleId() {
                return roleId;
            }

            public void setRoleId(int roleId) {
                this.roleId = roleId;
            }

            public String getModelType() {
                return modelType;
            }

            public void setModelType(String modelType) {
                this.modelType = modelType;
            }
        }
    }

    class UserSettings{
        @SerializedName("id")
        private int id;

        @SerializedName("user_id")
        private int userId;

        @SerializedName("language")
        private int language;

        @SerializedName("theme")
        private int theme;

        @SerializedName("lead_distribution")
        private int leadDistribution;

        @SerializedName("notify_new_lead")
        private int notifyNewLead;

        @SerializedName("notify_status_change")
        private int notifyStatusChange;

        @SerializedName("notify_sale_success")
        private int notifySaleSuccess;

        @SerializedName("notify_sale_failed")
        private int notifySaleFailed;

        @SerializedName("notify_unresponsed_leads")
        private int notifyUnresponsedLeads;

        @SerializedName("created_at")
        private String createDate;

        @SerializedName("updated_at")
        private String updateDate;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        public int getLanguage() {
            return language;
        }

        public void setLanguage(int language) {
            this.language = language;
        }

        public int getTheme() {
            return theme;
        }

        public void setTheme(int theme) {
            this.theme = theme;
        }

        public int getLeadDistribution() {
            return leadDistribution;
        }

        public void setLeadDistribution(int leadDistribution) {
            this.leadDistribution = leadDistribution;
        }

        public int getNotifyNewLead() {
            return notifyNewLead;
        }

        public void setNotifyNewLead(int notifyNewLead) {
            this.notifyNewLead = notifyNewLead;
        }

        public int getNotifyStatusChange() {
            return notifyStatusChange;
        }

        public void setNotifyStatusChange(int notifyStatusChange) {
            this.notifyStatusChange = notifyStatusChange;
        }

        public int getNotifySaleSuccess() {
            return notifySaleSuccess;
        }

        public void setNotifySaleSuccess(int notifySaleSuccess) {
            this.notifySaleSuccess = notifySaleSuccess;
        }

        public int getNotifySaleFailed() {
            return notifySaleFailed;
        }

        public void setNotifySaleFailed(int notifySaleFailed) {
            this.notifySaleFailed = notifySaleFailed;
        }

        public int getNotifyUnresponsedLeads() {
            return notifyUnresponsedLeads;
        }

        public void setNotifyUnresponsedLeads(int notifyUnresponsedLeads) {
            this.notifyUnresponsedLeads = notifyUnresponsedLeads;
        }

        public String getCreateDate() {
            return createDate;
        }

        public void setCreateDate(String createDate) {
            this.createDate = createDate;
        }

        public String getUpdateDate() {
            return updateDate;
        }

        public void setUpdateDate(String updateDate) {
            this.updateDate = updateDate;
        }
    }
}
