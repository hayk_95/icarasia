package com.icarasia.leadmarketplace.data.enums;

public enum ResponseMessage {
    SUCCESS_LOG_OUT("Successfully logged out"),
    UNAUTHENTICATED("Unauthenticated.");

    private String message;

    ResponseMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public static ResponseMessage getResponseMessage(String text){
        if(text.equals(SUCCESS_LOG_OUT.getMessage())){
            return SUCCESS_LOG_OUT;
        }else if(text.equals(UNAUTHENTICATED.getMessage())){
            return UNAUTHENTICATED;
        }else {
            return null;
        }
    }
}
