package com.icarasia.leadmarketplace.data.enums;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public enum ResponseStatus {
    @Expose
    @SerializedName("success")
    SUCCESS,
    @Expose
    @SerializedName("failed")
    FAILED,
    @Expose
    @SerializedName("unauthorized")
    UNAUTHORIZED,
    @Expose
    @SerializedName("not_colleagues")
    NOT_COLLEAGUES,
    UNKNOWN,
    INCONSISTENT
}
