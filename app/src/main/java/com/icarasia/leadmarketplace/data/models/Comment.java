package com.icarasia.leadmarketplace.data.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Comment {
    @SerializedName("id")
    private int id;
    @SerializedName("lead_id")
    private int leadId;
    @SerializedName("user_id")
    private int userId;
    @SerializedName("parent_id")
    private int parentId;
    @SerializedName("content")
    private String content;
    @SerializedName("created_at")
    private String createTime;
    @SerializedName("updated_at")
    private String updateTime;
    @SerializedName("likes_count")
    private int likeCount;
    @SerializedName("created_at_diff_for_human")
    private String showDiffTime;
    @SerializedName("user_name")
    private String userName;
    @SerializedName("user")
    private User user;
    @SerializedName("sub_comments")
    private ArrayList<Comment> subComments = new ArrayList<>();

    private boolean isLiked;

    public Comment(int id, String content, String showDiffTime, User user) {
        this.id = id;
        this.content = content;
        this.showDiffTime = showDiffTime;
        this.user = user;
        this.userName = user.getName();
    }

    public int getId() {
        return id;
    }

    public int getLeadId() {
        return leadId;
    }

    public void setLeadId(int leadId) {
        this.leadId = leadId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public int getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(int likeCount) {
        this.likeCount = likeCount;
    }

    public String getShowDiffTime() {
        return showDiffTime;
    }

    public void setShowDiffTime(String showDiffTime) {
        this.showDiffTime = showDiffTime;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ArrayList<Comment> getSubComments() {
        return subComments;
    }

    public void setSubComments(ArrayList<Comment> subComments) {
        this.subComments = subComments;
    }

    public boolean isLiked() {
        return isLiked;
    }

    public void setLiked(boolean liked) {
        isLiked = liked;
    }


}
