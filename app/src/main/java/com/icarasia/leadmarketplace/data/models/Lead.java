package com.icarasia.leadmarketplace.data.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;

import com.google.gson.annotations.SerializedName;
import com.icarasia.leadmarketplace.data.enums.LeadStatus;
import com.icarasia.leadmarketplace.data.enums.converters.LeadStatusConverters;

@Entity(tableName = "leads")
public class Lead {
    @PrimaryKey(autoGenerate = true)
    private int id;
    @SerializedName("first_name")
    private String name;
    @SerializedName("mobile_date")
    private String mobileDate;
    @SerializedName("desktop_date")
    private String desktopDate;
    @SerializedName("average_rating")
    private float rating;
    @TypeConverters(LeadStatusConverters.class)
    @SerializedName("status_lead")
    private LeadStatus status;

    public Lead(String name, String mobileDate, float rating, LeadStatus status) {
        this.name = name;
        this.mobileDate = mobileDate;
        this.rating = rating;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobileDate() {
        return mobileDate;
    }

    public void setMobileDate(String mobileDate) {
        this.mobileDate = mobileDate;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public LeadStatus getStatus() {
        return status;
    }

    public void setStatus(LeadStatus status) {
        this.status = status;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setRating(Float rating) {
        this.rating = rating;
    }

    public String getDesktopDate() {
        return desktopDate;
    }

    public void setDesktopDate(String desktopDate) {
        this.desktopDate = desktopDate;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Lead &&
                name.equals(((Lead) obj).name) &&
                mobileDate.equals(((Lead) obj).mobileDate) &&
                desktopDate.equals(((Lead) obj).desktopDate) &&
                rating == ((Lead) obj).rating &&
                status.equals(((Lead) obj).status)){
            return true;
        }else {
            return false;
        }
    }
}
