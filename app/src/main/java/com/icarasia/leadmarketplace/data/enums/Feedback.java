package com.icarasia.leadmarketplace.data.enums;

public enum Feedback {
    NOTHING("Nothing"),
    NO_URGENCY("No urgency"),
    NOT_SHOW_UP("Did not show up"),
    COMMUNICATION("Communication"),
    BUDGET("Budget"),
    LOCATION("Location"),
    OTHER("Other");

    private String feedback;

    Feedback(String feedback){
        this.feedback = feedback;
    }

    public String getFeedback() {
        return feedback;
    }
}
