package com.icarasia.leadmarketplace.data.enums;

import com.google.gson.annotations.SerializedName;

public enum LeadStatus {
    @SerializedName("read")
    READ("READ","read"),
    @SerializedName("unread")
    UNREAD("UNREAD","unread"),
    @SerializedName("success")
    SALE_CLOSED("SALE CLOSED","success"),
    @SerializedName("progress")
    SALE_IN_PROGRESS("SALE IN PROGRESS","progress"),
    @SerializedName("failed")
    SALE_FAILED("SALE FAILED","failed"),
    @SerializedName("archived")
    ARCHIVED("ARCHIVED","archived");

    private String statusName;
    private String serializeName;

    LeadStatus(String statusName,String serializeName) {
        this.statusName = statusName;
        this.serializeName = serializeName;
    }

    public String getStatusName() {
        return statusName;
    }

    public String getSerializeName() {
        return serializeName;
    }

    public static LeadStatus getStatusByName(String statusName) {
        statusName = statusName.toUpperCase();
        if (statusName.equals(READ.getStatusName())) {
            return READ;
        } else if (statusName.equals(UNREAD.getStatusName())) {
            return UNREAD;
        } else if (statusName.equals(SALE_CLOSED.getStatusName())) {
            return SALE_CLOSED;
        } else if (statusName.equals(SALE_IN_PROGRESS.getStatusName())) {
            return SALE_IN_PROGRESS;
        } else if (statusName.equals(SALE_FAILED.getStatusName())) {
            return SALE_FAILED;
        } else if (statusName.equals(ARCHIVED.getStatusName())) {
            return ARCHIVED;
        }else {
            return null;
        }
    }

    public static LeadStatus getStatusBySerializeName(String statusName) {
        if (statusName.equals(READ.getSerializeName())) {
            return READ;
        } else if (statusName.equals(UNREAD.getSerializeName())) {
            return UNREAD;
        } else if (statusName.equals(SALE_CLOSED.getSerializeName())) {
            return SALE_CLOSED;
        } else if (statusName.equals(SALE_IN_PROGRESS.getSerializeName())) {
            return SALE_IN_PROGRESS;
        } else if (statusName.equals(SALE_FAILED.getSerializeName())) {
            return SALE_FAILED;
        }else if (statusName.equals(ARCHIVED.getSerializeName())) {
            return ARCHIVED;
        } else {
            return null;
        }
    }

    public static Integer getStatusNumber(LeadStatus status) {
        switch (status) {
            case UNREAD:
                return 1;
            case READ:
                return 2;
            case SALE_IN_PROGRESS:
                return 3;
            case ARCHIVED:
                return 4;
            case SALE_CLOSED:
                return 5;
            case SALE_FAILED:
                return 6;
            default:
                return 0;
        }
    }

    public static LeadStatus getLeadStatusByNumber(Integer number) {
        switch (number) {
            case 1:
                return UNREAD;
            case 2:
                return READ;
            case 3:
                return SALE_IN_PROGRESS;
            case 4:
                return ARCHIVED;
            case 5:
                return SALE_CLOSED;
            case 6:
                return SALE_FAILED;
            default:
                return null;
        }
    }
}
