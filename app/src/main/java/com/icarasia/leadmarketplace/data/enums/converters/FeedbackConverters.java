package com.icarasia.leadmarketplace.data.enums.converters;

import android.arch.persistence.room.TypeConverter;

import com.icarasia.leadmarketplace.data.enums.Feedback;

public class FeedbackConverters {

    @TypeConverter
    public static Feedback getFeedback(String feedback){
        if(feedback.equals(Feedback.NOTHING.getFeedback())){
            return Feedback.NOTHING;
        }else if(feedback.equals(Feedback.NO_URGENCY.getFeedback())){
            return Feedback.NO_URGENCY;
        }else if(feedback.equals(Feedback.NOT_SHOW_UP.getFeedback())){
            return Feedback.NOT_SHOW_UP;
        }else if(feedback.equals(Feedback.COMMUNICATION.getFeedback())){
            return Feedback.COMMUNICATION;
        }else if(feedback.equals(Feedback.BUDGET.getFeedback())){
            return Feedback.BUDGET;
        }else if(feedback.equals(Feedback.LOCATION.getFeedback())){
            return Feedback.LOCATION;
        }else if(feedback.equals(Feedback.OTHER.getFeedback())){
            return Feedback.OTHER;
        }else {
            throw new IllegalArgumentException("Could not recognize status");
        }
    }

    @TypeConverter
    public static String getFeedbackName(Feedback feedback){
        return feedback.getFeedback();
    }
}
