package com.icarasia.leadmarketplace.data.models;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class Notification {
    @NonNull
    private int number;
    @Nullable
    private Integer count;
    private String message;
    private String time;
    private boolean isNew;

    public Notification(@NonNull int number, @Nullable Integer count, String message,String time,boolean isNew) {
        this.number = number;
        this.count = count;
        this.message = message;
        this.time = time;
        this.isNew = isNew;
    }

    @NonNull
    public int getNumber() {
        return number;
    }

    public void setNumber(@NonNull int number) {
        this.number = number;
    }

    @Nullable
    public Integer getCount() {
        return count;
    }

    public void setCount(@Nullable Integer count) {
        this.count = count;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public boolean isNew() {
        return isNew;
    }

    public void setNew(boolean aNew) {
        isNew = aNew;
    }
}
