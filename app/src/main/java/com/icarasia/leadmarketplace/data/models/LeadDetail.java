package com.icarasia.leadmarketplace.data.models;

import com.google.gson.annotations.SerializedName;
import com.icarasia.leadmarketplace.data.enums.LeadStatus;

public class LeadDetail {
    @SerializedName("id")
    private int id;
    @SerializedName("first_name")
    private String name;
    @SerializedName("email")
    private String email;
    @SerializedName("phone")
    private String phone;
    @SerializedName("desktop_date")
    private String date;
    @SerializedName("country_state_name")
    private String location;
    @SerializedName("model_name")
    private String modelName;
    @SerializedName("monthly_income")
    private int monthlyIncome;
    @SerializedName("monthly_expenses")
    private int monthlyExpenses;
    @SerializedName("insurance")
    private String insurance;
    @SerializedName("make")
    private String make;
    @SerializedName("model")
    private String model;
    @SerializedName("year")
    private String year;
    @SerializedName("status_lead")
    private LeadStatus status;
    @SerializedName("average_rating")
    private float rating;
    @SerializedName("is_given_feedback")
    private int isGivenFeedback;
    @SerializedName("is_assigned")
    private int isAssigned;
    @SerializedName("urgency")
    private String urgency;


    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public int getMonthlyIncome() {
        return monthlyIncome;
    }

    public void setMonthlyIncome(int monthlyIncome) {
        this.monthlyIncome = monthlyIncome;
    }

    public int getMonthlyExpenses() {
        return monthlyExpenses;
    }

    public void setMonthlyExpenses(int monthlyExpenses) {
        this.monthlyExpenses = monthlyExpenses;
    }

    public String getInsurance() {
        return insurance;
    }

    public void setInsurance(String insurance) {
        this.insurance = insurance;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public LeadStatus getStatus() {
        return status;
    }

    public void setStatus(LeadStatus status) {
        this.status = status;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public int getIsGivenFeedback() {
        return isGivenFeedback;
    }

    public void setIsGivenFeedback(int isGivenFeedback) {
        this.isGivenFeedback = isGivenFeedback;
    }

    public int getIsAssigned() {
        return isAssigned;
    }

    public void setIsAssigned(int isAssigned) {
        this.isAssigned = isAssigned;
    }

    public String getUrgency() {
        return urgency;
    }

    public void setUrgency(String urgency) {
        this.urgency = urgency;
    }
}
