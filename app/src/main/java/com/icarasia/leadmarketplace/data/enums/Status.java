package com.icarasia.leadmarketplace.data.enums;

public enum Status {
    SUCCESS,
    ERROR,
    LOADING
}
