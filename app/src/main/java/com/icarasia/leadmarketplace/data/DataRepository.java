package com.icarasia.leadmarketplace.data;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.icarasia.leadmarketplace.AppExecutors;
import com.icarasia.leadmarketplace.Constants;
import com.icarasia.leadmarketplace.data.enums.ResponseStatus;
import com.icarasia.leadmarketplace.data.models.Comment;
import com.icarasia.leadmarketplace.data.models.Lead;
import com.icarasia.leadmarketplace.data.models.LeadDetail;
import com.icarasia.leadmarketplace.data.models.Notification;
import com.icarasia.leadmarketplace.data.models.User;
import com.icarasia.leadmarketplace.data.models.UserInformation;
import com.icarasia.leadmarketplace.data.room.AppDatabase;
import com.icarasia.leadmarketplace.data.room.daos.LeadDao;
import com.icarasia.leadmarketplace.network.ApiService;
import com.icarasia.leadmarketplace.network.NetworkBoundResource;
import com.icarasia.leadmarketplace.network.NetworkBoundStatusResource;
import com.icarasia.leadmarketplace.network.RestClient;
import com.icarasia.leadmarketplace.network.body.AccountUpdateBody;
import com.icarasia.leadmarketplace.network.body.LoginBody;
import com.icarasia.leadmarketplace.network.body.RegisterBody;
import com.icarasia.leadmarketplace.network.body.SendCommentBody;
import com.icarasia.leadmarketplace.network.interceptor.TokenInterceptor;
import com.icarasia.leadmarketplace.network.response.AccountUpdateResponse;
import com.icarasia.leadmarketplace.network.response.BaseResponse;
import com.icarasia.leadmarketplace.network.response.CommentResponse;
import com.icarasia.leadmarketplace.network.response.LeadsResponse;
import com.icarasia.leadmarketplace.network.response.SendCommentResponse;
import com.icarasia.leadmarketplace.network.response.StatusResponse;
import com.icarasia.leadmarketplace.network.response.UserInformationResponse;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DataRepository {

    private static DataRepository instance;
    private AppDatabase database;
    private SharedPreferences sharedPreferences;
    private LeadDao leadDao;
    private ApiService service;
    private AppExecutors appExecutors;

    private DataRepository(final AppDatabase database, final SharedPreferences sharedPreferences) {
        this.database = database;
        this.sharedPreferences = sharedPreferences;
        leadDao = database.leadDao();
        service = RestClient.getInstance().createService(getUserToken());
        appExecutors = new AppExecutors();
    }

    public static DataRepository getInstance(final AppDatabase database, final SharedPreferences sharedPreferences) {

        if (instance == null) {
            synchronized (DataRepository.class) {
                if (instance == null) {
                    instance = new DataRepository(database, sharedPreferences);
                }
            }
        }
        return instance;
    }

    public static DataRepository getInstance(){
        return instance;
    }


    //Network requests

    public MutableLiveData<StatusResource> login(final LoginBody credentials) {
        MutableLiveData<StatusResource> liveData;
        liveData = new NetworkBoundStatusResource() {
            @Override
            protected void createCall() {
                service.loginUser(credentials).enqueue(new Callback<BaseResponse>() {
                    @Override
                    public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {

                        if (response.body() != null) {

                            BaseResponse iCarResponse = response.body();
                            switch (iCarResponse.getStatus()) {
                                case SUCCESS:
                                    saveUserToken(response.headers().get(TokenInterceptor.AUTH_TOKEN_HEADER_KEY));
                                    service = RestClient.getInstance().createService(response.headers().get(TokenInterceptor.AUTH_TOKEN_HEADER_KEY));
                                    setResultValue(StatusResource.success());
                                    break;
                                case FAILED:
                                    setResultValue(StatusResource.error(iCarResponse.getMessage()));
                                    break;
                                case UNAUTHORIZED:
                                    setResultValue(StatusResource.error(ResponseStatus.UNAUTHORIZED.name()));
                                    break;
                            }
                        } else {
                            setResultValue(StatusResource.error(ResponseStatus.UNKNOWN.name()));
                        }
                    }

                    @Override
                    public void onFailure(Call<BaseResponse> call, Throwable t) {
                        setResultValue(StatusResource.error(t.getMessage()));
                    }
                });
            }
        }.getAsMutableLiveData();

        return liveData;
    }

    public MutableLiveData<StatusResource> registerUser(final RegisterBody credentials) {
        MutableLiveData<StatusResource> liveData;
        liveData = new NetworkBoundStatusResource() {
            @Override
            protected void createCall() {
                service.registerUser(credentials).enqueue(new Callback<BaseResponse>() {
                    @Override
                    public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                        if (response.body() != null) {
                            BaseResponse body = response.body();
                        }
                    }

                    @Override
                    public void onFailure(Call<BaseResponse> call, Throwable t) {

                    }
                });
            }
        }.getAsMutableLiveData();

        return liveData;
    }

    public MutableLiveData<StatusResource> logOut() {
        MutableLiveData<StatusResource> liveData;
        liveData = new NetworkBoundStatusResource() {
            @Override
            protected void createCall() {
                service.logOut().enqueue(new Callback<BaseResponse>() {
                    @Override
                    public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                        if (response.isSuccessful()) {
                            sharedPreferences.edit().putString(Constants.AUTH_TOKEN, "").apply();
                            saveFilters("","",0,"");
                            service = RestClient.getInstance().createService();
                            setResultValue(StatusResource.success());
                        } else {
                            setResultValue(StatusResource.error(response.message()));
                        }
                    }

                    @Override
                    public void onFailure(Call<BaseResponse> call, Throwable t) {
                        setResultValue(StatusResource.error(t.getMessage()));
                    }
                });
            }
        }.

                getAsMutableLiveData();
        return liveData;
    }

    public MutableLiveData<Resource<User>> getUser() {
        MutableLiveData<Resource<User>> liveData;
        liveData = new NetworkBoundResource<User>() {
            @Override
            protected void createCall() {
                service.getUser().enqueue(new Callback<BaseResponse<User>>() {
                    @Override
                    public void onResponse(Call<BaseResponse<User>> call, Response<BaseResponse<User>> response) {
                        if (response.isSuccessful() && response.body() != null) {
                            setResultValue(Resource.success(response.body().getData()));
                        } else {
                            setResultValue(Resource.error(response.message(), null));
                        }
                    }

                    @Override
                    public void onFailure(Call<BaseResponse<User>> call, Throwable t) {
                        setResultValue(Resource.error(t.getMessage(), null));
                    }
                });
            }
        }.getAsMutableLiveData();

        return liveData;
    }

    public LiveData<Resource<List<Lead>>> getLeads() {
//        LiveData<PagedList<Lead>> listLiveData = new LivePagedListBuilder<>(leadDao.getLeads(), 20).build();
        MutableLiveData<Resource<List<Lead>>> liveData;
        String date;
        if (!getLeadsDateFromFilter().equals("")
                && !getLeadsDateToFilter().equals("")) {
            date = "{\"start\":\""
                    + getLeadsDateFromFilter()
                    + " 00:00:00\",\"end\":\""
                    + getLeadsDateToFilter()
                    + " 00:00:00\"}";
        } else {
            date = null;
        }
        Integer rating = getRatingFilter() != 0 ? getRatingFilter() : null;
        String status = !getStatusFilter().equals("") ? getStatusFilter() : null;

        liveData = new NetworkBoundResource<List<Lead>>() {
            @Override
            protected void createCall() {
                service.getLeads(date, rating, status).enqueue(new Callback<LeadsResponse>() {
                    @Override
                    public void onResponse(Call<LeadsResponse> call, Response<LeadsResponse> response) {
                        if (response.isSuccessful()) {
                            setResultValue(Resource.success(response.body().getLeads()));
                        } else {
                            setResultValue(Resource.error(response.message(), null));
                        }
                    }

                    @Override
                    public void onFailure(Call<LeadsResponse> call, Throwable t) {
                        setResultValue(Resource.error(t.getMessage(), null));
                    }
                });

            }
        }.getAsMutableLiveData();

        return liveData;
    }

    public LiveData<Resource<LeadDetail>> getLeadDetail(int id) {
        MutableLiveData<Resource<LeadDetail>> liveData;
        liveData = new NetworkBoundResource<LeadDetail>() {
            @Override
            protected void createCall() {
                service.getLeadDetail(id).enqueue(new Callback<LeadDetail>() {
                    @Override
                    public void onResponse(Call<LeadDetail> call, Response<LeadDetail> response) {
                        if (response.isSuccessful()) {
                            setResultValue(Resource.success(response.body()));
                        } else {
                            setResultValue(Resource.error(response.message(), null));
                        }
                    }

                    @Override
                    public void onFailure(Call<LeadDetail> call, Throwable t) {
                        setResultValue(Resource.error(t.getMessage(), null));
                    }
                });
            }
        }.getAsMutableLiveData();
        return liveData;
    }

    public MutableLiveData<Resource<List<Comment>>> getComments(int leadId) {
        MutableLiveData<Resource<List<Comment>>> liveData;
        liveData = new NetworkBoundResource<List<Comment>>() {
            @Override
            protected void createCall() {
                service.getComments(leadId).enqueue(new Callback<CommentResponse>() {
                    @Override
                    public void onResponse(Call<CommentResponse> call, Response<CommentResponse> response) {
                        if (response.isSuccessful()) {
                            setResultValue(Resource.success(response.body().getComments()));
                        } else {
                            setResultValue(Resource.error(response.message(), null));
                        }
                    }

                    @Override
                    public void onFailure(Call<CommentResponse> call, Throwable t) {
                        setResultValue(Resource.error(t.getMessage(), null));
                    }
                });
            }
        }.getAsMutableLiveData();

        return liveData;
    }

    public MutableLiveData<Resource<SendCommentResponse>> sendComment(int leadId,String content){
        MutableLiveData<Resource<SendCommentResponse>> liveData;
        liveData = new NetworkBoundResource<SendCommentResponse>() {
            @Override
            protected void createCall() {
                service.sendComment(new SendCommentBody(leadId,content)).enqueue(new Callback<SendCommentResponse>() {
                    @Override
                    public void onResponse(Call<SendCommentResponse> call, Response<SendCommentResponse> response) {
                        if(response.isSuccessful()){
                            setResultValue(Resource.success(response.body()));
                        }else {
                            setResultValue(Resource.error(response.message(),null));
                        }
                    }

                    @Override
                    public void onFailure(Call<SendCommentResponse> call, Throwable t) {
                        setResultValue(Resource.error(t.getMessage(),null));
                    }
                });
            }
        }.getAsMutableLiveData();

        return liveData;
    }

    public MutableLiveData<Resource<StatusResponse>> updateStatus(int leadId,String status){
        MutableLiveData<Resource<StatusResponse>> liveData;
        liveData = new NetworkBoundResource<StatusResponse>() {
            @Override
            protected void createCall() {
                service.updateStatus(leadId,status).enqueue(new Callback<StatusResponse>() {
                    @Override
                    public void onResponse(Call<StatusResponse> call, Response<StatusResponse> response) {
                        if(response.isSuccessful()){
                            setResultValue(Resource.success(response.body()));
                        }else {
                            setResultValue(Resource.error(response.message(),null));
                        }
                    }

                    @Override
                    public void onFailure(Call<StatusResponse> call, Throwable t) {
                        setResultValue(Resource.error(t.getMessage(),null));
                    }
                });
            }
        }.getAsMutableLiveData();

        return liveData;
    }

    public MutableLiveData<Resource<UserInformation>> getUserInformation(){
        MutableLiveData<Resource<UserInformation>> liveData;
        liveData = new NetworkBoundResource<UserInformation>() {
            @Override
            protected void createCall() {
                service.getUserInformation().enqueue(new Callback<UserInformationResponse>() {
                    @Override
                    public void onResponse(Call<UserInformationResponse> call, Response<UserInformationResponse> response) {
                        if(response.isSuccessful() && response.body() != null){
                            setResultValue(Resource.success(response.body().getUserInformation()));
                        }else {
                            setResultValue(Resource.error(response.message(),null));
                        }
                    }

                    @Override
                    public void onFailure(Call<UserInformationResponse> call, Throwable t) {
                        setResultValue(Resource.error(t.getMessage(),null));
                    }
                });
            }
        }.getAsMutableLiveData();

        return liveData;
    }

    public MutableLiveData<Resource<AccountUpdateResponse>> updateUserAccount(AccountUpdateBody body) {
        MutableLiveData<Resource<AccountUpdateResponse>> liveData;
        liveData = new NetworkBoundResource<AccountUpdateResponse>() {
            @Override
            protected void createCall() {
                service.updateAccount(body).enqueue(new Callback<AccountUpdateResponse>() {
                    @Override
                    public void onResponse(Call<AccountUpdateResponse> call, Response<AccountUpdateResponse> response) {
                        if(response.isSuccessful()){
                            setResultValue(Resource.success(response.body()));
                        }else {
                            setResultValue(Resource.error(response.message(),null));
                        }
                    }

                    @Override
                    public void onFailure(Call<AccountUpdateResponse> call, Throwable t) {
                        setResultValue(Resource.error(t.getMessage(),null));
                    }
                });
            }
        }.getAsMutableLiveData();

        return liveData;
    }

    public MutableLiveData<StatusResource> uploadAvatarImage(File file){

//        // Create a request body with file and image media type
//        RequestBody fileReqBody = RequestBody.create(MediaType.parse("image/jpeg"), file);
//        // Create MultipartBody.Part using file request-body,file name and part name
//        MultipartBody.Part part = MultipartBody.Part.createFormData("file", file.getName(), fileReqBody);
//        //Create request body with text description and text media type
//        RequestBody description = RequestBody.create(MediaType.parse("form-data"), "file");
//
//
//        RequestBody fileReqBody = RequestBody.create(MediaType.parse("image/jpeg"), file);
        // MultipartBody.Part is used to send also the actual file name
//        MultipartBody.Part body =
//                MultipartBody.Part.createFormData("file", file.getName(), fileReqBody);

        // add another part within the multipart request
//        String descriptionString = "example";
//        RequestBody description =
//                RequestBody.create(
//                        okhttp3.MultipartBody.FORM, descriptionString);

        RequestBody reqFile = RequestBody.create(MediaType.parse("image/jpeg"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("file", file.getName(), reqFile);
        RequestBody name = RequestBody.create(MediaType.parse("text/plain"), "upload_test");

        MutableLiveData<StatusResource> liveData;
        liveData = new NetworkBoundStatusResource() {
            @Override
            protected void createCall() {
                service.uploadAvatarImage(body,name).enqueue(new Callback<BaseResponse>() {
                    @Override
                    public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                        if(response.isSuccessful()){
                            setResultValue(StatusResource.success());
                        }else {
                            setResultValue(StatusResource.error(response.message()));
                        }
                    }

                    @Override
                    public void onFailure(Call<BaseResponse> call, Throwable t) {
                        setResultValue(StatusResource.error(t.getMessage()));
                    }
                });
            }
        }.getAsMutableLiveData();

        return liveData;
    }

    public MutableLiveData<Resource<StatusResponse>> processPayment(int id,String number,String name,String date,String cvv,String promoCode,int save){
        MutableLiveData<Resource<StatusResponse>> liveData;
        liveData = new NetworkBoundResource<StatusResponse>() {
            @Override
            protected void createCall() {
                service.processPayment(id,number,name,date,cvv,promoCode,save).enqueue(new Callback<StatusResponse>() {
                    @Override
                    public void onResponse(Call<StatusResponse> call, Response<StatusResponse> response) {
                        if (response.isSuccessful()) {
                            setResultValue(Resource.success(response.body()));
                        } else {
                            setResultValue(Resource.error(response.message(),null));
                        }
                    }

                    @Override
                    public void onFailure(Call<StatusResponse> call, Throwable t) {
                        setResultValue(Resource.error(t.getMessage(),null));
                    }
                });
            }
        }.getAsMutableLiveData();

        return liveData;
    }

    public MutableLiveData<StatusResource> sendVerification(String code) {
        MutableLiveData<StatusResource> liveData;
        liveData = new NetworkBoundStatusResource() {
            @Override
            protected void createCall() {
                //TODO Need api for request to server and get real information

                setResultValue(StatusResource.success());
            }
        }.getAsMutableLiveData();

        return liveData;
    }

    public MutableLiveData<StatusResource> sendEmailForVerification(String email) {
        MutableLiveData<StatusResource> liveData;
        liveData = new NetworkBoundStatusResource() {
            @Override
            protected void createCall() {
                //TODO Need api for request to server and get real information

                setResultValue(StatusResource.success());
            }
        }.getAsMutableLiveData();

        return liveData;
    }

    public MutableLiveData<StatusResource> changePassword(String oldP, String newP) {
        MutableLiveData<StatusResource> liveData;
        liveData = new NetworkBoundStatusResource() {
            @Override
            protected void createCall() {
                //TODO Need api for request to server and get real information

                setResultValue(StatusResource.success());
            }
        }.getAsMutableLiveData();

        return liveData;
    }

//    public void saveLeads() {
//        ArrayList<Lead> leads = new ArrayList<>();
//        for (int i = 0; i < 8; i++) {
//            leads.add(new Lead("Charlie Houston", "04-07-2018", 4.5f, LeadStatus.UNREAD));
//            leads.get(leads.size() - 1).setEmail("stephen_ramos@gmail.com");
//            leads.get(leads.size() - 1).setTelephone("+60-472-8262");
//            leads.add(new Lead("Nelly Cox", "04-07-2018", 4.5f, LeadStatus.UNREAD));
//            leads.get(leads.size() - 1).setEmail("stephen_ramos@gmail.com");
//            leads.get(leads.size() - 1).setTelephone("+60-472-8262");
//            leads.add(new Lead("Clyde Blair", "04-07-2018", 4.5f, LeadStatus.READ));
//            leads.get(leads.size() - 1).setEmail("stephen_ramos@gmail.com");
//            leads.get(leads.size() - 1).setTelephone("+60-472-8262");
//            leads.add(new Lead("Randal Brady", "04-07-2018", 4.5f, LeadStatus.SALE_IN_PROGRESS));
//            leads.get(leads.size() - 1).setEmail("stephen_ramos@gmail.com");
//            leads.get(leads.size() - 1).setTelephone("+60-472-8262");
//            leads.add(new Lead("Max Blair", "04-07-2018", 4.5f, LeadStatus.SALE_CLOSED));
//            leads.get(leads.size() - 1).setEmail("stephen_ramos@gmail.com");
//            leads.get(leads.size() - 1).setTelephone("+60-472-8262");
//            leads.add(new Lead("Chase Lowe", "04-07-2018", 4.5f, LeadStatus.SALE_FAILED));
//            leads.get(leads.size() - 1).setEmail("stephen_ramos@gmail.com");
//            leads.get(leads.size() - 1).setTelephone("+60-472-8262");
//        }
//        appExecutors.diskIO().execute(() -> leadDao.insertLeads(leads));
//    }

    public MutableLiveData<Resource<List<Notification>>> getNotifications() {
        MutableLiveData<Resource<List<Notification>>> liveData;
        liveData = new NetworkBoundResource<List<Notification>>() {
            @Override
            protected void createCall() {
                //TODO Need api for request to server and get real information

                List<Notification> list = new ArrayList<>();
                for (int i = 0; i < 8; i++) {
                    list.add(new Notification(100 + i, 2, "new comment from seller", "2 minutes ago", i == 0));
                    list.add(new Notification(200 + i, null, "Updated status", "1 hour ago", i == 0));
                    list.add(new Notification(300 + i, 1, "comments added by seller", "3 hours ago ago", i == 0));
                    list.add(new Notification(400 + i, 1, "new comment from seller", "5 minutes ago", i == 0));
                }
                setResultValue(Resource.success(list));
            }
        }.getAsMutableLiveData();

        return liveData;
    }

    //Database connection

//    public void updateFeedback(int id, Feedback feedback) {
//        appExecutors.diskIO().execute(() -> leadDao.updateFeedback(feedback, id));
//    }

    //Shared Prefs Calls
    public boolean isUserExist() {
        return !TextUtils.isEmpty(sharedPreferences.getString(Constants.AUTH_TOKEN, ""));
    }

    public void saveUserToken(String token){
        sharedPreferences.edit().putString(Constants.AUTH_TOKEN, token).apply();
    }

    public String getUserToken() {
        return sharedPreferences.getString(Constants.AUTH_TOKEN, "");
    }

    public int getRatingFilter() {
        return sharedPreferences.getInt(Constants.RATING_FILTER_PREF, 0);
    }

    public String getStatusFilter() {
        return sharedPreferences.getString(Constants.STATUS_FILTER_PREF, "");
    }

    public String getLeadsDateFromFilter() {
        return sharedPreferences.getString(Constants.LEADS_DATE_FROM_FILTER_PREF, "");
    }

    public String getLeadsDateToFilter() {
        return sharedPreferences.getString(Constants.LEADS_DATE_TO_FILTER_PREF, "");
    }

    public void saveFilters(String dateFrom, String dateTo, int ratingFilter, String
            statusFilter) {
        sharedPreferences.edit()
                .putString(Constants.LEADS_DATE_FROM_FILTER_PREF, dateFrom)
                .putString(Constants.LEADS_DATE_TO_FILTER_PREF, dateTo)
                .putInt(Constants.RATING_FILTER_PREF, ratingFilter)
                .putString(Constants.STATUS_FILTER_PREF, statusFilter)
                .apply();
    }
}
