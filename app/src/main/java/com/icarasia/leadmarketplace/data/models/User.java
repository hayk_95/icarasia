package com.icarasia.leadmarketplace.data.models;

import java.util.ArrayList;

public class User {
    private int id;
    private int auth_dealership_id;
    private String name;
    private String avatar;
    private ArrayList<String> role;
    private int credits;
    private String phone;
    private String email;

    public User() {
        role = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAuth_dealership_id() {
        return auth_dealership_id;
    }

    public void setAuth_dealership_id(int auth_dealership_id) {
        this.auth_dealership_id = auth_dealership_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public ArrayList<String> getRole() {
        return role;
    }

    public void setRole(ArrayList<String> role) {
        this.role = role;
    }

    public int getCredits() {
        return credits;
    }

    public void setCredits(int credits) {
        this.credits = credits;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void addRole(String singleRole){
        role.add(singleRole);
    }
}
