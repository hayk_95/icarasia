package com.icarasia.leadmarketplace.data.room.daos;

import android.arch.lifecycle.LiveData;
import android.arch.paging.DataSource;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.TypeConverters;
import android.arch.persistence.room.Update;

import com.icarasia.leadmarketplace.data.enums.LeadStatus;
import com.icarasia.leadmarketplace.data.enums.converters.LeadStatusConverters;
import com.icarasia.leadmarketplace.data.models.Lead;

import java.util.ArrayList;

@Dao
public interface LeadDao {
    @Query("SELECT * FROM leads")
    DataSource.Factory<Integer, Lead> getLeads();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertLead(Lead lead);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertLeads(ArrayList<Lead> leads);

    @Delete
    void deleteLead(Lead lead);

    @Query("DELETE FROM leads")
    void deleteAllLeads();

    @Update
    void updateLead(Lead lead);

    @Query("SELECT * FROM leads WHERE id = :leadId")
    LiveData<Lead> getLeadById(int leadId);

    @Query("UPDATE leads SET status = :newStatus WHERE id = :leadId")
    void updateStatus(@TypeConverters({LeadStatusConverters.class}) LeadStatus newStatus, int leadId);

//    @Query("UPDATE leads SET feedback = :feedback WHERE id = :leadId")
//    void updateFeedback(@TypeConverters({FeedbackConverters.class})Feedback feedback, int leadId);
}
