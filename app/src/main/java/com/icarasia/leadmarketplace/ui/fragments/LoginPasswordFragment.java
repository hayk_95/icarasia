package com.icarasia.leadmarketplace.ui.fragments;


import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.icarasia.leadmarketplace.R;
import com.icarasia.leadmarketplace.ui.activities.BaseActivity;
import com.icarasia.leadmarketplace.ui.activities.GuidelineActivity;
import com.icarasia.leadmarketplace.viewmodels.LoginViewModel;

public class LoginPasswordFragment extends Fragment {

    private EditText editPassword;
    private TextView signIn;
    private ImageView passwordIcon;

    private LoginViewModel model;

    public static LoginPasswordFragment newInstance() {

        Bundle args = new Bundle();
        LoginPasswordFragment fragment = new LoginPasswordFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login_password, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
        bindModelData();
        setListeners();
    }

    private void initViews(View view){
        editPassword = view.findViewById(R.id.enter_password);
        signIn = view.findViewById(R.id.sign_in);
        passwordIcon = view.findViewById(R.id.password_icon);
    }

    private void bindModelData(){
        model = ViewModelProviders.of(getActivity()).get(LoginViewModel.class);

        model.getLoginResponse().observe(this, statusResource -> {
            switch (statusResource.status){
                case SUCCESS:
                    ((BaseActivity) getActivity()).dismissCurrentDialog();
                    Intent intent = new Intent(getActivity(),GuidelineActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    break;
                case ERROR:
                    ((BaseActivity) getActivity()).dismissCurrentDialog();
                    if(statusResource.message != null){
                        ((BaseActivity) getActivity()).showErrorDialog(statusResource.message);
                    }else {
                        ((BaseActivity) getActivity()).showErrorDialog();
                    }
                    break;
                case LOADING:
                    ((BaseActivity) getActivity()).showLoadingDialog();
                    break;
            }
        });
    }

    private void setListeners(){
        signIn.setOnClickListener(view ->
                model.setPassword(editPassword.getText().toString()));

        editPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(TextUtils.isEmpty(editPassword.getText().toString())){
                    passwordIcon.setVisibility(View.VISIBLE);
                }else {
                    passwordIcon.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }
}
