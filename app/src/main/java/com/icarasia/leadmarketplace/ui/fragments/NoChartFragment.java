package com.icarasia.leadmarketplace.ui.fragments;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.icarasia.leadmarketplace.R;

public class NoChartFragment extends Fragment {

    private ImageView back;
    private TextView comeBackText;

    public static NoChartFragment newInstance() {
        Bundle args = new Bundle();
        NoChartFragment fragment = new NoChartFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_no_chart, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
        setListeners();
    }

    private void initViews(View view){
        back = view.findViewById(R.id.back);
        comeBackText = view.findViewById(R.id.come_back_text);
//        String text = getString(R.string.come_back_in_23_days);
        String text = "Please come back in soon";
        SpannableString changedText = new SpannableString(text);
        changedText.setSpan(new ForegroundColorSpan(Color.parseColor("#FD2AA6")), 20,text.length(), 0);
        comeBackText.setText(changedText);
    }

    private void setListeners(){
        back.setOnClickListener(v -> getFragmentManager().popBackStack());
    }
}
