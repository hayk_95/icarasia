package com.icarasia.leadmarketplace.ui.fragments;


import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.icarasia.leadmarketplace.R;
import com.icarasia.leadmarketplace.adapters.LeadsAdapter;
import com.icarasia.leadmarketplace.ui.activities.BaseActivity;
import com.icarasia.leadmarketplace.ui.activities.FiltersActivity;
import com.icarasia.leadmarketplace.ui.activities.LeadActivity;
import com.icarasia.leadmarketplace.ui.activities.MainActivity;
import com.icarasia.leadmarketplace.viewmodels.LeadsViewModel;

import static android.app.Activity.RESULT_OK;

public class LeadsFragment extends Fragment implements LeadsAdapter.OnLeadAdapterItemClickListener {
    public static final int FILTER_REQUEST_CODE = 101;

    private ImageView menu;
    private RecyclerView leadsList;
    private ImageView filters;

    private LeadsViewModel model;
    private LeadsAdapter adapter;

    public static LeadsFragment newInstance() {

        Bundle args = new Bundle();
        LeadsFragment fragment = new LeadsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_leads, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
        setListeners();
        initUI();
    }

    @Override
    public void onStart() {
        super.onStart();
        bindModelData();
    }

    private void initViews(View view) {
        menu = view.findViewById(R.id.menu);
        leadsList = view.findViewById(R.id.leads_list);
        filters = view.findViewById(R.id.filters);
    }

    private void setListeners() {
        menu.setOnClickListener(v ->
                ((MainActivity) getActivity()).setDrawerOpen(true));

        filters.setOnClickListener(v ->
                startActivityForResult(new Intent(getActivity(), FiltersActivity.class),FILTER_REQUEST_CODE));
    }

    private void initUI() {
        adapter = new LeadsAdapter(getActivity());
        leadsList.setLayoutManager(new LinearLayoutManager(getActivity()));
        leadsList.setAdapter(adapter);
        adapter.setOnLeadAdapterItemClickListener(this);
    }

    private void bindModelData() {
        model = ViewModelProviders.of(this).get(LeadsViewModel.class);

        model.getLeads().observe(this, listResource -> {
            if (listResource != null) {
                switch (listResource.status) {
                    case SUCCESS:
                        ((BaseActivity) getActivity()).dismissCurrentDialog();
                        adapter.updateLeads(listResource.data);
                        break;
                    case ERROR:
                        ((BaseActivity) getActivity()).dismissCurrentDialog();
                        if (listResource.message != null) {
                            ((BaseActivity) getActivity()).showErrorDialog(listResource.message);
                        } else {
                            ((BaseActivity) getActivity()).showErrorDialog();
                        }
                        break;
                    case LOADING:
                        ((BaseActivity) getActivity()).showLoadingDialog();
                        break;
                }
            }
        });
    }

    private void showLeadGuide() {

    }

    @Override
    public void onLeadItemClicked(int id) {
        Intent intent = new Intent(getActivity(), LeadActivity.class);
        intent.putExtra(LeadActivity.LEAD_ID, id);
        startActivity(intent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == FILTER_REQUEST_CODE && resultCode == RESULT_OK){
            bindModelData();
        }
    }
}
