package com.icarasia.leadmarketplace.ui.activities;

import android.os.Bundle;

import com.icarasia.leadmarketplace.R;
import com.icarasia.leadmarketplace.ui.fragments.PaymentFragment;

public class PaymentActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.payment_container,PaymentFragment.newInstance())
                .commit();
    }
}
