package com.icarasia.leadmarketplace.ui.fragments.dialogfragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.icarasia.leadmarketplace.R;

public class ErrorDialogFragment extends DialogFragment {

    private TextView gotIt, errorMsg;
    public static final String ERROR_MSG_KEY = "error_msg_key";

    public static ErrorDialogFragment newInstance(String errorMsg) {

        Bundle args = new Bundle();
        args.putString(ERROR_MSG_KEY, errorMsg);
        ErrorDialogFragment fragment = new ErrorDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_error_dialog, container, false);
    }

//    @Override
//    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
//        super.onViewCreated(view, savedInstanceState);
//        gotIt = view.findViewById(R.id.positive_button);
//        errorMsg = view.findViewById(R.id.description_text);
//        errorMsg.setText(getArguments().getString(ERROR_MSG_KEY));
//        gotIt.setOnClickListener(v -> {
//            dismiss();
//            if(!dismissible) {
//                getActivity().onBackPressed();
//            }
//
//        });
//    }
//
//    @NonNull
//    @Override
//    public Dialog onCreateDialog(Bundle savedInstanceState) {
//        Dialog dialog = new Dialog(getActivity(), getTheme()){
//            @Override
//            public void onBackPressed() {
//                dismiss();
//                if(!dismissible) {
//                    getActivity().onBackPressed();
//                }
//            }
//        };
//
//        // request a window without the title
//        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
//        dialog.setCanceledOnTouchOutside(false);
//        return dialog;
//    }
//
//
//    @Override
//    public void onStart() {
//        super.onStart();
//        //Disabling dark background
//        Dialog dialog = getDialog();
//        if (dialog != null) {
//            Window window = dialog.getWindow();
//            WindowManager.LayoutParams windowParams = window.getAttributes();
//            windowParams.dimAmount = 0f;
//            windowParams.flags |= WindowManager.LayoutParams.FLAG_DIM_BEHIND;
//            window.setAttributes(windowParams);
//        }
//
//    }
}
