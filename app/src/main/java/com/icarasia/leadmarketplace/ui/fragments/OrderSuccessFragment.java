package com.icarasia.leadmarketplace.ui.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.icarasia.leadmarketplace.R;


public class OrderSuccessFragment extends Fragment {

    private TextView gotIt;

    public static OrderSuccessFragment newInstance() {

        Bundle args = new Bundle();
        OrderSuccessFragment fragment = new OrderSuccessFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_order_success, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
        setListeners();
    }

    private void initViews(View view){
        gotIt = view.findViewById(R.id.got_it);
    }

    private void setListeners(){
        gotIt.setOnClickListener(v ->
            getActivity().finish());
    }
}
