package com.icarasia.leadmarketplace.ui.fragments;


import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.icarasia.leadmarketplace.R;
import com.icarasia.leadmarketplace.viewmodels.SignupViewModel;

public class SignupEmailFragment extends Fragment {

    private EditText editEmail;
    private TextView confirm;
    private ImageView emailIcon;

    private SignupViewModel model;

    public static SignupEmailFragment newInstance() {

        Bundle args = new Bundle();
        SignupEmailFragment fragment = new SignupEmailFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_signup_email, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);
        bindModelData();
        setListeners();
    }

    private void init(View view) {
        editEmail = view.findViewById(R.id.enter_email);
        confirm = view.findViewById(R.id.confirm);
        emailIcon = view.findViewById(R.id.email_icon);
    }

    private void bindModelData() {
        model = ViewModelProviders.of(getActivity()).get(SignupViewModel.class);
    }

    private void setListeners() {
        confirm.setOnClickListener(v -> model.sendEmailForVerification(editEmail.getText().toString()).observe(SignupEmailFragment.this, statusResource -> {
            switch (statusResource.status) {
                case SUCCESS:
                    model.setEmail(editEmail.getText().toString());
                    getFragmentManager().beginTransaction()
                            .addToBackStack(null)
                            .replace(R.id.login_container, SignupVerificationFragment.newInstance())
                            .commit();
                    break;
            }
        }));

        editEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(TextUtils.isEmpty(editEmail.getText().toString())){
                    emailIcon.setVisibility(View.VISIBLE);
                }else {
                    emailIcon.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }
}
