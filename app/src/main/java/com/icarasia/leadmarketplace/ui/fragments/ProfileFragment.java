package com.icarasia.leadmarketplace.ui.fragments;


import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.icarasia.leadmarketplace.R;
import com.icarasia.leadmarketplace.Utils;
import com.icarasia.leadmarketplace.data.models.User;
import com.icarasia.leadmarketplace.other.XAxisFormatter;
import com.icarasia.leadmarketplace.ui.activities.BaseActivity;
import com.icarasia.leadmarketplace.ui.activities.ProfileActivity;
import com.icarasia.leadmarketplace.viewmodels.ProfileViewModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class ProfileFragment extends Fragment {

    private ImageView back;
    private TextView edit;
    private LineChart lineChart;
    private TextView name;
    private TextView role;
    private ImageView avatar;
    private TextView credits;

    private ProfileViewModel model;

    public static ProfileFragment newInstance() {

        Bundle args = new Bundle();
        ProfileFragment fragment = new ProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
        bindModelData();
        setListeners();
        initUI();
    }

    private void initViews(View view) {
        back = view.findViewById(R.id.back);
        edit = view.findViewById(R.id.edit_profile);
        lineChart = view.findViewById(R.id.line_chart);
        name = view.findViewById(R.id.name);
        role = view.findViewById(R.id.role);
        avatar = view.findViewById(R.id.profile_image);
        credits = view.findViewById(R.id.coins_count);
    }

    private void bindModelData() {
        model = ViewModelProviders.of(this).get(ProfileViewModel.class);
        model.getUserInformation().observe(this, userResource -> {
            switch (userResource.status) {
                case SUCCESS:
                    ((BaseActivity) getActivity()).dismissCurrentDialog();
                    if (userResource.data != null) {
                        setUserInformation(userResource.data);
                    }
                    break;
                case ERROR:
                    ((BaseActivity) getActivity()).dismissCurrentDialog();
                    if (userResource.message != null) {
                        ((BaseActivity) getActivity()).showErrorDialog(userResource.message);
                    } else {
                        ((BaseActivity) getActivity()).showErrorDialog();
                    }
                    break;
                case LOADING:
                    ((BaseActivity) getActivity()).showLoadingDialog();
                    break;
            }
        });
    }

    private void setListeners() {
        back.setOnClickListener(v -> getFragmentManager().popBackStack());
        edit.setOnClickListener(v -> startActivity(new Intent(getActivity(), ProfileActivity.class)));
    }

    private void initUI() {
        drawLineChart();
    }

    private void drawLineChart() {
        initLineChart();
        List<Entry> entries = fakeInfo();
        LineDataSet dataSet = getLineChartDataset(entries);
        List<ILineDataSet> dataSets = new ArrayList<>();
        dataSets.add(dataSet);
        LineData data = new LineData(dataSets);
        lineChart.setData(data);
        lineChart.invalidate();
    }

    private void initLineChart() {
        LineChart.LayoutParams layoutParams = lineChart.getLayoutParams();
        layoutParams.width = Utils.convertDpToPixel(50 + 5 * 70);
        lineChart.setLayoutParams(layoutParams);
        lineChart.getAxisRight().setEnabled(false);
        lineChart.getDescription().setEnabled(false);
        lineChart.getLegend().setEnabled(false);
        XAxis xAxis = lineChart.getXAxis();
        xAxis.setDrawGridLines(false);
        xAxis.setDrawLimitLinesBehindData(false);
        xAxis.setDrawGridLines(false);
        xAxis.setTextColor(getResources().getColor(R.color.black_text_color));
        XAxisFormatter daysFormatter = new XAxisFormatter();
        daysFormatter.setList(getDays());
        xAxis.setValueFormatter(daysFormatter);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setGranularity(1f);
        xAxis.setAxisMaximum(4f);
        xAxis.setAxisMinimum(0f);
        xAxis.setTextSize(10);
        YAxis yAxis = lineChart.getAxisLeft();
        yAxis.setDrawGridLines(false);
        yAxis.setDrawLabels(false);
        yAxis.setDrawZeroLine(false);
        yAxis.setDrawAxisLine(false);
        yAxis.setAxisMaximum(70f);
        yAxis.setAxisMinimum(0f);
    }

    private LineDataSet getLineChartDataset(List<Entry> entries) {
        LineDataSet dataSet = new LineDataSet(entries, "Days");
        dataSet.setDrawFilled(false);
        dataSet.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        dataSet.setColor(Color.parseColor("#16ABFB"));
        dataSet.setLineWidth(2);
        dataSet.setCircleRadius(7);
        dataSet.setCircleHoleRadius(5);
        dataSet.setCircleColor(Color.parseColor("#16ABFB"));
        dataSet.setDrawCircles(true);
        dataSet.setDrawValues(true);
        dataSet.setValueTextSize(12);
        dataSet.setValueTextColor(getResources().getColor(R.color.black_text_color));
        return dataSet;
    }

    private List<Entry> fakeInfo() {
        List<Entry> entries = new ArrayList<>();
        entries.add(new Entry(0f, 28f));
        entries.add(new Entry(1f, 42f));
        entries.add(new Entry(2f, 25f));
        entries.add(new Entry(3f, 64f));
        entries.add(new Entry(4f, 50f));
        return entries;
    }

    private List<String> getDays() {
        List<String> list = new ArrayList<>();
        list.add("July");
        list.add("August");
        list.add("September");
        list.add("October");
        list.add("November");
        return list;
    }

    private void setUserInformation(User user) {
        name.setText(user.getName());
        role.setText(user.getRole().get(0));
        credits.setText(String.valueOf(user.getCredits()));
        Picasso.get().load(user.getAvatar()).into(avatar);
    }
}
