package com.icarasia.leadmarketplace.ui.fragments;


import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.icarasia.leadmarketplace.R;
import com.icarasia.leadmarketplace.adapters.NotificationsAdapter;
import com.icarasia.leadmarketplace.viewmodels.NotificationsViewModel;

public class NotificationsFragment extends Fragment {

    private ImageView back;
    private RecyclerView list;

    private NotificationsViewModel model;
    private NotificationsAdapter adapter;

    public static NotificationsFragment newInstance() {

        Bundle args = new Bundle();
        NotificationsFragment fragment = new NotificationsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_notifications, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
        initUI();
        bindModelData();
        setListeners();
    }

    private void initViews(View view){
        back = view.findViewById(R.id.back);
        list = view.findViewById(R.id.notifications_list);
    }

    private void initUI(){
        list.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new NotificationsAdapter();
        list.setAdapter(adapter);
    }


    private void bindModelData(){
        model = ViewModelProviders.of(this).get(NotificationsViewModel.class);
        model.getNotifications().observe(this,listResource -> {
            switch (listResource.status){
                case SUCCESS:
                    adapter.updateNotifications(listResource.data);
                    break;
            }
        });
    }

    private void setListeners(){
        back.setOnClickListener(v -> getFragmentManager().popBackStack());
    }
}
