package com.icarasia.leadmarketplace.ui.fragments;


import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.icarasia.leadmarketplace.R;
import com.icarasia.leadmarketplace.ui.activities.BaseActivity;
import com.icarasia.leadmarketplace.viewmodels.PaymentViewModel;

public class PaymentFragment extends Fragment {
    //Todo Views size not equal with Zeplin's sizes

    private ImageView back;
    private ConstraintLayout paypalEditLayout;
    private ConstraintLayout creditCardEditLayout;
    private ImageView paypalCheck;
    private ImageView creditCardCheck;
    private TextView payNow;
    private EditText cardCode1;
    private EditText cardCode2;
    private EditText cardCode3;
    private EditText cardCode4;
    private EditText cardName;
    private EditText cardDate;
    private EditText cardCVV;
    private CheckBox saveCard;

    private PaymentViewModel model;

    public static PaymentFragment newInstance() {

        Bundle args = new Bundle();
        PaymentFragment fragment = new PaymentFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_payment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
        bindModelData();
        setListeners();
    }

    private void initViews(View view) {
        back = view.findViewById(R.id.back);
        paypalEditLayout = view.findViewById(R.id.paypal_edit_layout);
        creditCardEditLayout = view.findViewById(R.id.credit_cards_edit_layout);
        paypalCheck = view.findViewById(R.id.paypal_check);
        creditCardCheck = view.findViewById(R.id.cards_check);
        payNow = view.findViewById(R.id.pay_now);
        cardCode1 = view.findViewById(R.id.card_code_1);
        cardCode2 = view.findViewById(R.id.card_code_2);
        cardCode3 = view.findViewById(R.id.card_code_3);
        cardCode4 = view.findViewById(R.id.card_code_4);
        cardName = view.findViewById(R.id.card_name);
        cardDate = view.findViewById(R.id.card_date);
        cardCVV = view.findViewById(R.id.card_cvv);
        saveCard = view.findViewById(R.id.save_card_check);
    }

    private void bindModelData() {
        model = ViewModelProviders.of(this).get(PaymentViewModel.class);
    }

    private void setListeners() {
        back.setOnClickListener(v ->
                getActivity().finish());

        paypalCheck.setOnClickListener(v -> {
            if (paypalEditLayout.getVisibility() == View.GONE) {
                paypalEditLayout.setVisibility(View.VISIBLE);
                paypalCheck.setImageDrawable(getResources().getDrawable(R.drawable.blue_fill_circle));
            } else {
                paypalEditLayout.setVisibility(View.GONE);
                paypalCheck.setImageDrawable(getResources().getDrawable(R.drawable.blue_empty_circle));
            }
            creditCardEditLayout.setVisibility(View.GONE);
            creditCardCheck.setImageDrawable(getResources().getDrawable(R.drawable.blue_empty_circle));
        });

        creditCardCheck.setOnClickListener(v -> {
            if (creditCardEditLayout.getVisibility() == View.GONE) {
                creditCardEditLayout.setVisibility(View.VISIBLE);
                creditCardCheck.setImageDrawable(getResources().getDrawable(R.drawable.blue_fill_circle));
            } else {
                creditCardEditLayout.setVisibility(View.GONE);
                creditCardCheck.setImageDrawable(getResources().getDrawable(R.drawable.blue_empty_circle));
            }
            paypalEditLayout.setVisibility(View.GONE);
            paypalCheck.setImageDrawable(getResources().getDrawable(R.drawable.blue_empty_circle));
        });

        payNow.setOnClickListener(v -> {
            if (!TextUtils.isEmpty(cardCode1.getText().toString()) &&
                    !TextUtils.isEmpty(cardCode2.getText().toString()) &&
                    !TextUtils.isEmpty(cardCode3.getText().toString()) &&
                    !TextUtils.isEmpty(cardCode4.getText().toString()) &&
                    !TextUtils.isEmpty(cardName.getText().toString()) &&
                    !TextUtils.isEmpty(cardDate.getText().toString()) &&
                    !TextUtils.isEmpty(cardCVV.getText().toString())) {
                int save;
                if(saveCard.isChecked()){
                    save = 1;
                }else {
                    save = 0;
                }
                model.processPayment(1, getCardNumber(),
                        cardName.getText().toString(),
                        cardDate.getText().toString(),
                        cardCVV.getText().toString(),
                "",
                        save).observe(this,statusResponseResource -> {
                    if (statusResponseResource != null) {
                        switch (statusResponseResource.status) {
                            case SUCCESS:
                                ((BaseActivity) getActivity()).dismissCurrentDialog();
                                if(statusResponseResource.data.getStatus().equals("success")){
                                    getFragmentManager().beginTransaction()
                                            .addToBackStack(null)
                                            .replace(R.id.payment_container,OrderSuccessFragment.newInstance())
                                            .commit();
                                }
                                break;
                            case ERROR:
                                ((BaseActivity) getActivity()).dismissCurrentDialog();
                                if (statusResponseResource.message != null) {
                                    ((BaseActivity) getActivity()).showErrorDialog(statusResponseResource.message);
                                } else {
                                    ((BaseActivity) getActivity()).showErrorDialog();
                                }
                                break;
                            case LOADING:
                                ((BaseActivity) getActivity()).showLoadingDialog();
                                break;
                        }
                    }
                });
            } else {
                Toast.makeText(getActivity(), "Please, fill all fields", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private String getCardNumber() {
        return cardCode1.getText().toString().concat(" ").concat(cardCode2.getText().toString()).concat(" ").concat(cardCode3.getText().toString()).concat(" ").concat(cardCode4.getText().toString());
    }
}
