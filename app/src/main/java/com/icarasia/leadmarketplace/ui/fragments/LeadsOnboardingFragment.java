package com.icarasia.leadmarketplace.ui.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.icarasia.leadmarketplace.R;
import com.icarasia.leadmarketplace.ui.activities.MainActivity;

public class LeadsOnboardingFragment extends Fragment {

    private TextView gotIt;

    public static LeadsOnboardingFragment newInstance() {

        Bundle args = new Bundle();

        LeadsOnboardingFragment fragment = new LeadsOnboardingFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_leads_onboarding, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
        setListeners();
    }

    private void initViews(View view){
        gotIt = view.findViewById(R.id.got_it);
    }

    private void setListeners(){
        gotIt.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        });
    }
}
