package com.icarasia.leadmarketplace.ui.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.icarasia.leadmarketplace.R;

public class InitialFragment extends Fragment {

    private TextView signIn;
    private TextView signUp;

    public static InitialFragment newInstance() {
        Bundle args = new Bundle();
        InitialFragment fragment = new InitialFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_initial, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
        setListeners();
    }

    private void initViews(View view){
        signIn = view.findViewById(R.id.sign_in);
        signUp = view.findViewById(R.id.sign_up);
    }

    private void setListeners(){
        signIn.setOnClickListener(view -> getFragmentManager().beginTransaction()
                .addToBackStack(null)
                .replace(R.id.login_container,LoginEmailFragment.newInstance())
                .commit());

        signUp.setOnClickListener(v -> getFragmentManager().beginTransaction()
                .addToBackStack(null)
                .replace(R.id.login_container,SignupEmailFragment.newInstance())
                .commit());
    }
}
