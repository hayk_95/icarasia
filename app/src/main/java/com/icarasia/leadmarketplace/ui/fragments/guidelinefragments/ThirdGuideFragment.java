package com.icarasia.leadmarketplace.ui.fragments.guidelinefragments;


import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.icarasia.leadmarketplace.R;
import com.icarasia.leadmarketplace.ui.activities.BaseActivity;
import com.icarasia.leadmarketplace.ui.activities.MainActivity;
import com.icarasia.leadmarketplace.ui.fragments.LeadsOnboardingFragment;
import com.icarasia.leadmarketplace.viewmodels.GuidelineViewModel;

public class ThirdGuideFragment extends Fragment {

    private TextView getStarted;

    private GuidelineViewModel model;

    public static ThirdGuideFragment newInstance() {

        Bundle args = new Bundle();
        ThirdGuideFragment fragment = new ThirdGuideFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_third_guide, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
        bindModelData();
        setListeners();
    }

    private void initViews(View view) {
        getStarted = view.findViewById(R.id.get_started);
    }

    private void bindModelData() {
        model = ViewModelProviders.of(this).get(GuidelineViewModel.class);
    }

    private void setListeners() {
        getStarted.setOnClickListener(view -> {
            model.getLeads().observe(this, listResource -> {
                if (listResource != null) {
                    switch (listResource.status) {
                        case SUCCESS:
                            ((BaseActivity) getActivity()).dismissCurrentDialog();
                            if (!listResource.data.isEmpty()) {
                                Intent intent = new Intent(getActivity(), MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            } else {
                                openOnboarding();
                            }
                            break;
                        case ERROR:
                            ((BaseActivity) getActivity()).dismissCurrentDialog();
                            openOnboarding();
                            break;
                        case LOADING:
                            ((BaseActivity) getActivity()).showLoadingDialog();
                            break;
                    }
                }
            });
        });
    }

    private void openOnboarding() {
        getFragmentManager().beginTransaction()
                .replace(R.id.guideline_container, LeadsOnboardingFragment.newInstance())
                .commit();
    }

}
