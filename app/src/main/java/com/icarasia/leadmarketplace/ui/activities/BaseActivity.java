package com.icarasia.leadmarketplace.ui.activities;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import com.icarasia.leadmarketplace.R;
import com.icarasia.leadmarketplace.Utils;
import com.icarasia.leadmarketplace.ui.fragments.dialogfragments.LoadingDialogFragment;

public class BaseActivity extends AppCompatActivity {

    public static final String LOADING_TAG = "loading";
    public static final String ERROR_TAG = "error";
    public static final String TIMEOUT = "timeout";

    private LoadingDialogFragment loadingDialog;
//    private ErrorDialogFragment errorDialog;

    private int pendingLoadingCount = 0;


    public void showLoadingDialog() {
        if (getSupportFragmentManager().findFragmentByTag(ERROR_TAG) == null) {
            if (pendingLoadingCount == 0) {
                dismissCurrentDialog();
                loadingDialog = LoadingDialogFragment.newInstance();
                FragmentManager manager = getSupportFragmentManager();
                loadingDialog.show(manager, LOADING_TAG);
            }
            pendingLoadingCount++;
        }
    }

    public void dismissCurrentDialog() {

        if (getSupportFragmentManager().findFragmentByTag(ERROR_TAG) == null) {
            if (getSupportFragmentManager().findFragmentByTag(LOADING_TAG) != null) {
                if (pendingLoadingCount > 0) {
                    pendingLoadingCount--;
                }
                if (pendingLoadingCount == 0) {
                    loadingDialog.dismiss();
                }
            }
        }
    }

//    public void showTimeoutErrorDialog() {
//        if (getSupportFragmentManager().findFragmentByTag(ERROR_TAG) == null) {
//            dismissCurrentDialog();
//            String errorMessage;
//            if (NetworkUtils.isNetworkConnected(this)) {
//                errorMessage = getString(R.string.timeout_error_message);
//            } else {
//                errorMessage = getString(R.string.no_connection_error_message);
//            }
//            errorDialog = ErrorDialogFragment.newInstance(errorMessage, false);
//            FragmentManager manager = getSupportFragmentManager();
//            errorDialog.show(manager, ERROR_TAG);
//        }
//    }

    public void showErrorDialog() {
        if (getSupportFragmentManager().findFragmentByTag(ERROR_TAG) == null) {
            String errorMessage = Utils.isNetworkConnected(this)
                    ? getString(R.string.default_error_message)
                    : getString(R.string.no_connection_error_message);
//            errorDialog = ErrorDialogFragment.newInstance(errorMessage);
//            errorDialog.show(getSupportFragmentManager(), ERROR_TAG);
            showErrorDialog(errorMessage);
        }
    }

    public void showErrorDialog(String errorMsg) {
        if (getSupportFragmentManager().findFragmentByTag(ERROR_TAG) == null) {
            dismissCurrentDialog();
//            errorDialog = ErrorDialogFragment.newInstance(errorMsg);
//            FragmentManager manager = getSupportFragmentManager();
//            errorDialog.show(manager, ERROR_TAG);
            new AlertDialog.Builder(this)
                    .setTitle("Error")
                    .setMessage(errorMsg)
                    .setPositiveButton(getString(R.string.ok),(dialog, which) -> dialog.dismiss())
                    .create().show();
        }
    }

}
