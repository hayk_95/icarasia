package com.icarasia.leadmarketplace.ui.fragments;


import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.icarasia.leadmarketplace.R;
import com.icarasia.leadmarketplace.adapters.CommentsAdapter;
import com.icarasia.leadmarketplace.data.models.Comment;
import com.icarasia.leadmarketplace.data.models.User;
import com.icarasia.leadmarketplace.ui.activities.BaseActivity;
import com.icarasia.leadmarketplace.viewmodels.CommentsViewModel;


public class CommentsFragment extends Fragment {
    private static final String LEAD_ID_ARGUMENT = "LeadIdArgument";

    private ImageView back;
    private RecyclerView list;
    private ImageView send;
    private ImageView emotion;
    private ImageView shareImage;
    private EditText editMessage;

    private CommentsViewModel model;
    private CommentsAdapter adapter;
    private User user;

    public static CommentsFragment newInstance(int leadId) {

        Bundle args = new Bundle();
        args.putInt(LEAD_ID_ARGUMENT, leadId);
        CommentsFragment fragment = new CommentsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_comments, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
        initUI();
        bindModelData();
        setListeners();
    }

    private void initViews(View view) {
        back = view.findViewById(R.id.back);
        list = view.findViewById(R.id.comments_list);
        send = view.findViewById(R.id.send_comment);
        emotion = view.findViewById(R.id.emotion);
        shareImage = view.findViewById(R.id.share_image);
        editMessage = view.findViewById(R.id.edit_message);
    }

    private void initUI() {
        list.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new CommentsAdapter(getActivity());
        list.setAdapter(adapter);
    }

    private void bindModelData() {
        model = ViewModelProviders.of(this).get(CommentsViewModel.class);

        model.getUserInformation().observe(this, userResource -> {
            if (userResource != null) {
                switch (userResource.status) {
                    case SUCCESS:
                        ((BaseActivity) getActivity()).dismissCurrentDialog();
                        user = userResource.data;
                        break;
                    case ERROR:
                        ((BaseActivity) getActivity()).dismissCurrentDialog();
                        if (userResource.message != null) {
                            ((BaseActivity) getActivity()).showErrorDialog(userResource.message);
                        } else {
                            ((BaseActivity) getActivity()).showErrorDialog();
                        }
                        break;
                    case LOADING:
                        ((BaseActivity) getActivity()).showLoadingDialog();
                        break;
                }
            }
        });

        model.getComments(getArguments().getInt(LEAD_ID_ARGUMENT)).observe(this, listResource -> {
            switch (listResource.status) {
                case SUCCESS:
                    ((BaseActivity) getActivity()).dismissCurrentDialog();
                    adapter.updateComments(listResource.data);
                    break;
                case ERROR:
                    ((BaseActivity) getActivity()).dismissCurrentDialog();
                    if (listResource.message != null) {
                        ((BaseActivity) getActivity()).showErrorDialog(listResource.message);
                    } else {
                        ((BaseActivity) getActivity()).showErrorDialog();
                    }
                    break;
                case LOADING:
                    ((BaseActivity) getActivity()).showLoadingDialog();
                    break;
            }
        });
    }

    private void setListeners() {
        back.setOnClickListener(v -> getFragmentManager().popBackStack());

        send.setOnClickListener(v -> {
            if (!TextUtils.isEmpty(editMessage.getText().toString())) {
                model.sendComment(getArguments().getInt(LEAD_ID_ARGUMENT, 0), editMessage.getText().toString()).observe(this, sendCommentResponseResource -> {
                    if (sendCommentResponseResource != null) {
                        switch (sendCommentResponseResource.status) {
                            case SUCCESS:
                                ((BaseActivity) getActivity()).dismissCurrentDialog();
                                adapter.addComment(new Comment(sendCommentResponseResource.data.getCommentId(), editMessage.getText().toString(),  "1 second ago",user));
                                editMessage.setText("");
                                closeKeyboard();
                                break;
                            case ERROR:
                                ((BaseActivity) getActivity()).dismissCurrentDialog();
                                if (sendCommentResponseResource.message != null) {
                                    ((BaseActivity) getActivity()).showErrorDialog(sendCommentResponseResource.message);
                                } else {
                                    ((BaseActivity) getActivity()).showErrorDialog();
                                }
                                break;
                            case LOADING:
                                ((BaseActivity) getActivity()).showLoadingDialog();
                                break;
                        }
                    }
                });
            } else {
                Toast.makeText(getActivity(), "Message content is empty", Toast.LENGTH_SHORT).show();
            }
        });

        editMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(TextUtils.isEmpty(editMessage.getText().toString())){
                    send.setVisibility(View.INVISIBLE);
                    emotion.setVisibility(View.VISIBLE);
                    shareImage.setVisibility(View.VISIBLE);
                }else {
                    send.setVisibility(View.VISIBLE);
                    emotion.setVisibility(View.INVISIBLE);
                    shareImage.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void closeKeyboard() {
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }
}
