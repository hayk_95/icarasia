package com.icarasia.leadmarketplace.ui.activities;

import android.os.Bundle;

import com.icarasia.leadmarketplace.R;
import com.icarasia.leadmarketplace.ui.fragments.guidelinefragments.GuideFragment;

public class GuidelineActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guideline);
        init();
    }

    private void init(){
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.guideline_container,GuideFragment.newInstance())
                .commit();
    }
}
