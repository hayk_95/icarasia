package com.icarasia.leadmarketplace.ui.fragments;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ScrollView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.icarasia.leadmarketplace.R;
import com.icarasia.leadmarketplace.Utils;
import com.icarasia.leadmarketplace.other.XAxisFormatter;

import java.util.ArrayList;
import java.util.List;

public class PerformanceFragment extends Fragment {

    private ImageView back;
    private TabLayout tabLayout;
    private PieChart pieChart;
    private LineChart lineChart;
    private BarChart barChart;
    private ScrollView scrollView;
    private boolean isBarChartAnimated = false;
    private boolean isLineChartAnimated = false;

    public static PerformanceFragment newInstance() {

        Bundle args = new Bundle();
        PerformanceFragment fragment = new PerformanceFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_performance, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
        initUI();
        setListeners();
    }

    private void initViews(View view) {
        back = view.findViewById(R.id.back);
        tabLayout = view.findViewById(R.id.tab_layout);
        pieChart = view.findViewById(R.id.pie_chart);
        lineChart = view.findViewById(R.id.line_chart);
        barChart = view.findViewById(R.id.bar_chart);
        scrollView = view.findViewById(R.id.scroll_view);
    }

    private void initUI() {
        drawPieChart();
        initLineChart();
        initBarChart();
        if(scrollView.getScrollY() + scrollView.getHeight() >= lineChart.getY() + lineChart.getHeight()/2){
            drawLineChart(fakeInfo());
            isLineChartAnimated = true;
            lineChart.animateXY(1500,1500);
        }
        tabLayout.addTab(tabLayout.newTab().setText("Response Time"));
        tabLayout.addTab(tabLayout.newTab().setText("Conversion Rate"));
        tabLayout.addTab(tabLayout.newTab().setText("Most Sales"));
    }

    private void setListeners() {
        back.setOnClickListener(v -> getFragmentManager().popBackStack());

        scrollView.getViewTreeObserver().addOnScrollChangedListener(() -> {
            if (!isLineChartAnimated && scrollView.getScrollY() + scrollView.getHeight() >= lineChart.getY() + lineChart.getHeight() / 2) {
                drawLineChart(fakeInfo());
                isLineChartAnimated = true;
                lineChart.animateXY(1500,1500);
            }

            if (!isBarChartAnimated && scrollView.getScrollY() + scrollView.getHeight() >= barChart.getY() + barChart.getHeight() / 2) {
                drawBarChart(fakeDealersRatingInfo());
                isBarChartAnimated = true;
                barChart.animateY(1500);
            }
        });
    }



    private void drawPieChart() {
        initPieChart();
        List<PieEntry> pieEntries = new ArrayList<>();
        pieEntries.add(new PieEntry(4.8f));
        pieEntries.add(new PieEntry(20f - 4.8f));
        PieDataSet dataSet = getPieChartDataset(pieEntries);
        dataSet.setDrawValues(false);
        PieData data = new PieData();
        data.setDataSet(dataSet);
        pieChart.setData(data);
        pieChart.highlightValue(new Highlight(0, 0, pieChart.getDataSetIndexForIndex(0)));
        pieChart.setHighlightPerTapEnabled(false);
        String text = "4.8\nmin";
        SpannableString changedText = new SpannableString(text);
        changedText.setSpan(new RelativeSizeSpan(2.6f), 0, 3, 0);
        pieChart.setCenterTextSizePixels(getResources().getDimension(R.dimen.text_size_18));
        pieChart.setCenterText(changedText);
        pieChart.setCenterTextColor(getResources().getColor(R.color.white));
        pieChart.animateX(1500);
        pieChart.invalidate();
    }

    private void initPieChart() {
        pieChart.getLegend().setEnabled(false);
        pieChart.getDescription().setEnabled(false);
        pieChart.setEntryLabelTextSize(20);
        pieChart.setHoleColor(getResources().getColor(R.color.transparent));
        pieChart.setTransparentCircleAlpha(60);
        pieChart.setTransparentCircleColor(getResources().getColor(R.color.feedback_black));
        pieChart.setTransparentCircleRadius(58);
    }

    private PieDataSet getPieChartDataset(List<PieEntry> entries) {
        PieDataSet dataSet = new PieDataSet(entries, "Response Time");
        dataSet.setColor(getResources().getColor(R.color.leads_diagram_start_color));
        dataSet.setValueTextSize(14f);
        dataSet.setSelectionShift(15f);
        dataSet.setSliceSpace(2f);
        List<Integer> list = new ArrayList<>();
        list.add(Color.parseColor("#FD2AA6"));
        list.add(Color.parseColor("#0F2148"));
        dataSet.setColors(list);
        return dataSet;
    }

    private void drawLineChart(List<Entry> entries) {
        LineDataSet dataSet = getLineChartDataset(entries);
        List<ILineDataSet> dataSets = new ArrayList<>();
        dataSets.add(dataSet);
        LineData data = new LineData(dataSets);
        lineChart.setData(data);
        lineChart.invalidate();
    }

    private void initLineChart() {
        LineChart.LayoutParams layoutParams = lineChart.getLayoutParams();
        layoutParams.height = Utils.convertDpToPixel(40 * 5);
        layoutParams.width = Utils.convertDpToPixel(55 + 6 * 100);
        lineChart.setLayoutParams(layoutParams);
        lineChart.getAxisRight().setEnabled(false);
        lineChart.getDescription().setEnabled(false);
        lineChart.getLegend().setEnabled(false);
        XAxis xAxis = lineChart.getXAxis();
        xAxis.setDrawGridLines(false);
        xAxis.setDrawLimitLinesBehindData(false);
        xAxis.setGridColor(getResources().getColor(R.color.transparent));
        xAxis.setTextColor(getResources().getColor(R.color.white));
        XAxisFormatter daysFormatter = new XAxisFormatter();
        daysFormatter.setList(getDays());
        xAxis.setValueFormatter(daysFormatter);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setGranularity(1f);
        xAxis.setAxisMaximum(4f);
        xAxis.setAxisMinimum(0f);
        YAxis yAxis = lineChart.getAxisLeft();
        yAxis.setDrawZeroLine(true);
        yAxis.setDrawAxisLine(false);
        yAxis.setAxisMaximum(30f);
        yAxis.setAxisMinimum(0f);
        yAxis.setZeroLineWidth(5f);
        yAxis.setGranularity(5f);
        yAxis.setZeroLineColor(getResources().getColor(R.color.white));
        yAxis.setTextColor(getResources().getColor(R.color.white));
        List<Entry> list = new ArrayList<>();
        list.add(new Entry(0f,0f));
        drawLineChart(list);
    }

    private LineDataSet getLineChartDataset(List<Entry> entries) {
        LineDataSet dataSet = new LineDataSet(entries, "Days");
        dataSet.setDrawFilled(true);
        dataSet.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        dataSet.setFillDrawable(getResources().getDrawable(R.drawable.blue_gradient));
        dataSet.setColor(getResources().getColor(R.color.leads_diagram_start_color));
        dataSet.setDrawCircles(false);
        dataSet.setDrawValues(false);
        return dataSet;
    }

    private List<Entry> fakeInfo() {
        List<Entry> entries = new ArrayList<>();
        entries.add(new Entry(0f, 20f));
        entries.add(new Entry(0.3f, 18f));
        entries.add(new Entry(0.7f, 16f));
        entries.add(new Entry(1f, 14f));
        entries.add(new Entry(1.4f, 12f));
        entries.add(new Entry(2f, 10f));
        entries.add(new Entry(2.4f, 12f));
        entries.add(new Entry(2.7f, 14f));
        entries.add(new Entry(3f, 16f));
        entries.add(new Entry(3.5f, 16f));
        entries.add(new Entry(4f, 14f));
        return entries;
    }

    private List<String> getDays() {
        List<String> list = new ArrayList<>();
        list.add("Nov5");
        list.add("Nov10");
        list.add("Nov15");
        list.add("Nov20");
        list.add("Nov25");
        return list;
    }

    private void drawBarChart(List<BarEntry> entries) {
        BarDataSet dataSet = getBarChartDataset(entries);
        List<IBarDataSet> dataSets = new ArrayList<>();
        dataSets.add(dataSet);
        BarData data = new BarData(dataSets);
        data.setBarWidth(0.5f);
        barChart.setData(data);
        barChart.invalidate();
    }

    private void initBarChart() {
        barChart.getAxisRight().setEnabled(false);
        barChart.getDescription().setEnabled(false);
        barChart.getLegend().setEnabled(false);
        XAxis xAxis = barChart.getXAxis();
        XAxisFormatter daysFormatter = new XAxisFormatter();
        daysFormatter.setList(getDealers());
        xAxis.setValueFormatter(daysFormatter);
        xAxis.setDrawGridLines(false);
        xAxis.setDrawLimitLinesBehindData(false);
        xAxis.setGridColor(getResources().getColor(R.color.transparent));
        xAxis.setTextColor(getResources().getColor(R.color.white));
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setGranularity(1f);
        xAxis.setAxisMaximum(3 - 0.55f);
        xAxis.setAxisMinimum(-0.45f);
        YAxis yAxis = barChart.getAxisLeft();
        yAxis.setAxisMinimum(0f);
        yAxis.setAxisMaximum(5f);
        yAxis.setZeroLineWidth(2f);
        yAxis.setGranularity(1f);
        yAxis.setDrawZeroLine(true);
        yAxis.setDrawAxisLine(false);
        yAxis.setDrawGridLines(false);
        yAxis.setDrawLabels(false);
        yAxis.setZeroLineColor(getResources().getColor(R.color.white));
        yAxis.setTextColor(getResources().getColor(R.color.white));
        List<BarEntry> list = new ArrayList<>();
        list.add(new BarEntry(0f,0f));
        drawBarChart(list);
    }

    private List<BarEntry> fakeDealersRatingInfo() {
        List<BarEntry> entries = new ArrayList<>();
        entries.add(new BarEntry(0, 4.3f));
        entries.add(new BarEntry(1, 4f));
        entries.add(new BarEntry(2, 3f));
        return entries;
    }

    private BarDataSet getBarChartDataset(List<BarEntry> entries) {
        BarDataSet dataSet = new BarDataSet(entries, "Dealers Rating");
        dataSet.setBarBorderWidth(1f);
        dataSet.setDrawValues(true);
        dataSet.setValueTextColor(getResources().getColor(R.color.white));
        dataSet.setValueTextSize(12);
        dataSet.setBarBorderColor(Color.parseColor("#293F86"));
        dataSet.setBarShadowColor(getResources().getColor(R.color.transparent));
        List<Integer> list = new ArrayList<>();
        list.add(Color.parseColor("#FD2AA6"));
        list.add(Color.parseColor("#A32CF9"));
        list.add(Color.parseColor("#1B95F2"));
        dataSet.setColors(list);
        return dataSet;
    }

    private List<String> getDealers() {
        List<String> list = new ArrayList<>();
        list.add("YOU");
        list.add("Similar Dealers");
        list.add("All Dealers");
        return list;
    }
}
