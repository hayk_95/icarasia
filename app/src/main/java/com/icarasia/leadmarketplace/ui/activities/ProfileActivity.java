package com.icarasia.leadmarketplace.ui.activities;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.icarasia.leadmarketplace.R;
import com.icarasia.leadmarketplace.ui.fragments.EditProfileFragment;

import static com.icarasia.leadmarketplace.ui.fragments.EditProfileFragment.PERMISSIONS_REQUEST_CODE;

public class ProfileActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.profile_container,EditProfileFragment.newInstance())
                .commit();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSIONS_REQUEST_CODE) {
            boolean isChecked = true;
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                    isChecked = false;
                    break;
                }
            }
            if (isChecked) {
                if(getSupportFragmentManager().findFragmentById(R.id.profile_container) != null &&
                        getSupportFragmentManager().findFragmentById(R.id.profile_container) instanceof EditProfileFragment){
                    ((EditProfileFragment) getSupportFragmentManager().findFragmentById(R.id.profile_container)).openPickImageDialog();
                }
            }
        }
    }
}
