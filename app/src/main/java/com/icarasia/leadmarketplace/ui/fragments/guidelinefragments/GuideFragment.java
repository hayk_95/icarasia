package com.icarasia.leadmarketplace.ui.fragments.guidelinefragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.icarasia.leadmarketplace.R;

public class GuideFragment extends Fragment {
    private static final int PAGES_COUNT = 3;

    private ViewPager pager;
    private ImageView firstDot;
    private ImageView secondDot;
    private ImageView thirdDot;

    public static GuideFragment newInstance() {

        Bundle args = new Bundle();
        GuideFragment fragment = new GuideFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_guide, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
        initUI();
        setListeners();
    }

    private void initViews(View view){
        pager = view.findViewById(R.id.guide_pager);
        firstDot = view.findViewById(R.id.first_dot);
        secondDot = view.findViewById(R.id.second_dot);
        thirdDot = view.findViewById(R.id.third_dot);
    }

    private void initUI(){
        pager.setAdapter(new GuidePagerAdapter(getFragmentManager()));
    }

    private void setListeners(){
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                setCurrentDot(i);
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }

    private void setCurrentDot(int position){
        firstDot.setImageDrawable(getResources().getDrawable(R.drawable.white_dot));
        secondDot.setImageDrawable(getResources().getDrawable(R.drawable.white_dot));
        thirdDot.setImageDrawable(getResources().getDrawable(R.drawable.white_dot));
        switch (position){
            case 0:
                firstDot.setImageDrawable(getResources().getDrawable(R.drawable.white_fill_dot));
                break;
            case 1:
                secondDot.setImageDrawable(getResources().getDrawable(R.drawable.white_fill_dot));
                break;
            case 2:
                thirdDot.setImageDrawable(getResources().getDrawable(R.drawable.white_fill_dot));
                break;
        }
    }

    private class GuidePagerAdapter extends FragmentPagerAdapter{

        private GuidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            switch (i){
                case 0:
                    return FirstGuideFragment.newInstance();
                case 1:
                    return SecondGuideFragment.newInstance();
                case 2:
                    return ThirdGuideFragment.newInstance();
            }
            return null;
        }

        @Override
        public int getCount() {
            return PAGES_COUNT;
        }
    }
}
