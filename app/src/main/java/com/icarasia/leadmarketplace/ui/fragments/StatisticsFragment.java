package com.icarasia.leadmarketplace.ui.fragments;


import android.app.DatePickerDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.icarasia.leadmarketplace.R;
import com.icarasia.leadmarketplace.Utils;
import com.icarasia.leadmarketplace.adapters.TopTradesAdapter;
import com.icarasia.leadmarketplace.other.XAxisFormatter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class StatisticsFragment extends Fragment implements DatePickerDialog.OnDateSetListener {
    private final int LEADS_DATE_FROM = 1;
    private final int LEADS_DATE_TO = 2;
    private final int TOP_MODEL_DATE_FROM = 3;
    private final int TOP_MODEL_DATE_TO = 4;
    private final int TOP_TRADE_DATE_FROM = 5;
    private final int TOP_TRADE_DATE_TO = 6;

    private ImageView back;
    private LineChart lineChart;
    private BarChart barChart;
    private PieChart pieChart;
    private ConstraintLayout leadsDateFrom;
    private ConstraintLayout leadsDateTo;
    private ConstraintLayout topModelsDateFrom;
    private ConstraintLayout topModelsDateTo;
    private ConstraintLayout topTradeDateFrom;
    private ConstraintLayout topTradeDateTo;
    private TextView leadsDateFromText;
    private TextView leadsDateToText;
    private TextView topModelDateFromText;
    private TextView topModelDateToText;
    private TextView topTradeDateFromText;
    private TextView topTradeDateToText;
    private RecyclerView topTradesList;

    private TopTradesAdapter topTradesAdapter;
    private PieEntry clickedEntry;
    private float pieEntriesFullCount = 0f;
    private int openedDateDialog = 0;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.US);

    public static StatisticsFragment newInstance() {

        Bundle args = new Bundle();
        StatisticsFragment fragment = new StatisticsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_statistics, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
        initUI();
        setListeners();
    }

    private void initViews(View view) {
        back = view.findViewById(R.id.back);
        lineChart = view.findViewById(R.id.leads_chart);
        barChart = view.findViewById(R.id.top_model_chart);
        pieChart = view.findViewById(R.id.top_trade_chart);
        leadsDateFrom = view.findViewById(R.id.leads_date_from);
        leadsDateTo = view.findViewById(R.id.leads_date_to);
        topModelsDateFrom = view.findViewById(R.id.top_model_date_from);
        topModelsDateTo = view.findViewById(R.id.top_model_date_to);
        topTradeDateFrom = view.findViewById(R.id.top_trade_date_from);
        topTradeDateTo = view.findViewById(R.id.top_trade_date_to);
        leadsDateFromText = view.findViewById(R.id.leads_date_from_text);
        leadsDateToText = view.findViewById(R.id.leads_date_to_text);
        topModelDateFromText = view.findViewById(R.id.top_model_date_from_text);
        topModelDateToText = view.findViewById(R.id.top_model_date_to_text);
        topTradeDateFromText = view.findViewById(R.id.top_trade_date_from_text);
        topTradeDateToText = view.findViewById(R.id.top_trade_date_to_text);
        topTradesList = view.findViewById(R.id.top_trades_list);
    }

    private void initUI() {
        topTradesAdapter = new TopTradesAdapter();
        topTradesList.setLayoutManager(new LinearLayoutManager(getActivity()));
        topTradesList.setAdapter(topTradesAdapter);
        drawLeadsDiagram();
        drawTopModelDiagram();
        drawTopTradeDiagram();
//        leadsDateFrom.setAdapter(new SpinnerAdapter(getActivity(), R.id.text, new String[]{"01/01/2018", "01/02/2018", "01/03/2018", "01/04/2018", "01/05/2018", "01/06/2018"}));
//        leadsDateTo.setAdapter(new SpinnerAdapter(getActivity(), R.id.text, new String[]{"30/01/2018", "30/02/2018", "30/03/2018", "30/04/2018", "30/05/2018", "30/06/2018"}));
//        topModelsDateFrom.setAdapter(new SpinnerAdapter(getActivity(), R.id.text, new String[]{"01/01/2018", "01/02/2018", "01/03/2018", "01/04/2018", "01/05/2018", "01/06/2018"}));
//        topModelsDateTo.setAdapter(new SpinnerAdapter(getActivity(), R.id.text, new String[]{"30/01/2018", "30/02/2018", "30/03/2018", "30/04/2018", "30/05/2018", "30/06/2018"}));
//        topTradeDateFrom.setAdapter(new SpinnerAdapter(getActivity(), R.id.text, new String[]{"01/01/2018", "01/02/2018", "01/03/2018", "01/04/2018", "01/05/2018", "01/06/2018"}));
//        topTradeDateTo.setAdapter(new SpinnerAdapter(getActivity(), R.id.text, new String[]{"30/01/2018", "30/02/2018", "30/03/2018", "30/04/2018", "30/05/2018", "30/06/2018"}));
    }

    private void setListeners() {
        back.setOnClickListener(v -> getFragmentManager().popBackStack());

        View.OnClickListener onDateClickListener = v -> {
            openedDateDialog = Integer.parseInt(String.valueOf(v.getTag()));
            openDatePickerDialog(((TextView) ((ConstraintLayout) v).getChildAt(0)).getText().toString());
        };
        leadsDateFrom.setOnClickListener(onDateClickListener);
        leadsDateTo.setOnClickListener(onDateClickListener);
        topModelsDateFrom.setOnClickListener(onDateClickListener);
        topModelsDateTo.setOnClickListener(onDateClickListener);
        topTradeDateFrom.setOnClickListener(onDateClickListener);
        topTradeDateTo.setOnClickListener(onDateClickListener);
    }

    private void drawLeadsDiagram() {
        initLeadsChart();
        List<Entry> entries = fakeLeadsInfo();
        LineDataSet dataSet = getLeadsDiagramDataset(entries);
        List<ILineDataSet> dataSets = new ArrayList<>();
        dataSets.add(dataSet);
        LineData data = new LineData(dataSets);
        lineChart.setData(data);
        lineChart.invalidate();
    }

    private void initLeadsChart() {
        LineChart.LayoutParams layoutParams = lineChart.getLayoutParams();
        layoutParams.height = Utils.convertDpToPixel(40 * 5);
        layoutParams.width = Utils.convertDpToPixel(55 + 6 * 100);
        lineChart.setLayoutParams(layoutParams);
        lineChart.getAxisRight().setEnabled(false);
        lineChart.getDescription().setEnabled(false);
        lineChart.getLegend().setEnabled(false);
        XAxis xAxis = lineChart.getXAxis();
        xAxis.setDrawGridLines(false);
        xAxis.setDrawLimitLinesBehindData(false);
        xAxis.setGridColor(getResources().getColor(R.color.transparent));
        xAxis.setTextColor(getResources().getColor(R.color.white));
        XAxisFormatter daysFormatter = new XAxisFormatter();
        daysFormatter.setList(getDays());
        xAxis.setValueFormatter(daysFormatter);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setGranularity(1f);
        xAxis.setAxisMaximum(6f);
        xAxis.setAxisMinimum(0f);
        YAxis yAxis = lineChart.getAxisLeft();
        yAxis.setDrawZeroLine(true);
        yAxis.setDrawAxisLine(false);
        yAxis.setAxisMaximum(1000f);
        yAxis.setAxisMinimum(0f);
        yAxis.setZeroLineWidth(5f);
        yAxis.setGranularity(200f);
        yAxis.setZeroLineColor(getResources().getColor(R.color.white));
        yAxis.setTextColor(getResources().getColor(R.color.white));
    }

    private LineDataSet getLeadsDiagramDataset(List<Entry> entries) {
        LineDataSet dataSet = new LineDataSet(entries, "Leads");
        dataSet.setDrawFilled(true);
        dataSet.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        dataSet.setFillDrawable(getResources().getDrawable(R.drawable.blue_gradient));
        dataSet.setColor(getResources().getColor(R.color.leads_diagram_start_color));
        dataSet.setDrawCircles(false);
        dataSet.setDrawValues(false);
        return dataSet;
    }

    private List<Entry> fakeLeadsInfo() {
        List<Entry> entries = new ArrayList<>();
        entries.add(new Entry(0f, 220f));
        entries.add(new Entry(0.3f, 270f));
        entries.add(new Entry(0.7f, 380f));
        entries.add(new Entry(1f, 580f));
        entries.add(new Entry(1.4f, 700f));
        entries.add(new Entry(2f, 500f));
        entries.add(new Entry(2.4f, 360f));
        entries.add(new Entry(3f, 400f));
        entries.add(new Entry(3.5f, 540f));
        entries.add(new Entry(4f, 420f));
        entries.add(new Entry(4.3f, 360f));
        entries.add(new Entry(4.8f, 320f));
        entries.add(new Entry(5.2f, 300f));
        entries.add(new Entry(5.7f, 350f));
        entries.add(new Entry(6f, 400f));
        return entries;
    }

    private List<String> getDays() {
        List<String> list = new ArrayList<>();
        list.add("Aug1");
        list.add("Aug2");
        list.add("Aug3");
        list.add("Aug4");
        list.add("Aug5");
        list.add("Aug6");
        list.add("Aug7");
        return list;
    }

    private void drawTopModelDiagram() {
        BarDataSet dataSet = getTopModelDiagramDataset(fakeTopModelInfo());
        BarDataSet dataSet2 = getTopModelDiagramDataset(fakeTopModelInfo2());
        BarDataSet dataSet3 = getTopModelDiagramDataset(fakeTopModelInfo3());
        BarDataSet dataSet4 = getTopModelDiagramDataset(fakeTopModelInfo4());
        initTopModelChart(4);
        List<IBarDataSet> dataSets = new ArrayList<>();
        dataSets.add(dataSet);
        dataSets.add(dataSet2);
        dataSets.add(dataSet3);
        dataSets.add(dataSet4);
        BarData data = new BarData(dataSets);
        data.setBarWidth(0.5f);
        barChart.setData(data);
        barChart.invalidate();
    }

    private void initTopModelChart(int itemCount) {
        BarChart.LayoutParams layoutParams = barChart.getLayoutParams();
        layoutParams.width = Utils.convertDpToPixel(40 + 80 * itemCount);
        layoutParams.height = Utils.convertDpToPixel(40 * 7);
        barChart.setLayoutParams(layoutParams);
        barChart.getAxisRight().setEnabled(false);
        barChart.getDescription().setEnabled(false);
        barChart.getLegend().setEnabled(false);
        XAxis xAxis = barChart.getXAxis();
        XAxisFormatter daysFormatter = new XAxisFormatter();
        daysFormatter.setList(getCars());
        xAxis.setValueFormatter(daysFormatter);
        xAxis.setDrawGridLines(false);
        xAxis.setDrawLimitLinesBehindData(false);
        xAxis.setGridColor(getResources().getColor(R.color.transparent));
        xAxis.setTextColor(getResources().getColor(R.color.white));
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setGranularity(1f);
        xAxis.setAxisMaximum(itemCount - 0.55f);
        xAxis.setAxisMinimum(-0.45f);
        YAxis yAxis = barChart.getAxisLeft();
        yAxis.setAxisMinimum(0f);
        yAxis.setAxisMaximum(1400f);
        yAxis.setZeroLineWidth(2f);
        yAxis.setGranularity(200f);
        yAxis.setDrawZeroLine(true);
        yAxis.setDrawAxisLine(false);
        yAxis.setZeroLineColor(getResources().getColor(R.color.white));
        yAxis.setTextColor(getResources().getColor(R.color.white));
    }

    private List<List> fakeTopModelInfo() {
        List<BarEntry> entries = new ArrayList<>();
        List<Integer> color = new ArrayList<>();
        entries.add(new BarEntry(0, 800));
        color.add(Color.parseColor("#FFFFFF"));
        entries.add(new BarEntry(0, 670));
        color.add(Color.parseColor("#FED5EE"));
        entries.add(new BarEntry(0, 450));
        color.add(Color.parseColor("#FEB5E1"));
        entries.add(new BarEntry(0, 390));
        color.add(Color.parseColor("#FE97D4"));
        entries.add(new BarEntry(0, 280));
        color.add(Color.parseColor("#FD76C6"));
        entries.add(new BarEntry(0, 200));
        color.add(Color.parseColor("#FD4AB4"));
        entries.add(new BarEntry(0, 80));
        color.add(Color.parseColor("#FD2AA6"));
        List<List> list = new ArrayList();
        list.add(entries);
        list.add(color);
        return list;
    }

    private List<List> fakeTopModelInfo2() {
        List<BarEntry> entries = new ArrayList<>();
        List<Integer> color = new ArrayList<>();
        entries.add(new BarEntry(1, 700));
        color.add(Color.parseColor("#FFFFFF"));
        entries.add(new BarEntry(1, 550));
        color.add(Color.parseColor("#EBCAFD"));
        entries.add(new BarEntry(1, 450));
        color.add(Color.parseColor("#D99DFB"));
        entries.add(new BarEntry(1, 350));
        color.add(Color.parseColor("#C874FA"));
        entries.add(new BarEntry(1, 180));
        color.add(Color.parseColor("#BB55FA"));
        entries.add(new BarEntry(1, 80));
        color.add(Color.parseColor("#A32CF9"));
        List<List> list = new ArrayList();
        list.add(entries);
        list.add(color);
        return list;
    }

    private List<List> fakeTopModelInfo3() {
        List<BarEntry> entries = new ArrayList<>();
        List<Integer> color = new ArrayList<>();
        entries.add(new BarEntry(2, 900));
        color.add(Color.parseColor("#FFFFFF"));
        entries.add(new BarEntry(2, 750));
        color.add(Color.parseColor("#D9C5FC"));
        entries.add(new BarEntry(2, 700));
        color.add(Color.parseColor("#BB9AFB"));
        entries.add(new BarEntry(2, 560));
        color.add(Color.parseColor("#9F74FA"));
        entries.add(new BarEntry(2, 370));
        color.add(Color.parseColor("#905FFA"));
        entries.add(new BarEntry(2, 210));
        color.add(Color.parseColor("#7942F9"));
        entries.add(new BarEntry(2, 80));
        color.add(Color.parseColor("#612FF9"));
        List<List> list = new ArrayList();
        list.add(entries);
        list.add(color);
        return list;
    }

    private List<List> fakeTopModelInfo4() {
        List<BarEntry> entries = new ArrayList<>();
        List<Integer> color = new ArrayList<>();
        entries.add(new BarEntry(3, 1300));
        color.add(Color.parseColor("#FFFFFF"));
        entries.add(new BarEntry(3, 1050));
        color.add(Color.parseColor("#DADAFD"));
        entries.add(new BarEntry(3, 970));
        color.add(Color.parseColor("#B7B6FC"));
        entries.add(new BarEntry(3, 760));
        color.add(Color.parseColor("#8181FA"));
        entries.add(new BarEntry(3, 440));
        color.add(Color.parseColor("#6163FA"));
        entries.add(new BarEntry(3, 280));
        color.add(Color.parseColor("#494EF9"));
        entries.add(new BarEntry(3, 100));
        color.add(Color.parseColor("#192FF9"));
        List<List> list = new ArrayList();
        list.add(entries);
        list.add(color);
        return list;
    }

    private List<String> getCars() {
        List<String> list = new ArrayList<>();
        list.add("Honda");
        list.add("Audi");
        list.add("BMW");
        list.add("Toyota");
        list.add("Mercedes");
        list.add("Bentley");
        return list;
    }

    private BarDataSet getTopModelDiagramDataset(List<List> lists) {
        BarDataSet dataSet = new BarDataSet(lists.get(0), "Top Models");
        dataSet.setBarBorderWidth(1f);
        dataSet.setDrawValues(false);
        dataSet.setBarBorderColor(Color.parseColor("#293F86"));
        dataSet.setBarShadowColor(getResources().getColor(R.color.transparent));
        dataSet.setColors(lists.get(1));
        return dataSet;
    }

    private void drawTopTradeDiagram() {
        initTopTradeChart();
        List<PieEntry> pieEntries = new ArrayList<>();
        pieEntries.add(new PieEntry(100));
        pieEntries.add(new PieEntry(35));
        pieEntries.add(new PieEntry(60));
        pieEntries.add(new PieEntry(44));
        pieEntries.add(new PieEntry(17));
        pieEntries.add(new PieEntry(80));
        List<String> cars = new ArrayList<>();
        cars.add("Honda");
        cars.add("Audi");
        cars.add("BMW");
        cars.add("Toyota");
        cars.add("Mercedes-Benz");
        cars.add("Volvo");
        topTradesAdapter.updateCars(cars);
        for (PieEntry pieEntry : pieEntries) {
            pieEntriesFullCount += pieEntry.getValue();
        }
        PieDataSet dataSet = getTopTradeDiagramDataset(pieEntries);
        dataSet.setDrawValues(false);

        PieData data = new PieData();
        data.setDataSet(dataSet);
        pieChart.setData(data);
        pieChart.invalidate();
    }

    private void initTopTradeChart() {
        pieChart.getLegend().setEnabled(false);
        pieChart.getDescription().setEnabled(false);
        pieChart.setEntryLabelTextSize(20);
        pieChart.setHoleColor(getResources().getColor(R.color.transparent));
        pieChart.setTransparentCircleAlpha(60);
        pieChart.setTransparentCircleColor(getResources().getColor(R.color.feedback_black));
        pieChart.setTransparentCircleRadius(58);

        pieChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {
                if (clickedEntry != null) {
                    clickedEntry.setLabel("");
                }
                ((PieEntry) e).setLabel(String.valueOf(getPercentOfPieEntry(((PieEntry) e).getValue())) + "%");
                clickedEntry = ((PieEntry) e);
            }

            @Override
            public void onNothingSelected() {
                if (clickedEntry != null) {
                    clickedEntry.setLabel("");
                    clickedEntry = null;
                }
            }
        });
    }

    private PieDataSet getTopTradeDiagramDataset(List<PieEntry> entries) {
        PieDataSet dataSet = new PieDataSet(entries, "Top Trade");
        dataSet.setColor(getResources().getColor(R.color.leads_diagram_start_color));
        dataSet.setValueTextSize(14f);
        dataSet.setSelectionShift(15f);
        dataSet.setSliceSpace(2f);
        List<Integer> list = new ArrayList<>();
        for (Integer i : topTradesAdapter.getColors()) {
            list.add(getResources().getColor(i));
        }
//        switch (entries.size()) {
//            case 5:
//                list.add(Color.parseColor("#FED5EE"));
//            case 4:
//                list.add(Color.parseColor("#612FF9"));
//            case 3:
//                list.add(Color.parseColor("#FD76C6"));
//            case 2:
//                list.add(Color.parseColor("#192FF9"));
//            case 1:
//                list.add(Color.parseColor("#FD2AA6"));
//        }
        dataSet.setColors(list);
        return dataSet;
    }

    private List<PieEntry> fakeTopTradeInfo() {
        List<PieEntry> entries = new ArrayList<>();
        entries.add(new PieEntry(100));
        entries.add(new PieEntry(26));
        entries.add(new PieEntry(10));
        entries.add(new PieEntry(32));
        entries.add(new PieEntry(8));
        return entries;
    }

    private int getPercentOfPieEntry(float entry) {
        return Math.round((entry * 100) / pieEntriesFullCount);
    }

    private void openDatePickerDialog(String dateText) {
        Date date;
        Calendar calendar = Calendar.getInstance();
        try {
            date = dateFormat.parse(dateText);
            calendar.setTime(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        new DatePickerDialog(getActivity(), StatisticsFragment.this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        String date = String.valueOf(dayOfMonth).concat("/").concat(String.valueOf(month).concat("/").concat(String.valueOf(year)));
        switch (openedDateDialog) {
            case LEADS_DATE_FROM:
                leadsDateFromText.setText(date);
                break;
            case LEADS_DATE_TO:
                leadsDateToText.setText(date);
                break;
            case TOP_MODEL_DATE_FROM:
                topModelDateFromText.setText(date);
                break;
            case TOP_MODEL_DATE_TO:
                topModelDateToText.setText(date);
                break;
            case TOP_TRADE_DATE_FROM:
                topTradeDateFromText.setText(date);
                break;
            case TOP_TRADE_DATE_TO:
                topTradeDateToText.setText(date);
                break;
        }
        openedDateDialog = 0;
    }
}
