package com.icarasia.leadmarketplace.ui.fragments;


import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.icarasia.leadmarketplace.R;
import com.icarasia.leadmarketplace.customviews.UnderlineTextView;
import com.icarasia.leadmarketplace.data.enums.LeadStatus;
import com.icarasia.leadmarketplace.data.models.LeadDetail;
import com.icarasia.leadmarketplace.ui.activities.BaseActivity;
import com.icarasia.leadmarketplace.viewmodels.LeadDetailsViewModel;
import com.squareup.picasso.Picasso;

public class LeadDetailsFragment extends Fragment {
    private static final String LEAD_ID_ARGUMENT = "LeadIdArgument";

    private ConstraintLayout root;
    private ImageView back;
    private TextView status;
    private TextView changeStatus;
    private TextView feedback;
    private ImageView feedbackCheck;
    private TextView giveFeedback;
    private TextView leadNumber;
    private TextView leadName;
    private RatingBar ratingBar;
    private TextView date;
    private TextView modelName;
    private TextView time;
    private UnderlineTextView email;
    private UnderlineTextView phone;
    private TextView location;
    private TextView monthlyIncome;
    private TextView monthlyExpense;
    private TextView depositAmount;
    private ImageView insuranceImage;
    private TextView insuranceStatus;
    private TextView make;
    private TextView carModel;
    private LinearLayout comment;

    private LeadDetailsViewModel model;
    private LeadDetail leadDetail;

    public static LeadDetailsFragment newInstance(int id) {

        Bundle args = new Bundle();
        args.putInt(LEAD_ID_ARGUMENT, id);
        LeadDetailsFragment fragment = new LeadDetailsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_lead_details, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
        bindModelData();
        setListeners();
        initUI();

        comment = view.findViewById(R.id.reply);
        comment.setOnClickListener(v ->
                getFragmentManager().beginTransaction()
                        .addToBackStack(null)
                        .replace(R.id.lead_container, CommentsFragment.newInstance(getArguments().getInt(LEAD_ID_ARGUMENT)))
                        .commit()
        );
    }

    private void initViews(View view) {
        root = view.findViewById(R.id.root);
        back = view.findViewById(R.id.back);
        status = view.findViewById(R.id.lead_status);
        changeStatus = view.findViewById(R.id.change_status);
        feedback = view.findViewById(R.id.feedback);
        feedbackCheck = view.findViewById(R.id.feedback_check);
        giveFeedback = view.findViewById(R.id.give_feedback);
        leadNumber = view.findViewById(R.id.number);
        leadName = view.findViewById(R.id.lead_name);
        ratingBar = view.findViewById(R.id.rating_bar);
        date = view.findViewById(R.id.mobileDate);
        modelName = view.findViewById(R.id.model_name);
        time = view.findViewById(R.id.time);
        email = view.findViewById(R.id.email);
        phone = view.findViewById(R.id.telephone);
        location = view.findViewById(R.id.location);
        monthlyIncome = view.findViewById(R.id.monthly_income);
        monthlyExpense = view.findViewById(R.id.monthly_expense);
        depositAmount = view.findViewById(R.id.deposit_amount);
        insuranceImage = view.findViewById(R.id.insurance_status_image);
        insuranceStatus = view.findViewById(R.id.insurance_status);
        make = view.findViewById(R.id.make);
        carModel = view.findViewById(R.id.model);
    }

    private void bindModelData() {
        model = ViewModelProviders.of(this).get(LeadDetailsViewModel.class);

        root.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                root.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                model.getLeadDetail(getArguments().getInt(LEAD_ID_ARGUMENT)).observe(LeadDetailsFragment.this, detailResource -> {
                    if (detailResource != null) {
                        switch (detailResource.status) {
                            case SUCCESS:
                                ((BaseActivity) getActivity()).dismissCurrentDialog();
                                LeadDetailsFragment.this.leadDetail = detailResource.data;
                                setLeadDetail();
                                break;
                            case ERROR:
                                ((BaseActivity) getActivity()).dismissCurrentDialog();
                                if (detailResource.message != null) {
                                    ((BaseActivity) getActivity()).showErrorDialog(detailResource.message);
                                } else {
                                    ((BaseActivity) getActivity()).showErrorDialog();
                                }
                                break;
                            case LOADING:
                                ((BaseActivity) getActivity()).showLoadingDialog();
                                break;
                        }
                    }
                });
            }
        });
    }

    private void setListeners() {
        View.OnClickListener onClickListener = v -> {
            switch (v.getId()) {
                case R.id.change_status:
                    getFragmentManager().beginTransaction()
                            .addToBackStack(null)
                            .replace(R.id.lead_container, ChangeStatusFragment.newInstance(leadDetail.getId(),leadDetail.getStatus()))
                            .commit();
                    break;
                case R.id.back:
                    getActivity().finish();
                    break;
                case R.id.give_feedback:
                    getFragmentManager().beginTransaction()
                            .addToBackStack(null)
                            .replace(R.id.lead_container, FeedbackFragment.newInstance(getArguments().getInt(LEAD_ID_ARGUMENT)))
                            .commit();
                    break;
                case R.id.email:
                    if (leadDetail != null) {
                        email.setDrawUnderline(false);
                        email.setText(leadDetail.getEmail());
                    }
                    break;
                case R.id.telephone:
                    if (leadDetail != null) {
                        email.setDrawUnderline(false);
                        phone.setText(leadDetail.getPhone());
                        setStatus(leadDetail.getStatus());
                    }
                    break;
            }
        };
        changeStatus.setOnClickListener(onClickListener);
        back.setOnClickListener(onClickListener);
        giveFeedback.setOnClickListener(onClickListener);
        email.setOnClickListener(onClickListener);
        phone.setOnClickListener(onClickListener);
    }

    private void initUI() {
        ratingBar.setIsIndicator(true);
    }

    private void setLeadDetail() {
        setFeedback(leadDetail.getIsGivenFeedback());
        setStatus(leadDetail.getStatus());
        leadName.setText(leadDetail.getName());
        date.setText(leadDetail.getDate());
        location.setText(leadDetail.getLocation());
        ratingBar.setRating(leadDetail.getRating());
        modelName.setText(leadDetail.getModelName());
        status.setText(leadDetail.getStatus().getStatusName());
        monthlyIncome.setText(String.valueOf(leadDetail.getMonthlyIncome()));
        monthlyExpense.setText(String.valueOf(leadDetail.getMonthlyExpenses()));
        setInsurance(leadDetail.getInsurance());
        make.setText(leadDetail.getMake());
        carModel.setText(leadDetail.getModel());
    }

    private void setInsurance(String insurance){
        if(insurance.equals("yes")){
            insuranceStatus.setText("Yes");
            Picasso.get().load(R.drawable.success).into(insuranceImage);
        }else {
            insuranceStatus.setText("No");
            insuranceImage.setImageDrawable(null);
        }
    }

    private void setStatus(LeadStatus leadStatus) {
        switch (leadStatus) {
            case READ:
                status.setBackground(getResources().getDrawable(R.drawable.read_status_background));
                break;
            case SALE_IN_PROGRESS:
                status.setBackground(getResources().getDrawable(R.drawable.sale_in_progress_status_background));
                break;
            case SALE_CLOSED:
                status.setBackground(getResources().getDrawable(R.drawable.sale_closed_status_background));
                break;
            case SALE_FAILED:
                status.setBackground(getResources().getDrawable(R.drawable.sale_failed_status_background));
                break;
        }
        status.setText(leadStatus.getStatusName());
    }

    private void setFeedback(int isGivenFeedback) {
        if (isGivenFeedback == 1) {
            feedback.setVisibility(View.VISIBLE);
            feedbackCheck.setVisibility(View.VISIBLE);
            giveFeedback.setText(getString(R.string.view_feedback));
        } else {
            feedback.setVisibility(View.INVISIBLE);
            feedbackCheck.setVisibility(View.INVISIBLE);
            giveFeedback.setText(getString(R.string.give_feedback));
        }
    }
}
