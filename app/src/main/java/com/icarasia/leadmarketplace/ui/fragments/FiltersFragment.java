package com.icarasia.leadmarketplace.ui.fragments;


import android.app.Activity;
import android.app.DatePickerDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;

import com.icarasia.leadmarketplace.R;
import com.icarasia.leadmarketplace.data.enums.LeadStatus;
import com.icarasia.leadmarketplace.viewmodels.FiltersViewModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class FiltersFragment extends Fragment implements DatePickerDialog.OnDateSetListener {
    private final int LEADS_DATE_FROM = 1;
    private final int LEADS_DATE_TO = 2;

    private ImageView back;
    private TextView clear;
    private TextView oneStar;
    private TextView twoStar;
    private TextView threeStar;
    private TextView fourStar;
    private TextView unreadStatus;
    private TextView readStatus;
    private TextView closedStatus;
    private TextView inProgressStatus;
    private TextView failedStatus;
    private TextView apply;
    private ConstraintLayout dateFrom;
    private ConstraintLayout dateTo;
    private TextView dateFromText;
    private TextView dateToText;

    private FiltersViewModel model;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
    private int openedDateDialog = 0;

    public static FiltersFragment newInstance() {

        Bundle args = new Bundle();
        FiltersFragment fragment = new FiltersFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_filters, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
        bindModelData();
        setListeners();
        initUI();
    }

    private void initViews(View view) {
        back = view.findViewById(R.id.back);
        clear = view.findViewById(R.id.clear);
        oneStar = view.findViewById(R.id.one_star);
        twoStar = view.findViewById(R.id.two_star);
        threeStar = view.findViewById(R.id.three_star);
        fourStar = view.findViewById(R.id.four_star);
        unreadStatus = view.findViewById(R.id.unread_status);
        readStatus = view.findViewById(R.id.read_status);
        closedStatus = view.findViewById(R.id.closed_status);
        inProgressStatus = view.findViewById(R.id.in_progress_status);
        failedStatus = view.findViewById(R.id.failed_status);
        apply = view.findViewById(R.id.apply);
        dateFrom = view.findViewById(R.id.leads_date_from);
        dateTo = view.findViewById(R.id.leads_date_to);
        dateFromText = view.findViewById(R.id.date_from);
        dateToText = view.findViewById(R.id.date_to);
    }

    private void bindModelData() {
        model = ViewModelProviders.of(this).get(FiltersViewModel.class);
    }

    private void setListeners() {
        View.OnClickListener ratingClickListener = v -> {
            int rating = Integer.valueOf(((String) v.getTag()));
            setRatingFilter(rating);
            model.setRatingFilter(rating);
        };

        oneStar.setOnClickListener(ratingClickListener);
        twoStar.setOnClickListener(ratingClickListener);
        threeStar.setOnClickListener(ratingClickListener);
        fourStar.setOnClickListener(ratingClickListener);

        View.OnClickListener statusClickListener = v -> {
            LeadStatus leadStatus = LeadStatus.getStatusByName(((TextView) v).getText().toString());
            setStatusFilter(leadStatus);
            model.setStatusFilter(leadStatus);
        };

        readStatus.setOnClickListener(statusClickListener);
        unreadStatus.setOnClickListener(statusClickListener);
        closedStatus.setOnClickListener(statusClickListener);
        inProgressStatus.setOnClickListener(statusClickListener);
        failedStatus.setOnClickListener(statusClickListener);

        apply.setOnClickListener(v -> {
            model.saveFilters();
            getActivity().setResult(Activity.RESULT_OK);
            getActivity().finish();
        });

        back.setOnClickListener(v -> getActivity().finish());

        clear.setOnClickListener(v -> {
            model.setRatingFilter(0);
            setRatingFilter(0);
            model.setStatusFilter(null);
            setStatusFilter(null);
        });

        dateFrom.setOnClickListener(v -> {
            openedDateDialog = LEADS_DATE_FROM;
            openDatePickerDialog(dateFromText.getText().toString());
        });

        dateTo.setOnClickListener(v -> {
            openedDateDialog = LEADS_DATE_TO;
            openDatePickerDialog(dateToText.getText().toString());
        });
    }

    private void initUI() {
        setRatingFilter(model.getRatingFilter());
        setStatusFilter(model.getStatusFilter());
        dateFromText.setText(model.getLeadsDateFrom());
        dateToText.setText(model.getLeadsDateTo());
//        dateFrom.setAdapter(new SpinnerAdapter(getActivity(),R.id.text,new String[]{"01/01/2018","01/02/2018","01/03/2018","01/04/2018","01/05/2018","01/06/2018"}));
//        dateTo.setAdapter(new SpinnerAdapter(getActivity(),R.id.text,new String[]{"30/01/2018","30/02/2018","30/03/2018","30/04/2018","30/05/2018","30/06/2018"}));
    }

    private void setRatingFilter(int rating) {
        oneStar.setBackground(getResources().getDrawable(R.drawable.feedback_background));
        twoStar.setBackground(getResources().getDrawable(R.drawable.feedback_background));
        threeStar.setBackground(getResources().getDrawable(R.drawable.feedback_background));
        fourStar.setBackground(getResources().getDrawable(R.drawable.feedback_background));
        switch (rating) {
            case 1:
                oneStar.setBackground(getResources().getDrawable(R.drawable.black_fill_rounded_back));
                break;
            case 2:
                twoStar.setBackground(getResources().getDrawable(R.drawable.black_fill_rounded_back));
                break;
            case 3:
                threeStar.setBackground(getResources().getDrawable(R.drawable.black_fill_rounded_back));
                break;
            case 4:
                fourStar.setBackground(getResources().getDrawable(R.drawable.black_fill_rounded_back));
                break;
        }
    }

    private void setStatusFilter(LeadStatus leadStatus) {
        readStatus.setBackground(getResources().getDrawable(R.drawable.read_filter_back));
        unreadStatus.setBackground(getResources().getDrawable(R.drawable.read_filter_back));
        closedStatus.setBackground(getResources().getDrawable(R.drawable.sale_closed_filter_back));
        inProgressStatus.setBackground(getResources().getDrawable(R.drawable.sale_in_progress_filter_back));
        failedStatus.setBackground(getResources().getDrawable(R.drawable.sale_failed_filter_back));
        if (leadStatus != null) {
            switch (leadStatus) {
                case READ:
                    readStatus.setBackground(getResources().getDrawable(R.drawable.checked_status_filter_back));
                    break;
                case UNREAD:
                    unreadStatus.setBackground(getResources().getDrawable(R.drawable.checked_status_filter_back));
                    break;
                case SALE_CLOSED:
                    closedStatus.setBackground(getResources().getDrawable(R.drawable.checked_status_filter_back));
                    break;
                case SALE_IN_PROGRESS:
                    inProgressStatus.setBackground(getResources().getDrawable(R.drawable.checked_status_filter_back));
                    break;
                case SALE_FAILED:
                    failedStatus.setBackground(getResources().getDrawable(R.drawable.checked_status_filter_back));
                    break;
            }
        }
    }

    private void openDatePickerDialog(String dateText) {
        Date date;
        Calendar calendar = Calendar.getInstance();
        try {
            date = dateFormat.parse(dateText);
            calendar.setTime(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        new DatePickerDialog(getActivity(),R.style.DialogTheme, FiltersFragment.this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        String date = String.valueOf(year).concat("-").concat(String.valueOf(month).concat("-").concat(String.valueOf(dayOfMonth)));
        switch (openedDateDialog){
            case LEADS_DATE_FROM:
                model.setLeadsDateFrom(date);
                dateFromText.setText(date);
                break;
            case LEADS_DATE_TO:
                model.setLeadsDateTo(date);
                dateToText.setText(date);
                break;
        }
        openedDateDialog = 0;
    }
}
