package com.icarasia.leadmarketplace.ui.fragments;


import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.icarasia.leadmarketplace.R;
import com.icarasia.leadmarketplace.data.enums.Feedback;
import com.icarasia.leadmarketplace.viewmodels.FeedbackViewModel;

public class FeedbackFragment extends Fragment {
    private static final String LEAD_ARGUMENT_ID = "LeadArgumentId";

    private ImageView back;
    private TextView noUrgency;
    private TextView notShowUp;
    private TextView communication;
    private TextView budget;
    private TextView location;
    private TextView other;
    private TextView done;

    private FeedbackViewModel model;
    private Feedback currentFeedback = Feedback.NOTHING;

    public static FeedbackFragment newInstance(int id) {

        Bundle args = new Bundle();
        args.putInt(LEAD_ARGUMENT_ID,id);
        FeedbackFragment fragment = new FeedbackFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_feedback, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
        bindModelData();
        setListeners();
    }

    private void initViews(View view){
        back = view.findViewById(R.id.back);
        noUrgency = view.findViewById(R.id.no_urgency);
        notShowUp = view.findViewById(R.id.not_show_up);
        communication = view.findViewById(R.id.communication);
        budget = view.findViewById(R.id.budget);
        location = view.findViewById(R.id.location);
        other = view.findViewById(R.id.other);
        done = view.findViewById(R.id.done);
    }

    private void bindModelData(){
        model = ViewModelProviders.of(this).get(FeedbackViewModel.class);
//        model.getLead(getArguments().getInt(LEAD_ARGUMENT_ID)).observe(this,lead -> {
//            setCurrentFeedback(lead.getFeedback());
//        });
    }

    private void setListeners(){
        back.setOnClickListener(v -> getFragmentManager().popBackStack());
        done.setOnClickListener(v -> {
//            model.changeFeedback(getArguments().getInt(LEAD_ARGUMENT_ID), currentFeedback);
            getFragmentManager().popBackStack();
        });
        View.OnClickListener clickListener = v -> {
            switch (v.getId()){
                case R.id.no_urgency:
                    setCurrentFeedback(Feedback.NO_URGENCY);
                    break;
                case R.id.not_show_up:
                    setCurrentFeedback(Feedback.NOT_SHOW_UP);
                    break;
                case R.id.communication:
                    setCurrentFeedback(Feedback.COMMUNICATION);
                    break;
                case R.id.budget:
                    setCurrentFeedback(Feedback.BUDGET);
                    break;
                case R.id.location:
                    setCurrentFeedback(Feedback.LOCATION);
                    break;
                case R.id.other:
                    setCurrentFeedback(Feedback.OTHER);
                    break;
            }
        };
        noUrgency.setOnClickListener(clickListener);
        notShowUp.setOnClickListener(clickListener);
        communication.setOnClickListener(clickListener);
        budget.setOnClickListener(clickListener);
        location.setOnClickListener(clickListener);
        other.setOnClickListener(clickListener);
    }

    private void setCurrentFeedback(Feedback feedback){
        currentFeedback = feedback;
        noUrgency.setBackground(getResources().getDrawable(R.drawable.feedback_background));
        notShowUp.setBackground(getResources().getDrawable(R.drawable.feedback_background));
        communication.setBackground(getResources().getDrawable(R.drawable.feedback_background));
        budget.setBackground(getResources().getDrawable(R.drawable.feedback_background));
        location.setBackground(getResources().getDrawable(R.drawable.feedback_background));
        other.setBackground(getResources().getDrawable(R.drawable.feedback_background));
        switch (feedback){
            case NO_URGENCY:
                noUrgency.setBackground(getResources().getDrawable(R.drawable.black_fill_rounded_back));
                break;
            case NOT_SHOW_UP:
                notShowUp.setBackground(getResources().getDrawable(R.drawable.black_fill_rounded_back));
                break;
            case COMMUNICATION:
                communication.setBackground(getResources().getDrawable(R.drawable.black_fill_rounded_back));
                break;
            case BUDGET:
                budget.setBackground(getResources().getDrawable(R.drawable.black_fill_rounded_back));
                break;
            case LOCATION:
                location.setBackground(getResources().getDrawable(R.drawable.black_fill_rounded_back));
                break;
            case OTHER:
                other.setBackground(getResources().getDrawable(R.drawable.black_fill_rounded_back));
                break;
        }
    }
}
