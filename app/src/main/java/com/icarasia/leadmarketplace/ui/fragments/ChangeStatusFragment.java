package com.icarasia.leadmarketplace.ui.fragments;


import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.icarasia.leadmarketplace.R;
import com.icarasia.leadmarketplace.data.enums.LeadStatus;
import com.icarasia.leadmarketplace.ui.activities.BaseActivity;
import com.icarasia.leadmarketplace.viewmodels.ChangeStatusViewModel;

public class ChangeStatusFragment extends Fragment {
    private static final String LEAD_ID_ARGUMENT = "LeadIdArgument";
    private static final String LEAD_STATUS_ARGUMENT = "LeadStatusArgument";

    private ImageView back;
    private FrameLayout read;
    private FrameLayout inProgress;
    private FrameLayout succeed;
    private FrameLayout failed;
    private ImageView readCheck;
    private ImageView inProgressCheck;
    private ImageView succeedCheck;
    private ImageView failedCheck;

    private ChangeStatusViewModel model;
    private LeadStatus currentStatus;
    private TextView done;

    public static ChangeStatusFragment newInstance(int leadId, LeadStatus leadStatus) {

        Bundle args = new Bundle();
        args.putInt(LEAD_ID_ARGUMENT, leadId);
        args.putSerializable(LEAD_STATUS_ARGUMENT, leadStatus);
        ChangeStatusFragment fragment = new ChangeStatusFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_change_status, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
        bindModelData();
        setListeners();
    }

    private void initViews(View view) {
        back = view.findViewById(R.id.back);
        read = view.findViewById(R.id.read);
        inProgress = view.findViewById(R.id.in_progress);
        succeed = view.findViewById(R.id.succeed);
        failed = view.findViewById(R.id.failed);
        readCheck = view.findViewById(R.id.read_check);
        inProgressCheck = view.findViewById(R.id.in_progress_check);
        succeedCheck = view.findViewById(R.id.succeed_check);
        failedCheck = view.findViewById(R.id.failed_check);
        done = view.findViewById(R.id.done);
    }

    private void bindModelData() {
        model = ViewModelProviders.of(this).get(ChangeStatusViewModel.class);
        changeCurrentStatus(((LeadStatus) getArguments().getSerializable(LEAD_STATUS_ARGUMENT)));
    }

    private void setListeners() {
        back.setOnClickListener(v -> getFragmentManager().popBackStack());

        done.setOnClickListener(v -> {
            model.updateStatus(getArguments().getInt(LEAD_ID_ARGUMENT),currentStatus).observe(this, baseResponseResource -> {
                if (baseResponseResource != null) {
                    switch (baseResponseResource.status) {
                        case SUCCESS:
                            ((BaseActivity) getActivity()).dismissCurrentDialog();
                            getFragmentManager().popBackStack();
                            break;
                        case ERROR:
                            ((BaseActivity) getActivity()).dismissCurrentDialog();
                            if (baseResponseResource.message != null) {
                                ((BaseActivity) getActivity()).showErrorDialog(baseResponseResource.message);
                            } else {
                                ((BaseActivity) getActivity()).showErrorDialog();
                            }
                            break;
                        case LOADING:
                            ((BaseActivity) getActivity()).showLoadingDialog();
                            break;
                    }
                }
            });
    });

        View.OnClickListener clickListener = v -> {
            switch (v.getId()) {
                case R.id.read:
                    changeCurrentStatus(LeadStatus.READ);
                    break;
                case R.id.in_progress:
                    changeCurrentStatus(LeadStatus.SALE_IN_PROGRESS);
                    break;
                case R.id.succeed:
                    changeCurrentStatus(LeadStatus.SALE_CLOSED);
                    break;
                case R.id.failed:
                    changeCurrentStatus(LeadStatus.SALE_FAILED);
                    break;
            }
        };
        read.setOnClickListener(clickListener);
        inProgress.setOnClickListener(clickListener);
        succeed.setOnClickListener(clickListener);
        failed.setOnClickListener(clickListener);
    }

    private void changeCurrentStatus(LeadStatus status) {
        currentStatus = status;
        read.setAlpha(0.6f);
        inProgress.setAlpha(0.6f);
        succeed.setAlpha(0.6f);
        failed.setAlpha(0.6f);
        readCheck.setVisibility(View.GONE);
        inProgressCheck.setVisibility(View.GONE);
        succeedCheck.setVisibility(View.GONE);
        failedCheck.setVisibility(View.GONE);
        switch (status) {
            case READ:
                read.setAlpha(1f);
                readCheck.setVisibility(View.VISIBLE);
                break;
            case SALE_IN_PROGRESS:
                inProgress.setAlpha(1f);
                inProgressCheck.setVisibility(View.VISIBLE);
                break;
            case SALE_CLOSED:
                succeed.setAlpha(1f);
                succeedCheck.setVisibility(View.VISIBLE);
                break;
            case SALE_FAILED:
                failed.setAlpha(1f);
                failedCheck.setVisibility(View.VISIBLE);
                break;
        }
    }
}
