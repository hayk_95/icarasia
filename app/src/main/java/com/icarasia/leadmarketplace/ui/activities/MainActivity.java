package com.icarasia.leadmarketplace.ui.activities;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.widget.LinearLayout;

import com.icarasia.leadmarketplace.R;
import com.icarasia.leadmarketplace.ui.fragments.AccountBalanceFragment;
import com.icarasia.leadmarketplace.ui.fragments.AchievementsFragment;
import com.icarasia.leadmarketplace.ui.fragments.EmptyProfileFragment;
import com.icarasia.leadmarketplace.ui.fragments.LeadsFragment;
import com.icarasia.leadmarketplace.ui.fragments.NoChartFragment;
import com.icarasia.leadmarketplace.ui.fragments.NotificationsFragment;
import com.icarasia.leadmarketplace.ui.fragments.TrainingCentreFragment;
import com.icarasia.leadmarketplace.viewmodels.MainViewModel;

public class MainActivity extends BaseActivity {

    private DrawerLayout drawer;
    private NavigationView navigationView;
    private LinearLayout myAccount;
    private LinearLayout achievements;
    private LinearLayout notifications;
    private LinearLayout performance;
    private LinearLayout training;
    private LinearLayout logOut;
    private LinearLayout balance;

    private MainViewModel model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
        bindModelData();
        setListeners();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    private void initViews() {
        drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        myAccount = findViewById(R.id.my_account);
        achievements = findViewById(R.id.achievements);
        notifications = findViewById(R.id.notifications);
        performance = findViewById(R.id.performance);
        training = findViewById(R.id.training);
        logOut = findViewById(R.id.log_out);
        balance = findViewById(R.id.balance);
    }

    private void bindModelData() {
        model = ViewModelProviders.of(this).get(MainViewModel.class);
        new Thread(new Runnable() {
            @Override
            public void run() {
//                model.refresh();
//                RestClient.getInstance().createService(PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString(Constants.AUTH_TOKEN, "")).refreshToken();
//                DataRepository.getInstance().refreshToken();
            }
        }).start();

        if (!model.isUserExist()) {
            openLoginPages();
        } else if (getSupportFragmentManager().findFragmentById(R.id.main_container) == null) {
            openLeadsPage();
        }
    }

    private void setListeners() {
        View.OnClickListener navigationClickListener = v -> {
            switch (v.getId()) {
                case R.id.my_account:
                    openFragment(EmptyProfileFragment.newInstance());
                    break;
                case R.id.balance:
                    openFragment(AccountBalanceFragment.newInstance());
                    break;
                case R.id.achievements:
                    openFragment(AchievementsFragment.newInstance());
                    break;
                case R.id.notifications:
                    openFragment(NotificationsFragment.newInstance());
                    break;
                case R.id.performance:
                    openFragment(NoChartFragment.newInstance());
                    break;
                case R.id.training:
                    openFragment(TrainingCentreFragment.newInstance());
                    break;
                case R.id.log_out:
//                    openFragment(StatisticsFragment.newInstance());
                    model.logOut().observe(this,statusResource -> {
                        switch (statusResource.status){
                            case SUCCESS:
                                dismissCurrentDialog();
                                getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                                openLoginPages();
                                break;
                            case ERROR:
                                dismissCurrentDialog();
                                if(statusResource.message != null){
                                    showErrorDialog(statusResource.message);
                                }else {
                                    showErrorDialog();
                                }
                                break;
                            case LOADING:
                                showLoadingDialog();
                                break;
                        }
                    });
                    break;
            }
            drawer.closeDrawers();
        };
        myAccount.setOnClickListener(navigationClickListener);
        balance.setOnClickListener(navigationClickListener);
        achievements.setOnClickListener(navigationClickListener);
        notifications.setOnClickListener(navigationClickListener);
        performance.setOnClickListener(navigationClickListener);
        training.setOnClickListener(navigationClickListener);
        logOut.setOnClickListener(navigationClickListener);
    }

    private void openFragment(Fragment fragment) {
        if (getSupportFragmentManager().findFragmentById(R.id.main_container) == null ||
                !getSupportFragmentManager().findFragmentById(R.id.main_container).getTag().equals(fragment.getClass().toString())) {
            getSupportFragmentManager().beginTransaction()
                    .addToBackStack(null)
                    .replace(R.id.main_container, fragment, fragment.getClass().toString())
                    .commit();
        }
    }

    private void openLoginPages() {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private void openGuidelinePages() {
        Intent intent = new Intent(this, GuidelineActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private void openLeadsPage() {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.main_container, LeadsFragment.newInstance(), LeadsFragment.class.toString())
                .commit();
    }

    public void setDrawerOpen(boolean open) {
        if (open) {
            drawer.openDrawer(navigationView);
        } else {
            drawer.closeDrawers();
        }
    }
}
