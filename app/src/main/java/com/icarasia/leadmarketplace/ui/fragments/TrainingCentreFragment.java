package com.icarasia.leadmarketplace.ui.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Spinner;

import com.icarasia.leadmarketplace.R;
import com.icarasia.leadmarketplace.adapters.SpinnerAdapter;

public class TrainingCentreFragment extends Fragment {

    private ImageView back;
    private Spinner topics;

    public static TrainingCentreFragment newInstance() {

        Bundle args = new Bundle();
        TrainingCentreFragment fragment = new TrainingCentreFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_training_centre, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
        initUI();
        setListeners();
    }

    private void initViews(View view) {
        back = view.findViewById(R.id.back);
        topics = view.findViewById(R.id.topics_spinner);
    }

    private void initUI() {
        topics.setAdapter(new SpinnerAdapter(getActivity(), R.id.text, new String[]{"Achievements", "Leads", "Dealers", "Performance"},getResources().getInteger(R.integer.text_size_int_15)));
    }

    private void setListeners() {
        back.setOnClickListener(v -> getFragmentManager().popBackStack());
    }
}
