package com.icarasia.leadmarketplace.ui.fragments;


import android.Manifest;
import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.icarasia.leadmarketplace.R;
import com.icarasia.leadmarketplace.Utils;
import com.icarasia.leadmarketplace.data.models.UserInformation;
import com.icarasia.leadmarketplace.ui.activities.BaseActivity;
import com.icarasia.leadmarketplace.viewmodels.EditProfileViewModel;
import com.squareup.picasso.Picasso;

import java.io.File;

public class EditProfileFragment extends Fragment {
    public static final int PERMISSIONS_REQUEST_CODE = 17;
    public static final int GALLERY_REQUEST_CODE = 18;
    public static final int CAMERA_REQUEST_CODE = 19;

    private ConstraintLayout root;
    private ImageView back;
    private TextView changePassword;
    private TextView save;
    private EditText editName;
    private EditText editPosition;
    private EditText editPhone;
    private EditText editPhoneCode;
    private EditText editEmail;
    private ImageView avatar;

    private EditProfileViewModel model;

    public static EditProfileFragment newInstance() {

        Bundle args = new Bundle();
        EditProfileFragment fragment = new EditProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_edit_profile, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
        bindModelData();
        setListeners();
    }

    private void initViews(View view) {
        root = view.findViewById(R.id.root);
        back = view.findViewById(R.id.back);
        changePassword = view.findViewById(R.id.change_password);
        save = view.findViewById(R.id.save);
        editName = view.findViewById(R.id.edit_name);
        editPosition = view.findViewById(R.id.edit_position);
        editPhone = view.findViewById(R.id.edit_number);
        editPhoneCode = view.findViewById(R.id.edit_code);
        editEmail = view.findViewById(R.id.edit_email);
        avatar = view.findViewById(R.id.avatar);
    }

    private void bindModelData() {
        model = ViewModelProviders.of(this).get(EditProfileViewModel.class);

        root.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                root.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                model.getUserInformation().observe(EditProfileFragment.this, userInformationResource -> {
                    if (userInformationResource != null) {
                        switch (userInformationResource.status) {
                            case SUCCESS:
                                ((BaseActivity) getActivity()).dismissCurrentDialog();
                                if (userInformationResource.data != null) {
                                    setUserInformation(userInformationResource.data);
                                }
                                break;
                            case ERROR:
                                ((BaseActivity) getActivity()).dismissCurrentDialog();
                                if (userInformationResource.message != null) {
                                    ((BaseActivity) getActivity()).showErrorDialog(userInformationResource.message);
                                } else {
                                    ((BaseActivity) getActivity()).showErrorDialog();
                                }
                                break;
                            case LOADING:
                                ((BaseActivity) getActivity()).showLoadingDialog();
                                break;
                        }
                    }
                });
            }
        });
    }

    private void setListeners() {
        back.setOnClickListener(v -> getActivity().finish());

        changePassword.setOnClickListener(v ->
                getFragmentManager().beginTransaction()
                        .addToBackStack(null)
                        .replace(R.id.profile_container, EditPasswordFragment.newInstance())
                        .commit());

        avatar.setOnClickListener(v -> {
            if (checkPermissions()) {
                openPickImageDialog();
            }
        });

        save.setOnClickListener(v -> {
            updateUserAccount();
        });
    }

    private void setUserInformation(UserInformation user) {
        editName.setText(user.getName());
        editPosition.setText(user.getPosition());
        Picasso.get().load(user.getAvatar()).into(avatar);
        editPhone.setText(user.getPhone());
        editPhoneCode.setText(user.getCallingCode());
        editEmail.setText(user.getEmail());
    }

    private void updateUserAccount() {
        if (!TextUtils.isEmpty(editName.getText().toString()) &&
                !TextUtils.isEmpty(editPosition.getText().toString()) &&
                !TextUtils.isEmpty(editPhone.getText().toString()) &&
                !TextUtils.isEmpty(editPhoneCode.getText().toString())) {
            if (Utils.isValidEmail(editEmail.getText().toString())) {
                model.updateUserAccount(editName.getText().toString(),
                        editPosition.getText().toString(),
                        editPhone.getText().toString(),
                        editPhoneCode.getText().toString(),
                        editEmail.getText().toString()).observe(this, updateResponseResource -> {
                    if (updateResponseResource != null) {
                        switch (updateResponseResource.status) {
                            case SUCCESS:
                                ((BaseActivity) getActivity()).dismissCurrentDialog();
                                getActivity().finish();
                                break;
                            case ERROR:
                                ((BaseActivity) getActivity()).dismissCurrentDialog();
                                if (updateResponseResource.data != null && updateResponseResource.data.getMessage() != null) {
                                    ((BaseActivity) getActivity()).showErrorDialog(updateResponseResource.data.getMessage());
                                } else {
                                    ((BaseActivity) getActivity()).showErrorDialog();
                                }
                                break;
                            case LOADING:
                                ((BaseActivity) getActivity()).showLoadingDialog();
                                break;
                        }
                    }
                });
            } else {
                Toast.makeText(getActivity(), "Not Valid Email", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(getActivity(), "Please, fill all required fields", Toast.LENGTH_SHORT).show();
        }
    }

    private boolean checkPermissions() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},
                    PERMISSIONS_REQUEST_CODE);
            return false;
        } else {
            return true;
        }
    }

    public void openPickImageDialog() {
        new AlertDialog.Builder(getActivity())
                .setTitle("Select Action")
                .setMessage("Select one option")
                .setPositiveButton("Gallery", (dialog, which) -> {

                    Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto, GALLERY_REQUEST_CODE);

                }).setNeutralButton("Cancel", (dialog, which) -> {

            dialog.dismiss();

        }).setNegativeButton("Camera", (dialog, which) -> {

            Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(takePicture, CAMERA_REQUEST_CODE);

        }).create().show();
    }

    File file = null;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == Activity.RESULT_OK){
            if(requestCode == GALLERY_REQUEST_CODE || requestCode == CAMERA_REQUEST_CODE) {
                Uri selectedImage = data.getData();
                if(selectedImage == null){
                    Bitmap imageBitmap = (Bitmap) data.getExtras().get("data");
                    avatar.setImageBitmap(imageBitmap);
                }else {
                    file = new File(selectedImage.getPath());
                    avatar.setImageURI(selectedImage);
                    Picasso.get().load(selectedImage).into(avatar);
                    model.uploadAvatarImage(file).observe(this,statusResource -> {
                        switch (statusResource.status){
                            case SUCCESS:
                                Toast.makeText(getActivity(), "Uploaded", Toast.LENGTH_SHORT).show();
                                break;
                            case ERROR:
                                Toast.makeText(getActivity(), "Canceled", Toast.LENGTH_SHORT).show();
                                break;
                        }
                    });
                }
            }
        }
    }
}
