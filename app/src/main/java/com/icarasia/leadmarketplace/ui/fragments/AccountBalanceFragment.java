package com.icarasia.leadmarketplace.ui.fragments;


import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.icarasia.leadmarketplace.R;
import com.icarasia.leadmarketplace.ui.activities.BaseActivity;
import com.icarasia.leadmarketplace.ui.activities.PaymentActivity;
import com.icarasia.leadmarketplace.viewmodels.AccountBalanceViewModel;

public class AccountBalanceFragment extends Fragment {
    private ImageView back;
    private TextView coinsCount;
    private FrameLayout coin10;
    private FrameLayout coin25;
    private FrameLayout coin50;
    private FrameLayout coin100;
    private View coin10Icon;
    private View coin25Icon;
    private View coin50Icon;
    private View coin100Icon;
    private LinearLayout infoAboutCoin;
    private TextView buyNow;

    private AccountBalanceViewModel model;

    public static AccountBalanceFragment newInstance() {

        Bundle args = new Bundle();
        AccountBalanceFragment fragment = new AccountBalanceFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_account_balance, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
        bindModelData();
        setListeners();
    }

    private void initViews(View view){
        back = view.findViewById(R.id.back);
        coinsCount = view.findViewById(R.id.coins_count);
        coin10 = view.findViewById(R.id.coin_10);
        coin25 = view.findViewById(R.id.coin_25);
        coin50 = view.findViewById(R.id.coin_50);
        coin100 = view.findViewById(R.id.coin_100);
        coin10Icon = view.findViewById(R.id.coin_10_icon);
        coin25Icon = view.findViewById(R.id.coin_25_icon);
        coin50Icon = view.findViewById(R.id.coin_50_icon);
        coin100Icon = view.findViewById(R.id.coin_100_icon);
        infoAboutCoin = view.findViewById(R.id.info_icar_coin);
        buyNow = view.findViewById(R.id.buy_now);
    }

    private void bindModelData(){
        model = ViewModelProviders.of(this).get(AccountBalanceViewModel.class);

        model.getUserInformation().observe(this,userResource -> {
            switch (userResource.status) {
                case SUCCESS:
                    ((BaseActivity) getActivity()).dismissCurrentDialog();
                    if (userResource.data != null) {
                        coinsCount.setText(String.valueOf(userResource.data.getCredits()));
                    }
                    break;
                case ERROR:
                    ((BaseActivity) getActivity()).dismissCurrentDialog();
                    if (userResource.message != null) {
                        ((BaseActivity) getActivity()).showErrorDialog(userResource.message);
                    } else {
                        ((BaseActivity) getActivity()).showErrorDialog();
                    }
                    break;
                case LOADING:
                    ((BaseActivity) getActivity()).showLoadingDialog();
                    break;
            }
        });
    }

    private void setListeners(){
        back.setOnClickListener(v -> getFragmentManager().popBackStack());

        View.OnClickListener clickListener = v -> {
            coin10.setBackground(getResources().getDrawable(R.drawable.coin_background));
            coin25.setBackground(getResources().getDrawable(R.drawable.coin_background));
            coin50.setBackground(getResources().getDrawable(R.drawable.coin_background));
            coin100.setBackground(getResources().getDrawable(R.drawable.coin_background));
            coin10Icon.setBackground(getResources().getDrawable(R.drawable.white_empty_circle));
            coin25Icon.setBackground(getResources().getDrawable(R.drawable.white_empty_circle));
            coin50Icon.setBackground(getResources().getDrawable(R.drawable.white_empty_circle));
            coin100Icon.setBackground(getResources().getDrawable(R.drawable.white_empty_circle));
            switch (v.getId()){
                case R.id.coin_10:
                    coin10.setBackground(getResources().getDrawable(R.drawable.blue_fill_rounded_back_without_padding));
                    coin10Icon.setBackground(getResources().getDrawable(R.drawable.white_fill_circle));
                    break;
                case R.id.coin_25:
                    coin25.setBackground(getResources().getDrawable(R.drawable.blue_fill_rounded_back_without_padding));
                    coin25Icon.setBackground(getResources().getDrawable(R.drawable.white_fill_circle));
                    break;
                case R.id.coin_50:
                    coin50.setBackground(getResources().getDrawable(R.drawable.blue_fill_rounded_back_without_padding));
                    coin50Icon.setBackground(getResources().getDrawable(R.drawable.white_fill_circle));
                    break;
                case R.id.coin_100:
                    coin100.setBackground(getResources().getDrawable(R.drawable.blue_fill_rounded_back_without_padding));
                    coin100Icon.setBackground(getResources().getDrawable(R.drawable.white_fill_circle));
                    break;
            }
        };
        coin10.setOnClickListener(clickListener);
        coin25.setOnClickListener(clickListener);
        coin50.setOnClickListener(clickListener);
        coin100.setOnClickListener(clickListener);

        infoAboutCoin.setOnClickListener(v ->
            getFragmentManager().beginTransaction()
                    .addToBackStack(null)
                    .replace(R.id.main_container,TrainingCentreFragment.newInstance())
                    .commit());

        buyNow.setOnClickListener(v ->
                startActivity(new Intent(getActivity(),PaymentActivity.class)));
    }
}
