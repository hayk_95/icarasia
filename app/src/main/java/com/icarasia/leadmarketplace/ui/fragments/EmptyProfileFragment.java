package com.icarasia.leadmarketplace.ui.fragments;


import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.icarasia.leadmarketplace.R;
import com.icarasia.leadmarketplace.data.models.User;
import com.icarasia.leadmarketplace.ui.activities.BaseActivity;
import com.icarasia.leadmarketplace.ui.activities.ProfileActivity;
import com.icarasia.leadmarketplace.viewmodels.ProfileViewModel;
import com.squareup.picasso.Picasso;

public class EmptyProfileFragment extends Fragment {

    private ImageView back;
    private ImageView credit;
    private TextView edit;
    private TextView name;
    private TextView role;
    private ImageView avatar;

    private ProfileViewModel model;

    public static EmptyProfileFragment newInstance() {

        Bundle args = new Bundle();
        EmptyProfileFragment fragment = new EmptyProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_empty_profile, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
        bindModelData();
        setListeners();
    }

    private void initViews(View view) {
        back = view.findViewById(R.id.back);
        credit = view.findViewById(R.id.credit_icon);
        edit = view.findViewById(R.id.edit_profile);
        name = view.findViewById(R.id.name);
        role = view.findViewById(R.id.role);
        avatar = view.findViewById(R.id.avatar);
    }

    private void bindModelData(){
        model = ViewModelProviders.of(this).get(ProfileViewModel.class);
        model.getUserInformation().observe(this, userResource -> {
            switch (userResource.status) {
                case SUCCESS:
                    ((BaseActivity) getActivity()).dismissCurrentDialog();
                    if (userResource.data != null) {
                        setUserInformation(userResource.data);
                    }
                    break;
                case ERROR:
                    ((BaseActivity) getActivity()).dismissCurrentDialog();
                    if (userResource.message != null) {
                        ((BaseActivity) getActivity()).showErrorDialog(userResource.message);
                    } else {
                        ((BaseActivity) getActivity()).showErrorDialog();
                    }
                    break;
                case LOADING:
                    ((BaseActivity) getActivity()).showLoadingDialog();
                    break;
            }
        });
    }

    private void setListeners() {
        back.setOnClickListener(v -> getFragmentManager().popBackStack());

        credit.setOnClickListener(v ->
                getFragmentManager().beginTransaction()
                        .addToBackStack(null)
                        .replace(R.id.main_container, AccountBalanceFragment.newInstance())
                        .commit());

        edit.setOnClickListener(v -> startActivity(new Intent(getActivity(), ProfileActivity.class)));
    }

    private void setUserInformation(User user) {
        name.setText(user.getName());
        role.setText(user.getRole().get(0));
        Picasso.get().load(user.getAvatar()).into(avatar);
    }
}
