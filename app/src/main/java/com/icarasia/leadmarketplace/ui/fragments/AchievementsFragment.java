package com.icarasia.leadmarketplace.ui.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.icarasia.leadmarketplace.R;

public class AchievementsFragment extends Fragment {

    private ImageView back;

    public static AchievementsFragment newInstance() {

        Bundle args = new Bundle();
        AchievementsFragment fragment = new AchievementsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_achievements, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
        setListeners();
    }

    private void initViews(View view){
        back = view.findViewById(R.id.back);
    }

    private void setListeners(){
        back.setOnClickListener(v -> getFragmentManager().popBackStack());
    }
}
