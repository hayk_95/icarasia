package com.icarasia.leadmarketplace.ui.fragments;


import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.icarasia.leadmarketplace.R;
import com.icarasia.leadmarketplace.Utils;
import com.icarasia.leadmarketplace.viewmodels.LoginViewModel;

public class LoginEmailFragment extends Fragment {

    private EditText editEmail;
    private TextView next;
    private ImageView emailIcon;

    private LoginViewModel model;

    public static LoginEmailFragment newInstance() {

        Bundle args = new Bundle();
        LoginEmailFragment fragment = new LoginEmailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login_email, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
        bindModelData();
        setListeners();
    }

    private void initViews(View view){
        editEmail = view.findViewById(R.id.enter_email);
        next = view.findViewById(R.id.next);
        emailIcon = view.findViewById(R.id.email_icon);
    }

    private void bindModelData(){
        model = ViewModelProviders.of(getActivity()).get(LoginViewModel.class);
    }

    private void setListeners(){
        next.setOnClickListener(view -> {
            if(Utils.isValidEmail(editEmail.getText().toString())){
                model.setEmail(editEmail.getText().toString());

                getFragmentManager().beginTransaction()
                        .addToBackStack(null)
                        .replace(R.id.login_container,LoginPasswordFragment.newInstance())
                        .commit();
            }else {
                Toast.makeText(getActivity(), "Not valid email_icon", Toast.LENGTH_SHORT).show();
            }
        });

        editEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(TextUtils.isEmpty(editEmail.getText().toString())){
                    emailIcon.setVisibility(View.VISIBLE);
                }else {
                    emailIcon.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

}
