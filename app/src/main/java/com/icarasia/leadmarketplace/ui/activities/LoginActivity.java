package com.icarasia.leadmarketplace.ui.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.KeyEvent;

import com.icarasia.leadmarketplace.R;
import com.icarasia.leadmarketplace.ui.fragments.InitialFragment;
import com.icarasia.leadmarketplace.ui.fragments.SignupPasswordFragment;

public class LoginActivity extends BaseActivity {

    private OnKeyUpListener keyUpListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        init();
    }

    private void init(){
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.login_container,InitialFragment.newInstance())
                .commit();
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if(keyUpListener != null){
            keyUpListener.onKeyUp(keyCode);
        }
        return super.onKeyUp(keyCode, event);
    }

    public interface OnKeyUpListener {
        void onKeyUp(int keyCode);
    }

    public void setOnKeyUpListener(OnKeyUpListener keyUpListener){
        this.keyUpListener = keyUpListener;
    }

    @Override
    public void onBackPressed() {
        if(getSupportFragmentManager().findFragmentById(R.id.login_container) instanceof SignupPasswordFragment){
            getSupportFragmentManager().popBackStack(null,FragmentManager.POP_BACK_STACK_INCLUSIVE);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.login_container,InitialFragment.newInstance())
                    .commit();
        }else {
            super.onBackPressed();
        }
    }
}
