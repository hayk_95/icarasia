package com.icarasia.leadmarketplace.ui.activities;

import android.os.Bundle;

import com.icarasia.leadmarketplace.R;
import com.icarasia.leadmarketplace.ui.fragments.LeadDetailsFragment;

public class LeadActivity extends BaseActivity {
    public static final String LEAD_ID = "LeadId";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lead);
        init();
    }

    private void init(){
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.lead_container,LeadDetailsFragment.newInstance(getIntent().getIntExtra(LEAD_ID,-1)))
                .commit();
    }
}
