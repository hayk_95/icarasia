package com.icarasia.leadmarketplace.ui.activities;

import android.os.Bundle;

import com.icarasia.leadmarketplace.R;
import com.icarasia.leadmarketplace.ui.fragments.FiltersFragment;

public class FiltersActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filters);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.filters_container,FiltersFragment.newInstance())
                .commit();
    }
}
