package com.icarasia.leadmarketplace.ui.fragments;


import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.icarasia.leadmarketplace.R;
import com.icarasia.leadmarketplace.viewmodels.EditProfileViewModel;

public class EditPasswordFragment extends Fragment {

    private ImageView back;
    private EditText editOldPassword;
    private EditText editNewPassword;
    private EditText editConfirmPassword;
    private TextView save;
    private TextView cancel;

    private EditProfileViewModel model;

    public static EditPasswordFragment newInstance() {

        Bundle args = new Bundle();
        EditPasswordFragment fragment = new EditPasswordFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_edit_password, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
        bindModelData();
        setListeners();
    }

    private void initViews(View view) {
        back = view.findViewById(R.id.back);
        editOldPassword = view.findViewById(R.id.edit_old_password);
        editNewPassword = view.findViewById(R.id.edit_new_password);
        editConfirmPassword = view.findViewById(R.id.edit_confirm_password);
        save = view.findViewById(R.id.save);
        cancel = view.findViewById(R.id.cancel);
    }

    private void bindModelData() {
        model = ViewModelProviders.of(getActivity()).get(EditProfileViewModel.class);
    }

    private void setListeners(){
        back.setOnClickListener(v -> getFragmentManager().popBackStack());

        save.setOnClickListener(v -> {
            if(verifyPasswords()){
                model.setPassword(editOldPassword.getText().toString(), editNewPassword.getText().toString(),editConfirmPassword.getText().toString());
                getFragmentManager().popBackStack();
            }
        });

        cancel.setOnClickListener(v -> getFragmentManager().popBackStack());
    }

    private boolean verifyPasswords() {
        if (editOldPassword.getText().toString().length() > 7 &&
                editNewPassword.getText().toString().length() > 7 &&
                editConfirmPassword.getText().toString().length() > 7) {
            if (editNewPassword.getText().toString().equals(editConfirmPassword.getText().toString())) {
                return true;
            } else {
                Toast.makeText(getActivity(), "New passwords are not equals", Toast.LENGTH_SHORT).show();
            }
        }else {
            Toast.makeText(getActivity(), "Passwords must be greater than 8 characters", Toast.LENGTH_SHORT).show();
        }
        return false;
    }
}
