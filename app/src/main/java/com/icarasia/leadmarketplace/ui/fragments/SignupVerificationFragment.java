package com.icarasia.leadmarketplace.ui.fragments;


import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.icarasia.leadmarketplace.R;
import com.icarasia.leadmarketplace.Utils;
import com.icarasia.leadmarketplace.ui.activities.LoginActivity;
import com.icarasia.leadmarketplace.viewmodels.SignupViewModel;

import java.util.ArrayList;
import java.util.List;

public class SignupVerificationFragment extends Fragment implements LoginActivity.OnKeyUpListener {

    private EditText rect1;
    private EditText rect2;
    private EditText rect3;
    private EditText rect4;
    private EditText rect5;
    private EditText rect6;
    private TextView confirm;
    private View rectGroup;

    private SignupViewModel model;
    private int currentEditNumber = 0;
    private List<EditText> editsList;

    public static SignupVerificationFragment newInstance() {

        Bundle args = new Bundle();
        SignupVerificationFragment fragment = new SignupVerificationFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_signup_verification, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
        bindModelData();
        setListeners();
        initUI();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ((LoginActivity) getActivity()).setOnKeyUpListener(null);
    }

    private void initViews(View view) {
        rect1 = view.findViewById(R.id.rect_1);
        rect2 = view.findViewById(R.id.rect_2);
        rect3 = view.findViewById(R.id.rect_3);
        rect4 = view.findViewById(R.id.rect_4);
        rect5 = view.findViewById(R.id.rect_5);
        rect6 = view.findViewById(R.id.rect_6);
        confirm = view.findViewById(R.id.confirm);
        rectGroup = view.findViewById(R.id.rect_group);
    }

    private void bindModelData() {
        model = ViewModelProviders.of(this).get(SignupViewModel.class);
    }

    private void setListeners() {
        confirm.setOnClickListener(v -> {
            if (TextUtils.isEmpty(rect1.getText()) ||
                    TextUtils.isEmpty(rect2.getText()) ||
                    TextUtils.isEmpty(rect3.getText()) ||
                    TextUtils.isEmpty(rect4.getText()) ||
                    TextUtils.isEmpty(rect5.getText()) ||
                    TextUtils.isEmpty(rect6.getText())) {
                Toast.makeText(getActivity(), "The code was not filled properly", Toast.LENGTH_SHORT).show();
            } else {
                model.sendVerification(mergeVerificationsNumbers()).observe(SignupVerificationFragment.this, statusResource -> {
                    if (statusResource != null) {
                        switch (statusResource.status) {
                            case SUCCESS:
                                getFragmentManager().beginTransaction()
                                        .replace(R.id.login_container, SignupPasswordFragment.newInstance())
                                        .commit();
                                break;
                            case ERROR:
                                Toast.makeText(getActivity(), statusResource.message, Toast.LENGTH_SHORT).show();
                                break;
                            case LOADING:
                                break;
                        }
                    }
                });
            }
        });

        rectGroup.setOnClickListener(v -> {
            editsList.get(currentEditNumber).requestFocus();
            setKeyboardVisibility(true);
        });
    }

    private void initUI() {
        initEditTexts();
        rect1.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                rect1.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                LinearLayout.LayoutParams layoutParams = ((LinearLayout.LayoutParams) rect1.getLayoutParams());
                layoutParams.height = Math.round(rect1.getWidth() / 0.75f);
                layoutParams.setMarginStart(Utils.convertDpToPixel(5));
                layoutParams.setMarginEnd(Utils.convertDpToPixel(5));
                rect1.setLayoutParams(layoutParams);
                rect2.setLayoutParams(layoutParams);
                rect3.setLayoutParams(layoutParams);
                rect4.setLayoutParams(layoutParams);
                rect5.setLayoutParams(layoutParams);
                rect6.setLayoutParams(layoutParams);
            }
        });
        ((LoginActivity) getActivity()).setOnKeyUpListener(this);
    }

    private void initEditTexts() {
        editsList = new ArrayList<>();
        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                requestEdit(!TextUtils.isEmpty(editsList.get(currentEditNumber).getText()));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };
        editsList.add(rect1);
        editsList.add(rect2);
        editsList.add(rect3);
        editsList.add(rect4);
        editsList.add(rect5);
        editsList.add(rect6);
        for (EditText editText : editsList) {
            editText.addTextChangedListener(textWatcher);
        }
    }


    private void requestEdit(boolean isFull) {
        if (editsList.get(currentEditNumber).isEnabled()) {
            if (currentEditNumber > 0 && currentEditNumber < 5) {
                if (isFull) {
                    changeEditFocus(true);
                } else {
                    changeEditFocus(false);
                }
            } else if (currentEditNumber == 0) {
                if (isFull) {
                    changeEditFocus(true);
                }
            } else if (currentEditNumber == 5) {
                if (isFull) {
                    setKeyboardVisibility(false);
                } else {
                    changeEditFocus(false);
                }
            }
        }
    }

    private void changeEditFocus(boolean toNextEdit) {
        if (toNextEdit) {
            editsList.get(currentEditNumber + 1).setEnabled(true);
            editsList.get(currentEditNumber + 1).requestFocus();
            editsList.get(currentEditNumber).setEnabled(false);
            currentEditNumber++;
        } else {
            editsList.get(currentEditNumber - 1).setEnabled(true);
            editsList.get(currentEditNumber - 1).requestFocus();
            editsList.get(currentEditNumber).setEnabled(false);
            currentEditNumber--;
        }
    }


    private void setKeyboardVisibility(boolean visibility) {
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            if (visibility) {
                imm.showSoftInput(editsList.get(currentEditNumber), InputMethodManager.SHOW_IMPLICIT);
            } else {
                if (imm != null) {
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
            }
        }
    }

    private String mergeVerificationsNumbers() {
        return rect1.getText().toString()
                .concat(rect2.getText().toString())
                .concat(rect3.getText().toString())
                .concat(rect4.getText().toString())
                .concat(rect5.getText().toString())
                .concat(rect6.getText().toString());
    }

    @Override
    public void onKeyUp(int keyCode) {
        if (keyCode == KeyEvent.KEYCODE_DEL
                && currentEditNumber > 0
                && TextUtils.isEmpty(editsList.get(currentEditNumber).getText())) {
            editsList.get(currentEditNumber - 1).setText("");
            changeEditFocus(false);
        }
    }
}
