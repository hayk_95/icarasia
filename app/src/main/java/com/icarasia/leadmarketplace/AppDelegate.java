package com.icarasia.leadmarketplace;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.icarasia.leadmarketplace.data.DataRepository;
import com.icarasia.leadmarketplace.data.room.AppDatabase;

public class AppDelegate extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        getRepository();
    }

    private AppDatabase getDatabase() {
        return AppDatabase.getInstance(this);
    }

    public SharedPreferences getDefaultSharedPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(this);
    }

    public DataRepository getRepository() {
        return DataRepository.getInstance(getDatabase(), getDefaultSharedPreferences());
    }
}
